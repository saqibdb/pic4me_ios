//
//  Utility.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 8/2/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Utility : NSObject




+(NSString *)getTimeFormatForDate :(NSDate *)commentDate ;

@end
