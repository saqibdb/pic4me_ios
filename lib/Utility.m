//
//  Utility.m
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 8/2/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "Utility.h"
#import <UIKit/UIKit.h>
//#import "OutBeat-swift.h"

@implementation Utility


+(UIView *)createGrayView {
    UIView *grayView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    grayView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
    return grayView ;
}

+(UIColor *)getOrangeColor {
    UIColor *orangeColor = [UIColor colorWithRed:251.0 / 255.0 green:102.0 / 255.0 blue:68.0 / 255.0 alpha:1.0];
    return orangeColor ;
}


+(NSString *)getTimeFormatForDate :(NSDate *)commentDate
{
    

    
    
    
    NSDate* datetime = [NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]]; // Prevent adjustment to user's local time zone.
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    
    NSTimeInterval distanceBetweenDates = [datetime timeIntervalSinceDate:commentDate];
    double secondsInAMinute = 60;
    NSInteger minutesBetweenDates = distanceBetweenDates / secondsInAMinute;
    
    
    if (minutesBetweenDates < 3) {
        return @"now" ;
    }
    else if (minutesBetweenDates < 60){
        return [NSString stringWithFormat:@"%li mins ago.",(long)minutesBetweenDates] ;
    }
    else if (minutesBetweenDates < 1440){
        long hours = minutesBetweenDates / 60 ;
        return [NSString stringWithFormat:@"%i hours ago.",(int)hours] ;
    }
    else if (minutesBetweenDates < 43200){
        long hours = minutesBetweenDates / (60*24) ;
        return [NSString stringWithFormat:@"%i days ago",(int)hours] ;
    }
    else if (minutesBetweenDates < 525600){
        long hours = minutesBetweenDates / (60*24*30) ;
        return [NSString stringWithFormat:@"%i months ago",(int)hours] ;
    }
    else{
        return [NSString stringWithFormat:@"%li",(long)minutesBetweenDates] ;
    }
    
    /*
     
    NSDate *currentDate = [[NSDate alloc] init];
    NSTimeZone *timeZone = [NSTimeZone defaultTimeZone];
    // or Timezone with specific name like
    // [NSTimeZone timeZoneWithName:@"Europe/Riga"] (see link below)
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *localDateString = [dateFormatter stringFromDate:currentDate];
    return localDateString ;
    */
}

@end
