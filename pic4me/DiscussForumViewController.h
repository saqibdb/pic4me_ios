//
//  DiscussForumViewController.h
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/14/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Photo.h"

#import <AssetsLibrary/AssetsLibrary.h>

@interface DiscussForumViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *lblNoData;

@property (weak, nonatomic) IBOutlet UITableView *photosTableView;
- (IBAction)backeAction:(id)sender;
@property (strong) NSMutableArray  *photosFound;
@property (strong) NSMutableArray  *thumbsFound;
@property (strong) NSMutableArray  *vidsFound;
@property (strong) Photo *selectedPhoto;
@property (strong) NSMutableArray *selections;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

- (IBAction)yourPhotos:(UIButton *)sender;
- (IBAction)photosOfYouAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *yourPhotosSprite;
@property (weak, nonatomic) IBOutlet UIView *friendsPhotosSprite;




@end
