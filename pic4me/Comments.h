//
//  Comments.h
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/15/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"
#import "Photo.h"
#import "Video.h"


@interface Comments : NSObject

@property (nonatomic, strong) NSString *Message;
@property (nonatomic, strong) BackendlessUser *User;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) Photo *photo;
@property (nonatomic, strong) Video *video;

@end
