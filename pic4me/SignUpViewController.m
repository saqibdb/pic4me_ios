//
//  SignUpViewController.m
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/21/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "SignUpViewController.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "ActionSheetPicker.h"

@interface SignUpViewController ()
{
    UITextField *currentTextField;
    UIImage *selectedImage;
}

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.userNameTxt.delegate = self;
    self.emailTxt.delegate = self;
    self.passwordTxt.delegate = self;
    self.genderTxt.delegate = self;
    self.phoneTxt.delegate = self;
    [self maskTriananlgesAndCircularBorderedImage];
    [self createHorizontalList];
    [self setCursorColor];
}
    
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
    
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
    
#pragma mark - keyboard movements
    
- (void)keyboardWillShow:(NSNotification *)notification
    {
        CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = -keyboardSize.height/2.0;
            self.view.frame = f;
        }];
    }
    
-(void)keyboardWillHide:(NSNotification *)notification
    {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect f = self.view.frame;
            f.origin.y = 0.0f;
            self.view.frame = f;
        }];
    }

-(void)setCursorColor {
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)maskTriananlgesAndCircularBorderedImage {
    // Build a triangular path
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){self.signUpSprite.frame.size.width / 2, 0}];
    [path addLineToPoint:(CGPoint){0, self.signUpSprite.frame.size.height}];
    [path addLineToPoint:(CGPoint){self.signUpSprite.frame.size.width, self.signUpSprite.frame.size.height}];
    [path addLineToPoint:(CGPoint){self.signUpSprite.frame.size.width / 2, 0}];
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = self.signUpSprite.bounds;
    mask.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    self.signUpSprite.layer.mask = mask;
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask2 = [CAShapeLayer new];
    mask2.frame = self.signUpSprite.bounds;
    mask2.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    self.signUpSprite.layer.mask = mask2;
    
    
    
    self.signUpBtn.layer.cornerRadius = 5;
    self.signUpBtn.clipsToBounds = YES;
    
    [_profileImageBtn.layer setCornerRadius:_profileImageBtn.frame.size.height/2];
    _profileImageBtn.clipsToBounds=YES;
    [_profileBackview.layer setCornerRadius:_profileBackview.frame.size.height/2];
    _profileBackview.clipsToBounds=YES;
   
    
    
   
}
- (void)createHorizontalList {
    
    
    TNCircularRadioButtonData *maleData = [TNCircularRadioButtonData new];
    maleData.labelText = @"Male";
    maleData.identifier = @"male";
    maleData.selected = YES;
    maleData.labelColor =[UIColor whiteColor];
    maleData.borderColor = [UIColor whiteColor];
    maleData.circleColor = [UIColor whiteColor];
    
    
    TNCircularRadioButtonData *femaleData = [TNCircularRadioButtonData new];
    femaleData.labelText = @"Female";
    femaleData.identifier = @"female";
    femaleData.selected = NO;
    femaleData.borderRadius = 12;
    femaleData.circleRadius = 5;
    femaleData.labelColor=[UIColor whiteColor];
    femaleData.borderColor = [UIColor whiteColor];
    femaleData.circleColor = [UIColor whiteColor];
    
    
    _sexGroup= [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[maleData, femaleData] layout:TNRadioButtonGroupLayoutHorizontal];
    self.sexGroup.identifier = @"Sex group";
    [self.sexGroup create];
    // self.sexGroup.position = CGPointMake(25, 300);
    [self.OuterView addSubview:_sexGroup];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sexGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:self.sexGroup];
    
    // show how update data works...
    
    [self.sexGroup update];
    
}
- (void)sexGroupUpdated:(NSNotification *)notification {
    NSLog(@"[MainView] Sex group updated to %@", self.sexGroup.selectedRadioButton.data.identifier);
}


-(void)saveNewContact

{
    [SVProgressHUD showWithStatus:@"Registering User"];
    
    BackendlessUser *user = [BackendlessUser new];
    
    
    
    [user setProperty:@"name" object:_userNameTxt.text];
    [user setProperty:@"email" object:_emailTxt.text];
    [user setProperty:@"password" object:_passwordTxt.text];
    [user setProperty:@"gender" object:self.sexGroup.selectedRadioButton.data.identifier];
    [user setProperty:@"age" object:_genderTxt.text];
    [user setProperty:@"phone" object:_phoneTxt.text];
    
        [backendless.userService registering:user response:^(BackendlessUser *userRegistered) {
        NSLog(@"Object Id Will be = %@", userRegistered.objectId);
        
        [self uploadAsyncWithImageName:userRegistered.objectId andNewRegistereddUser:userRegistered];
        
    } error:^(Fault *error) {
       
       AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:error.message andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];
        
        [SVProgressHUD dismiss];
        
    }];
    
}
-(void)loginWithUser :(BackendlessUser *)loginUser
{
    [loginUser setProperty:@"password" object:_passwordTxt.text];
    
    [backendless.userService login:loginUser.email password:loginUser.password response:^(BackendlessUser *userLogged) {
        UIDevice *device = [UIDevice currentDevice];
        NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
        
        
        [userLogged setProperty:@"deviceId" object:currentDeviceId];
        
        id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
        [dataStore save:userLogged response:^(id result) {
            
        } error:^(Fault *error) {
            NSLog(@"Error at deviceId %@",error.description);
        }];
        
        
        
        [SVProgressHUD dismiss];
        [self performSegueWithIdentifier:@"signUpToHomeScreen" sender:self];
    } error:^(Fault *error) {
        
        [SVProgressHUD dismiss];
        
       AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:[NSString stringWithFormat:@"%@",error.message] andCancelButton:NO forAlertType:AlertInfo];
         [alert show];
    }];
}


-(void)updateImageUrl:(BackendlessUser *)imageUpdateUser :(NSString *)newUrl
{
    
    [imageUpdateUser setProperty:@"profileImage" object:[NSString stringWithFormat:@"%@", newUrl]];
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    [dataStore save:imageUpdateUser response:^(id result) {
        [SVProgressHUD dismiss];
        [self loginWithUser:imageUpdateUser];
        
    } error:^(Fault *error) {
        NSLog(@"Error At imageUrl Update %@", error.description);
               NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:error.message andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];
        
        
        [SVProgressHUD dismiss];
        
    }];
}

-(void)uploadAsyncWithImageName :(NSString *)imageName andNewRegistereddUser :(BackendlessUser *)newRegisteredUser {
    
    NSLog(@"\n============ Uploading file  with the ASYNC API ============");
    
   

    int REQUIRED_SIZE = 100;
    selectedImage = [self getScaledToRequiredSizedImage:REQUIRED_SIZE :selectedImage];
    
    NSData *imageData = UIImageJPEGRepresentation(selectedImage, 1.0);
    
    [backendless.fileService upload:[NSString stringWithFormat:@"myfiles/%@.jpg", imageName]  content:imageData
                           response:^(BackendlessFile *uploadedFile) {
                               NSLog(@"File has been uploaded. File URL is - %@", uploadedFile.fileURL);
                               
                               [self updateImageUrl:newRegisteredUser :uploadedFile.fileURL];
                               
                           }
                              error:^(Fault *fault) {
                                  NSLog(@"Server reported an error: %@", fault);
                                  
                                 
                                  NSLog(@"FAULT = %@ <%@>", fault.message, fault.detail);
                                  AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:fault.message andCancelButton:NO forAlertType:AlertFailure];
                                  
                                  [alert show];
                                  
                                  [SVProgressHUD dismiss];
                                  
                              }];
}

-(UIImage *)getScaledToRequiredSizedImage :(int)requiredSize :(UIImage *)sentImage{
    int scale = 1;
    
    while (sentImage.size.width / scale / 2 >= requiredSize
           && sentImage.size.height / scale / 2 >= requiredSize)
        scale *= 2;
    
    return [self imageWithImage:selectedImage scaledToSize:CGSizeMake(selectedImage.size.width/scale, selectedImage.size.height/scale)];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (IBAction)imagePickerAction:(UIButton *)sender {
    NSArray *colors = [NSArray arrayWithObjects:@"Gallery" , @"Camera", nil];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Image Source."
                                            rows:colors
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           
                                           if (selectedIndex == 1) {
                                               [self showImagePicker:UIImagePickerControllerSourceTypeCamera sender:nil];
                                               
                                           }
                                           else{
                                               [self showImagePicker:UIImagePickerControllerSourceTypePhotoLibrary sender:nil];
                                               
                                           }
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];
    
}


#pragma mark Image Picker And Delegates

- (void)showImagePicker:(UIImagePickerControllerSourceType)sourceType sender:(id)sender {
    
    UIImagePickerController *controler = [[UIImagePickerController alloc] init];
    if (![UIImagePickerController isSourceTypeAvailable:sourceType]) {
        /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Permission Error" andText:@"Permission not granted"  andCancelButton:NO forAlertType:AlertFailure];
         
         [alert show];*/
        
        //loder remove
        return;
    }
    controler.sourceType = sourceType;
    controler.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        
        [self presentViewController:controler animated:YES completion:nil];
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"Picker returned successfully.");
    
    NSURL *mediaUrl;
    mediaUrl = (NSURL *)[info valueForKey:UIImagePickerControllerMediaURL];
    if (mediaUrl == nil) {
        selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerEditedImage];
        if (selectedImage == nil) {
            selectedImage= (UIImage *) [info valueForKey:UIImagePickerControllerOriginalImage];
            NSLog(@"Original image picked.");
            selectedImage = [self scaleAndRotateImage:selectedImage];
            [self.profileImageBtn setImage:selectedImage forState:UIControlStateNormal];
        }
        else {
            NSLog(@"Edited image picked.");
        }
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Image url is : %@",[mediaUrl absoluteString]);
}

- (UIImage *)scaleAndRotateImage:(UIImage *) image {
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}


- (IBAction)signUpAction:(id)sender {
     [self saveNewContact];
}
- (IBAction)selectProfilePictureAction:(id)sender {
    [self.view endEditing:YES];
    //[currentTextField resignFirstResponder];
    
    NSArray *colors = [NSArray arrayWithObjects:@"Gallery" , @"Camera", nil];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Image Source."
                                            rows:colors
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           
                                           if (selectedIndex == 1) {
                                               [self showImagePicker:UIImagePickerControllerSourceTypeCamera sender:nil];
                                               
                                           }
                                           else{
                                               [self showImagePicker:UIImagePickerControllerSourceTypePhotoLibrary sender:nil];
                                               
                                           }
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];
    

}
- (IBAction)backAction:(id)sender {
    [self performSegueWithIdentifier:@"signUptologin" sender:self];
}
@end
