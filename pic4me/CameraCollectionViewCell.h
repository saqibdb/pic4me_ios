//
//  CameraCollectionViewCell.h
//  pic4me
//
//  Created by ibuildx on 9/19/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (weak, nonatomic) IBOutlet UIImageView *ImageView;
@property (weak, nonatomic) IBOutlet UIButton *openCameraBtn;

@end
