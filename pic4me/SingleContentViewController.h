//
//  SingleContentViewController.h
//  pic4me
//
//  Created by ibuildx on 7/4/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//
#import <AVKit/AVKit.h>
#import <UIKit/UIKit.h>
#import "Photo.h"
#import "Video.h"
#import "Grid.h"
@interface SingleContentViewController : UIViewController <UITableViewDelegate , UITableViewDataSource,AVPlayerViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *commentsTableView;
- (IBAction)singleContentBackAction:(id)sender;
@property (strong) Photo *selectedPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *mainContentImage;
@property (weak, nonatomic) IBOutlet UITextField *writeCommentTxtField;
- (IBAction)doneCommentAction:(id)sender;
@property (nonatomic) AVPlayerViewController *playerViewController;
@property (weak, nonatomic) IBOutlet UITextField *writeCommntTxt;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic, strong) Grid *grid;

- (IBAction)shareAndRatingAction:(id)sender;
- (IBAction)openTemplateComments:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *BtnThumbsUp;
@property (weak, nonatomic) IBOutlet UIButton *btnThumbsDown;

@property (weak, nonatomic) IBOutlet UITableView *templateCommetnsTableView;
@property (weak, nonatomic) IBOutlet UIButton *openTemplates;
@property (weak, nonatomic) IBOutlet UIView *templateView;
@property (strong) NSMutableArray  *CommentsFound;
- (IBAction)thumbsUpAction:(UIButton *)sender;
- (IBAction)thumbsDownAction:(UIButton *)sender;

@property (strong) NSMutableArray  *thumbsUpFound;
@property (strong) NSMutableArray  *thumbsDownFound;
@property (weak, nonatomic) IBOutlet UIButton *showThumbsUpBtn;
@property (weak, nonatomic) IBOutlet UIButton *showThumbsDownBtn;
- (IBAction)showThumbsUpAction:(UIButton *)sender;
- (IBAction)showThumbsDownAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *ratingUserTableView;

@property (weak, nonatomic) IBOutlet UIView *ratingview;
@property (strong) Video *selectedVideo;
- (IBAction)hidePopAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIImageView *gridUserProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *gridUserName;

@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@property (weak, nonatomic) IBOutlet UILabel *lblComment;

@end
