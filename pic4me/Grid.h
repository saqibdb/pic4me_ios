//
//  Grid.h
//  pic4me
//
//  Created by ibuildx on 11/18/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"
#import "Photo.h"
@interface Grid : NSObject
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSMutableArray *friends;
@property (nonatomic, strong) BackendlessUser *User;
@property (nonatomic, strong) NSMutableArray *Photos;
@property (nonatomic, strong) NSMutableArray *Videos;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *comment;
@property  NSInteger style;
@property (strong, nonatomic) NSDate *created;

@property (nonatomic, strong) NSString *place;

@end
