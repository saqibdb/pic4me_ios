//
//  PhotosCollectionViewController.h
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/27/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosCollectionViewController : UICollectionViewController

@property (strong) NSMutableArray  *photosFound;

@property (strong, nonatomic) IBOutlet UICollectionView *collectioView;

@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
