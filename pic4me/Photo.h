//
//  Photo.h
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/15/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"

@interface Photo : NSObject

@property (nonatomic, strong) BackendlessUser *User;
@property (nonatomic, strong) NSString *photo;
@property (nonatomic, strong) NSString *votes;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *webImageLink;
@property (nonatomic, strong) NSString *linkDescription;

@property (nonatomic, strong) NSString *place;
@property (nonatomic, strong) NSString *linkTitle;

@end
