//
//  FolderCollectionViewCell.h
//  pic4me
//
//  Created by ibuildx on 9/19/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FolderCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *openFolderBtn;

@end
