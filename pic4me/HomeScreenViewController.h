//
//  HomeScreenViewController.h
//  pic4me
//
//  Created by ibuildx on 8/1/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeScreenViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *welcomeTextLabel;

- (IBAction)DecisionsAction:(UIButton *)sender;
- (IBAction)chatsAction:(UIButton *)sender;
- (IBAction)ArchiveAction:(UIButton *)sender;
- (IBAction)profileAction:(UIButton *)sender;



@end
