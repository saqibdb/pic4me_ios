//
//  CustomImageView.m
//  uChoose
//
//  Created by Mohit Gorakhiya on 4/21/18.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import "CustomImageView.h"
#import <AVFoundation/AVFoundation.h>

@implementation CustomImageView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
//-(void)generateThumbImage:(NSString *)filepath
//{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        
//        NSURL *url = [NSURL fileURLWithPath:filepath];
//        
//        AVAsset *asset = [AVAsset assetWithURL:url];
//        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
//        
//        imageGenerator.appliesPreferredTrackTransform = YES;
//        CMTime time = [asset duration];
//        time.value = 0;
//        [imageGenerator generateCGImagesAsynchronouslyForTimes:[NSArray arrayWithObject:[NSValue valueWithCMTime:time]] completionHandler:^(CMTime requestedTime, CGImageRef image, CMTime actualTime,AVAssetImageGeneratorResult result, NSError *error)
//         {
//             if (result == AVAssetImageGeneratorSucceeded)
//             {
//                 
//                 if(image != nil)
//                 {
//                     
//                 }
//             }
//             
//             if (result == AVAssetImageGeneratorFailed)
//             {
//                 NSLog(@"Failed with error: %@", [error localizedDescription]);
//             }
//             if (result == AVAssetImageGeneratorCancelled)
//             {
//                 NSLog(@"Canceled");
//             }
//             
//         }];
//        
//    });
//  
//}

-(void)displayIndicator
{
    activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyleWhite)];
    [activity startAnimating];
    activity.center = self.center;
    [self addSubview:activity];
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([object isKindOfClass:[AVPlayerItem class]])
    {
        AVPlayerItem *player = (AVPlayerItem *)object;
        if ([keyPath isEqualToString:@"status"])
        {
            if (player.status == AVPlayerStatusReadyToPlay)
            {
                NSLog(@"AVPlayerStatusReadyToPlay1");
                
                [player removeObserver:self forKeyPath:@"status"];
                [activity stopAnimating];
                [activity removeFromSuperview];
                [self removeFromSuperview];
            }
            else if (player.status == AVPlayerStatusFailed)
            {
                // something went wrong. player.error should contain some information
            }
        }
    }
    
    
}
@end
