//
//  ProfileViewController.m
//  pic4me
//
//  Created by ibuildx on 7/4/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//
#import "UIImageView+WebCache.h"
#import "ProfileViewController.h"
#import "ActionSheetPicker.h"
#import "UIViewController+CWPopup.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import <Contacts/Contacts.h>
#import <PermissionScope/PermissionScope-Swift.h>
@interface ProfileViewController ()
{
    NSString *genderText;
    UIImage *selectedImage;

}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    NSLog(@"ProfileViewController");
   
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    self.updateProfileAction.layer.cornerRadius = 5;
    self.updateProfileAction.clipsToBounds = YES;
    self.txtUserName.delegate = self;
    self.txtNickName.delegate = self;
    self.txtAge.delegate = self;
    self.txtGender.delegate = self;
    self.txtPassword.delegate = self;
    if ([backendless.userService.currentUser getProperty:@"name"] != [NSNull null]){
        self.txtUserName.text = [backendless.userService.currentUser getProperty:@"name"];
    }
    if ([backendless.userService.currentUser getProperty:@"age"] != [NSNull null]){
        self.txtAge.text = [backendless.userService.currentUser getProperty:@"age"];
    }
    if ([backendless.userService.currentUser getProperty:@"gender"] != [NSNull null]){
        
        
        NSLog(@"%@",[backendless.userService.currentUser getProperty:@"gender"]);
        if ([[backendless.userService.currentUser getProperty:@"gender"] isEqualToString:@"male"]) {
            [self.maleRadioImage setImage:[UIImage imageNamed:@"radioYes"]];
            [self.femailRadioImage setImage:[UIImage imageNamed:@"radioNo"]];
        }
        else{
            
            [self.maleRadioImage setImage:[UIImage imageNamed:@"radioNo"]];
            [self.femailRadioImage setImage:[UIImage imageNamed:@"radioYes"]];
        }
        
    }
    
    [self.userImage sd_setImageWithURL:[NSURL URLWithString:[backendless.userService.currentUser getProperty:@"profileImage"]]
                         placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                  options:SDWebImageRefreshCached];
    
    
    
    [self.userImage.layer setCornerRadius:self.userImage.frame.size.height/2];
    self.userImage.clipsToBounds=YES;
    self.updateProfileAction.layer.cornerRadius = 5;
    self.updateProfileAction.clipsToBounds = YES;
  
    
//    self.multiPscope = [[PermissionScope alloc]init];
//    
//    [self.multiPscope addPermission:[[ContactsPermission alloc]init] message:@"."];
//    [self.multiPscope addPermission:[[LocationWhileInUsePermission alloc]init] message:@"."];
//    [self.multiPscope addPermission:[[PhotosPermission alloc]init] message:@"."];
//    //[self.multiPscope addPermission:[[CameraPermission alloc]init] message:@"."];
    

    
   
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
-(void)viewDidAppear:(BOOL)animated{
    
//    static dispatch_once_t onceToken;
//       dispatch_once (&onceToken, ^{
//        [self.multiPscope show:^(BOOL completed, NSArray *results) {
//            NSLog(@"Changed: %@ - %@", @(completed), results);
//        } cancelled:^(NSArray *x) {
//            NSLog(@"cancelled");
//        }];
//    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)unwindToProfileUpdate:(UIStoryboardSegue*)sender
{
    
}
- (IBAction)updateProfileAction:(id)sender {
    
    
    if(!selectedImage){
        [self updateProfile:nil];
    }
    else{
    NSLog(@"\n============ Uploading file  with the ASYNC API ============");
    
    int REQUIRED_SIZE = 100;
    selectedImage = [self getScaledToRequiredSizedImage:REQUIRED_SIZE :selectedImage];
    
    NSData *imageData = UIImageJPEGRepresentation(selectedImage, 1.0);
    
    [SVProgressHUD showWithStatus:@"updating User..."];
    [backendless.fileService upload:[NSString stringWithFormat:@"myfiles/%ld.jpg", arc4random() % 100000000000]  content:imageData
                           response:^(BackendlessFile *uploadedFile) {
                               NSLog(@"File has been uploaded. File URL is - %@", uploadedFile.fileURL);
                               
                               [self updateProfile:uploadedFile.fileURL];
                               
                           }
                              error:^(Fault *fault) {
                                  NSLog(@"Server reported an error: %@", fault);
                                  
                                  
                                  NSLog(@"FAULT = %@ <%@>", fault.message, fault.detail);
                                  AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:fault.message andCancelButton:NO forAlertType:AlertFailure];
                                  
                                  [alert show];
                                  
                                  [SVProgressHUD dismiss];
                                  
                              }];
    }
}

-(void)updateProfile:(NSString *)imageUrl{
    
    [SVProgressHUD showWithStatus:@"updating User..."];
    BackendlessUser *updateUser = backendless.userService.currentUser ;
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    
    [updateUser setProperty:@"name" object:self.txtUserName.text];
    [updateUser setProperty:@"age" object:self.txtAge.text];
    [updateUser setProperty:@"gender" object:genderText];
    if(self.txtPassword.text.length > 0){
    [updateUser setProperty:@"password" object:self.txtPassword.text];
    }
    if(selectedImage){
    
    [updateUser setProperty:@"profileImage" object:[NSString stringWithFormat:@"%@", imageUrl]];}
    
    [dataStore save:updateUser response:^(BackendlessUser *updateUser) {
        [SVProgressHUD dismiss];
        backendless.userService.currentUser = updateUser;
        self.txtPassword.text = @"";
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@" " andText:@"User Updated Successfully" andCancelButton:NO forAlertType:AlertSuccess];
        
        [alert show];
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        
        NSString *errorMsg = [NSString stringWithFormat:@"Failed to Update. Details = %@", error.description];
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];
    }];



}
-(UIImage *)getScaledToRequiredSizedImage :(int)requiredSize :(UIImage *)sentImage{
    int scale = 1;
    
    while (sentImage.size.width / scale / 2 >= requiredSize
           && sentImage.size.height / scale / 2 >= requiredSize)
        scale *= 2;
    
    return [self imageWithImage:selectedImage scaledToSize:CGSizeMake(selectedImage.size.width/scale, selectedImage.size.height/scale)];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (IBAction)profileBackAction:(id)sender {
   [self performSegueWithIdentifier:@"profileToHomeBack" sender:self];
}

- (IBAction)cangeProfileAction:(UIButton *)sender {
    NSArray *colors = [NSArray arrayWithObjects:@"Gallery" , @"Camera", nil];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Image Source."
                                            rows:colors
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           
                                           if (selectedIndex == 1) {
                                               [self showImagePicker:UIImagePickerControllerSourceTypeCamera sender:nil];
                                               
                                           }
                                           else{
                                               [self showImagePicker:UIImagePickerControllerSourceTypePhotoLibrary sender:nil];
                                               
                                           }
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];
    

}
- (void)showImagePicker:(UIImagePickerControllerSourceType)sourceType sender:(id)sender {
    
    UIImagePickerController *controler = [[UIImagePickerController alloc] init];
    if (![UIImagePickerController isSourceTypeAvailable:sourceType]) {
        /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Permission Error" andText:@"Permission not granted"  andCancelButton:NO forAlertType:AlertFailure];
         
         [alert show];*/
        
        //loder remove
        return;
    }
    controler.sourceType = sourceType;
    controler.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        
        [self presentViewController:controler animated:YES completion:nil];
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"Picker returned successfully.");
    
    NSURL *mediaUrl;
    mediaUrl = (NSURL *)[info valueForKey:UIImagePickerControllerMediaURL];
    if (mediaUrl == nil) {
        selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerEditedImage];
        if (selectedImage == nil) {
            selectedImage= (UIImage *) [info valueForKey:UIImagePickerControllerOriginalImage];
            NSLog(@"Original image picked.");
            selectedImage = [self scaleAndRotateImage:selectedImage];
            [self.userImage setImage:selectedImage];
        }
        else {
            NSLog(@"Edited image picked.");
        }
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Image url is : %@",[mediaUrl absoluteString]);
}

- (UIImage *)scaleAndRotateImage:(UIImage *) image {
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}


- (IBAction)genderRadioAction:(UIButton *)sender {
    if (sender.tag == 1) {
        genderText = @"male";
        [self.maleRadioImage setImage:[UIImage imageNamed:@"radioYes"]];
        [self.femailRadioImage setImage:[UIImage imageNamed:@"radioNo"]];
    }
    else{
        genderText = @"female";
        [self.maleRadioImage setImage:[UIImage imageNamed:@"radioNo"]];
        [self.femailRadioImage setImage:[UIImage imageNamed:@"radioYes"]];
    }

}
@end
