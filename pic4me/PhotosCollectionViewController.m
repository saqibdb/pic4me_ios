//
//  PhotosCollectionViewController.m
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/27/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "PhotosCollectionViewController.h"
//#import "AMSmoothAlertView.h"
//#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "Backendless.h"
#import "Photo.h"
#import "PhotoCollectionViewCell.h"
@interface PhotosCollectionViewController ()

@end

@implementation PhotosCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fetchPhotos];
    self.collectioView.dataSource=self;
    self.collectioView.delegate=self;
    
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)fetchPhotos{
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"User.objectId = '%@' ",backendless.userService.currentUser.objectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Photo class]];
    [dataStore find:query response:^(BackendlessCollection *photoColection) {
        NSLog(@"photos found = %lu",(unsigned long)photoColection.data.count);
        
        self.photosFound = [[NSMutableArray alloc] initWithArray:photoColection.data];
        //[SVProgressHUD dismiss];
     
    } error:^(Fault *error) {
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
}

#pragma mark <UICollectionViewDataSource>


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photosFound.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    //recipeImageView.image = [UIImage imageNamed:[self.photosFound objectAtIndex:indexPath.row]];
    
    Photo *photo =[self.photosFound objectAtIndex:indexPath.row];
    [recipeImageView sd_setImageWithURL:[NSURL URLWithString:photo.photo]
                   placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                            options:SDWebImageRefreshCached];
    
    return cell;
}
#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
