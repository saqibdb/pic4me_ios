//
//  LocationPopUp.h
//  pic4me
//
//  Created by ibuildx on 11/17/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationPopUp : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *radioAddLocation;
@property (weak, nonatomic) IBOutlet UIButton *radioUseGps;
@property (weak, nonatomic) IBOutlet UIImageView *imgAddLocation;
@property (weak, nonatomic) IBOutlet UIImageView *imgUseGps;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UITextField *txtLocation;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;


@end
