//
//  ViewController.h
//  pic4me
//
//  Created by ibuildx on 7/3/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>

@property BOOL isLogin ;

@property BOOL isBrowsing ;

@property (weak, nonatomic) IBOutlet UITextField *loginEmailTxt;
@property (weak, nonatomic) IBOutlet UITextField *loginPasswordTxt;
@property (weak, nonatomic) IBOutlet UIButton *loginForgetPasswordBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;


@property (weak, nonatomic) IBOutlet UIView *signUpView;


@property (weak, nonatomic) IBOutlet UITextField *signUpEmailTxt;

@property (weak, nonatomic) IBOutlet UITextField *signUpPasswordTxt;
@property (weak, nonatomic) IBOutlet UITextField *signUpPasswordReTxt;

@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;

@property (weak, nonatomic) IBOutlet UIButton *loginTabBtn;

@property (weak, nonatomic) IBOutlet UIButton *signUpTabBtn;

@property (weak, nonatomic) IBOutlet UIView *loginSprite;

@property (weak, nonatomic) IBOutlet UIView *signUpSprite;


@property (weak, nonatomic) IBOutlet UIButton *learnMoreBtn;



@property (weak, nonatomic) IBOutlet UIButton *browseBtn;
@property (weak, nonatomic) IBOutlet UIButton *backbtn;
@property (weak, nonatomic) IBOutlet UIButton *facebookLoginBtn;
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)loginAction:(UIButton *)sender;
- (IBAction)forgotPasswordAction:(UIButton *)sender;
- (IBAction)loginPasswordAction:(UIButton *)sender;
- (IBAction)signUpAction:(UIButton *)sender;
- (IBAction)learnMoreAction:(UIButton *)sender;
- (IBAction)loginTabAction:(UIButton *)sender;
- (IBAction)signUpTabAction:(UIButton *)sender;
- (IBAction)browseAction:(UIButton *)sender;
- (IBAction)facebookLoginAction:(id)sender;
- (IBAction)signUpNewAction:(id)sender;

@end

