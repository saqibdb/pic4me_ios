//
//  SettingsViewController.h
//  pic4me
//
//  Created by ibuildx on 8/4/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
@interface SettingsViewController : UIViewController<CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *userProfile;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UISwitch *locationSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *notificationSwitch;
- (IBAction)locationSwitchAction:(UISwitch *)sender;
- (IBAction)notificationSwitchAction:(UISwitch *)sender;
@property (weak, nonatomic) IBOutlet UIView *changePasswordView;
@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
- (IBAction)changePasswordAction:(UIButton *)sender;
- (IBAction)cancelChangeAction:(UIButton *)sender;
- (IBAction)openChangePasswordPopUp:(UIButton *)sender;

- (IBAction)backAction:(UIButton *)sender;
- (IBAction)logoutAction:(id)sender;

@end
