//
//  ViewController.m
//  NewChatBubble
//
//  Created by Siba Prasad Hota  on 1/3/15.
//  Copyright (c) 2015 Wemakeappz. All rights reserved.
//
#import "UIImageView+WebCache.h"
#import "FlatChatViewController.h"
#import "SPHTextBubbleCell.h"
#import "SPHMediaBubbleCell.h"
#import "Constantvalues.h"
#import "SPH_PARAM_List.h"
#import "SVProgressHUD.h"

#import "AMSmoothAlertView.h"

#import "Inbox.h"




@interface FlatChatViewController ()<TextCellDelegate,MediaCellDelegate>
{
    NSMutableArray *sphBubbledata;
    BOOL isfromMe;
}

@end

@implementation FlatChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isfromMe=YES;
    sphBubbledata =[[NSMutableArray alloc]init];
    
    self.chattable.backgroundColor = [UIColor clearColor];
    
    //[self SetupHistoryMessages];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.chattable addGestureRecognizer:tap];
    self.chattable.backgroundColor =[UIColor clearColor];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.messageField.leftView = paddingView;
    self.messageField.leftViewMode = UITextFieldViewModeAlways;
    
    
    self.userNameText.text = self.selectedUser.name ;
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"MessageREcieved"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
//    [self performSelector:@selector(refreshChat) withObject:nil afterDelay:1.5 ];
    
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshChat];
}
-(void)refreshChat
{
    [self SetupHistoryMessages];
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    self.textFieldBottomConstraint.constant = 0;
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGFloat height = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    self.textFieldBottomConstraint.constant = height;
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}


- (void) receiveTestNotification:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    NSString *alertMessage = [[notification.userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    
    [self adddMediaBubbledata:kTextByOther mediaPath:alertMessage mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];
    [self.chattable reloadData];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark tableview delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return sphBubbledata.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SPH_PARAM_List *feed_data=[[SPH_PARAM_List alloc]init];
    feed_data=[sphBubbledata objectAtIndex:indexPath.row];
    
    if ([feed_data.chat_media_type isEqualToString:kImagebyme]||[feed_data.chat_media_type isEqualToString:kImagebyOther])  return 180;
    
    CGSize labelSize =[feed_data.chat_message boundingRectWithSize:CGSizeMake(226.0f, MAXFLOAT)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{ NSFontAttributeName:[UIFont systemFontOfSize:14.0f] }
                                            context:nil].size;
    return labelSize.height + 30 + TOP_MARGIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *L_CellIdentifier = @"SPHTextBubbleCell";
    static NSString *R_CellIdentifier = @"SPHMediaBubbleCell";
    
    SPH_PARAM_List *feed_data=[[SPH_PARAM_List alloc]init];
    feed_data=[sphBubbledata objectAtIndex:indexPath.row];
    
    if ([feed_data.chat_media_type isEqualToString:kTextByme]||[feed_data.chat_media_type isEqualToString:kTextByOther])
    {
        SPHTextBubbleCell *cell = (SPHTextBubbleCell *) [tableView dequeueReusableCellWithIdentifier:L_CellIdentifier];
        if (cell == nil)
        {
            cell = [[SPHTextBubbleCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:L_CellIdentifier];
        }
        cell.bubbletype=([feed_data.chat_media_type isEqualToString:kTextByme])?@"LEFT":@"RIGHT";
        
        
        // Decode
        NSString *companyStr = feed_data.chat_message;
        NSString *trimmedString2 = [companyStr stringByTrimmingCharactersInSet:
                                    [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSData *decodedCompany = [[NSData alloc] initWithBase64EncodedString:trimmedString2 options:0];
        NSString *msg = [[NSString alloc] initWithData:decodedCompany encoding:NSUTF8StringEncoding];
        //
        
        
        cell.textLabel.text = msg;
        cell.textLabel.tag=indexPath.row;
        cell.timestampLabel.text = feed_data.chat_date_time;
        cell.CustomDelegate=self;
        
        

        if ([feed_data.chat_media_type isEqualToString:kTextByme]) {
            
            [cell.AvatarImageView sd_setImageWithURL:[NSURL URLWithString:[backendless.userService.currentUser getProperty:@"profileImage"]]
                                    placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                             options:SDWebImageRefreshCached];
        }
        else
        {
            [cell.AvatarImageView sd_setImageWithURL:[NSURL URLWithString:[self.selectedUser getProperty:@"profileImage"]]
                              placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                       options:SDWebImageRefreshCached];
            
        }
        
        
        //cell.AvatarImageView.image=([feed_data.chat_media_type isEqualToString:kTextByme])?[UIImage imageNamed:@"ProfilePic"]:[UIImage imageNamed:@"person"];
        return cell;

    }
    
    SPHMediaBubbleCell *cell = (SPHMediaBubbleCell *) [tableView dequeueReusableCellWithIdentifier:R_CellIdentifier];
    if (cell == nil)
    {
        cell = [[SPHMediaBubbleCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:R_CellIdentifier];
    }
    cell.bubbletype=([feed_data.chat_media_type isEqualToString:kImagebyme])?@"LEFT":@"RIGHT";
    cell.textLabel.text = feed_data.chat_message;
    cell.messageImageView.tag=indexPath.row;
    cell.CustomDelegate=self;
    cell.timestampLabel.text = @"02:20 AM";
    
    if ([feed_data.chat_media_type isEqualToString:kImagebyme]) {
        [cell.AvatarImageView sd_setImageWithURL:[NSURL URLWithString:[backendless.userService.currentUser getProperty:@"profileImage"]]
                                placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                         options:SDWebImageRefreshCached];
    }
    else
    {
        [cell.AvatarImageView sd_setImageWithURL:[NSURL URLWithString:[self.selectedUser getProperty:@"profileImage"]]
                                placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                         options:SDWebImageRefreshCached];
    }
    
    
    //cell.AvatarImageView.image=([feed_data.chat_media_type isEqualToString:kImagebyme])?[UIImage imageNamed:@"ProfilePic"]:[UIImage imageNamed:@"person"];
    
    return cell;
}



//=========***************************************************=============
#pragma mark - CELL CLICKED  PROCEDURE
//=========***************************************************=============


-(void)textCellDidTapped:(SPHTextBubbleCell *)tesxtCell AndGesture:(UIGestureRecognizer*)tapGR;
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:tesxtCell.textLabel.tag inSection:0];
    NSLog(@"Forward Pressed =%@ and IndexPath=%@",tesxtCell.textLabel.text,indexPath);
    [tesxtCell showMenu];
}
// 7684097905

-(void)cellCopyPressed:(SPHTextBubbleCell *)tesxtCell
{
    NSLog(@"copy Pressed =%@",tesxtCell.textLabel.text);
    
}

-(void)cellForwardPressed:(SPHTextBubbleCell *)tesxtCell
{
    NSLog(@"Forward Pressed =%@",tesxtCell.textLabel.text);
    
}
-(void)cellDeletePressed:(SPHTextBubbleCell *)tesxtCell
{
    NSLog(@"Delete Pressed =%@",tesxtCell.textLabel.text);
    
}

//=========*******************  BELOW FUNCTIONS FOR IMAGE  **************************=============

-(void)mediaCellDidTapped:(SPHMediaBubbleCell *)mediaCell AndGesture:(UIGestureRecognizer*)tapGR;
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:mediaCell.messageImageView.tag inSection:0];
    NSLog(@"Media cell Pressed  and IndexPath=%@",indexPath);

    [mediaCell showMenu];
}

-(void)mediaCellCopyPressed:(SPHMediaBubbleCell *)mediaCell
{
    NSLog(@"copy Pressed =%@",mediaCell.messageImageView.image);
    
}

-(void)mediaCellForwardPressed:(SPHMediaBubbleCell *)mediaCell
{
    NSLog(@"Forward Pressed =%@",mediaCell.messageImageView.image);
   
}
-(void)mediaCellDeletePressed:(SPHMediaBubbleCell *)mediaCell
{
    NSLog(@"Delete Pressed =%@",mediaCell.messageImageView.image);
   
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark               KEYBOARD UPDOWN EVENT
/////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (sphBubbledata.count>2) {
        [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.0];
    }
    CGRect tableviewframe=self.chattable.frame;
    tableviewframe.size.height-=210;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.msgInPutView.frame=CGRectMake(0,self.view.frame.size.height-265, self.view.frame.size.width, 50);
        self.chattable.frame=tableviewframe;
    }];
    
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect tableviewframe=self.chattable.frame;
    tableviewframe.size.height+=210;
    [UIView animateWithDuration:0.25 animations:^{
        self.msgInPutView.frame=CGRectMake(0,self.view.frame.size.height-50,  self.view.frame.size.width, 50);
        self.chattable.frame=tableviewframe;  }];
    if (sphBubbledata.count>2) {
        [self performSelector:@selector(scrollTableview) withObject:nil afterDelay:0.25];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}






/////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark       SEND MESSAGE PRESSED   
/////////////////////////////////////////////////////////////////////////////////////////////////////

- (IBAction)sendMessageNow:(id)sender
{
    
    [self.messageField resignFirstResponder];
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
    if ([self.messageField.text length]>0) {
        
        // Encode a message string
        NSData *plainData = [self.messageField.text dataUsingEncoding:NSUTF8StringEncoding];
        NSString *txtMessage = [plainData base64EncodedStringWithOptions:0];
        //
        
        
        
        NSString *rowNum=[NSString stringWithFormat:@"%d",(int)sphBubbledata.count];
        [self adddMediaBubbledata:kTextByme mediaPath:txtMessage mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSending msg_ID:[self genRandStringLength:7]];
        [self performSelector:@selector(messageSent:) withObject:rowNum afterDelay:1];
        
        
        
        Inbox *newInbox = [[Inbox alloc] init] ;
        newInbox.messageBody = txtMessage ;
        newInbox.Sender = backendless.userService.currentUser ;
        newInbox.Reciever = self.selectedUser ;
        newInbox.isSeen = @"NO" ;
        newInbox.AppTime = [formatter stringFromDate:date] ;
        id<IDataStore> dataStore = [backendless.persistenceService of:[Inbox class]];
        [dataStore save:newInbox response:^(id savedInbox) {
            [self publishMessageAsPushNotificationAsync:self.messageField.text forDevice:[self.selectedUser getProperty:@"deviceId"]];
        } error:^(Fault *error) {
            NSString *errorMsg = [NSString stringWithFormat:@"something went wrong. Details = %@", error.description];
            NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
            [sphBubbledata removeLastObject];
            [self.chattable reloadData];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }];
        self.messageField.text=@"";
        [self.chattable reloadData];
        [self scrollTableview];
    }
}

- (IBAction)backAction:(UIButton *)sender {
    
    NSLog(@"clicked");
    
    [self performSegueWithIdentifier:@"backToAllChats" sender:sender];

}

/////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)messageSent:(NSString*)rownum
{
    int rowID=[rownum intValue];
    
    SPH_PARAM_List *feed_data=[[SPH_PARAM_List alloc]init];
    feed_data=[sphBubbledata objectAtIndex:rowID];
    
    [sphBubbledata  removeObjectAtIndex:rowID];
    feed_data.chat_send_status=kSent;
    [sphBubbledata insertObject:feed_data atIndex:rowID];
    
    // [self.chattable reloadData];
    
    NSArray *indexPaths = [NSArray arrayWithObjects:
                           [NSIndexPath indexPathForRow:rowID inSection:0],
                           // Add some more index paths if you want here
                           nil];
    BOOL animationsEnabled = [UIView areAnimationsEnabled];
    [UIView setAnimationsEnabled:NO];
    [self.chattable reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [UIView setAnimationsEnabled:animationsEnabled];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)scrollTableview
{
    
    NSInteger item = [self.chattable numberOfRowsInSection:0] - 1;
    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForItem:item inSection:0];
    [self.chattable scrollToRowAtIndexPath:lastIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}


/////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)adddMediaBubbledata:(NSString*)mediaType  mediaPath:(NSString*)mediaPath mtime:(NSString*)messageTime thumb:(NSString*)thumbUrl  downloadstatus:(NSString*)downloadstatus sendingStatus:(NSString*)sendingStatus msg_ID:(NSString*)msgID
{
    
    SPH_PARAM_List *feed_data=[[SPH_PARAM_List alloc]init];
    feed_data.chat_message=mediaPath;
    feed_data.chat_date_time=messageTime;
    feed_data.chat_media_type=mediaType;
    feed_data.chat_send_status=sendingStatus;
    feed_data.chat_Thumburl=thumbUrl;
    feed_data.chat_downloadStatus=downloadstatus;
    feed_data.chat_messageID=msgID;
    [sphBubbledata addObject:feed_data];
}


/////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark  GENERATE RANDOM ID to SAVE IN LOCAL
/////////////////////////////////////////////////////////////////////////////////////////////////////


-(NSString *) genRandStringLength: (int) len {
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    
    return randomString;
}



/////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark       SETUP DUMMY MESSAGE / REPLACE THEM IN LIVE
/////////////////////////////////////////////////////////////////////////////////////////////////////
-(void)sortByDate :(NSArray *)comments completion:(void (^)(NSArray *arraySorted))completion __attribute__((nonnull(2)));
{
    NSMutableArray *commentsArray = [[NSMutableArray alloc] initWithArray:comments];
    
    NSArray *sortedLocations = [commentsArray sortedArrayUsingComparator: ^(Inbox *a1, Inbox *a2) {
        
        return [a1.created compare:a2.created];
    }];
    completion(sortedLocations);
}

-(void)SetupHistoryMessages
{
    [SVProgressHUD show];
    BackendlessDataQuery *query = [BackendlessDataQuery query];
//    NSLog(@"%@",self.selectedUser.objectId);
//    NSLog(@"%@",backendless.userService.currentUser.objectId);
    
    query.whereClause = [NSString stringWithFormat:@"((Reciever.objectId = '%@' AND Sender.objectId = '%@') OR (Reciever.objectId = '%@' AND Sender.objectId = '%@')) ", self.selectedUser.objectId, backendless.userService.currentUser.objectId, backendless.userService.currentUser.objectId, self.selectedUser.objectId];
    [backendless.persistenceService find:[Inbox class] dataQuery:query response:^(BackendlessCollection *collection1)  {
        NSLog(@"Messgaes Found %i", [[collection1 getTotalObjects] intValue]);
        
        
        [self sortByDate:collection1.data completion:^(NSArray *arraySorted) {
            for (Inbox *inbox in arraySorted) {
                
                if ([inbox.Sender.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
                    
                    [self adddMediaBubbledata:kTextByme mediaPath:inbox.messageBody mtime:inbox.AppTime thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];
                }
                else{
                    [self adddMediaBubbledata:kTextByOther mediaPath:inbox.messageBody mtime:inbox.AppTime thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];
                }
            }

            [self.chattable reloadData];
            [SVProgressHUD dismiss];

        }];
        
    } error:^(Fault *error) {
        NSLog(@"Inbox Found %@",error.description);
        [SVProgressHUD dismiss];
    }];

    
}
-(void)SetupDummyMessages
{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    
    //  msg_ID  Any Random ID
    
    //  mediaPath  : Your Message  or  Path of the Image
    
    [self adddMediaBubbledata:kTextByme mediaPath:@"Hi, check this new control!" mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];
    //[self performSelector:@selector(messageSent:) withObject:@"0" afterDelay:1];
    
    [self adddMediaBubbledata:kTextByOther mediaPath:@"Hello! How are you?" mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];
    
    /*[self adddMediaBubbledata:kTextByme mediaPath:@"I'm doing Great!" mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];
    
    [self adddMediaBubbledata:kImagebyme mediaPath:@"ImageUrl" mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];
    
    [self adddMediaBubbledata:kImagebyOther mediaPath:@"Yeah its cool!" mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];
    
    [self adddMediaBubbledata:kTextByme mediaPath:@"Supports Image too." mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];
    
    [self adddMediaBubbledata:kTextByOther mediaPath:@"Yup. I like the tail part of it." mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];
    
    [self adddMediaBubbledata:kImagebyme mediaPath:@"ImageUrl" mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSending msg_ID:@"ABFCXYZ"];
    
    [self adddMediaBubbledata:kImagebyOther mediaPath:@"Hi, check this new control!" mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];
    [self adddMediaBubbledata:kTextByme mediaPath:@"lets meet some time for dinner! hope you will like it." mtime:[formatter stringFromDate:date] thumb:@"" downloadstatus:@"" sendingStatus:kSent msg_ID:[self genRandStringLength:7]];*/
    
    
    
    [self.chattable reloadData];
}
-(void)publishMessageAsPushNotificationAsync:(NSString *)message forDevice:(NSString *)deviceId
{
    DeliveryOptions *deliveryOptions = [DeliveryOptions new];
    deliveryOptions.pushSinglecast = [@[deviceId] mutableCopy];
    
    PublishOptions *publishOptions = [PublishOptions new];
    //publishOptions.headers = @{@"android-content-title":@"Notification title for Android",@"android-content-text":@"Notification text for Android"};
    [backendless.messagingService
     publish:@"default" message:message publishOptions:publishOptions
     deliveryOptions:deliveryOptions
     response:^(MessageStatus *messageStatus) {
         NSLog(@"MessageStatus = %@ <%@>", messageStatus.messageId, messageStatus.status);
     }
     error:^(Fault *fault) {
         NSLog(@"FAULT = %@", fault);
         [sphBubbledata removeLastObject];
         [self.chattable reloadData];
         AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Message Failed!" andText:[NSString stringWithFormat:@"%@",fault.description] andCancelButton:NO forAlertType:AlertInfo];
         [alert show];
     }
     ];
}




@end
