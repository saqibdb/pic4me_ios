//
//  ShareGridsViewController.m
//  pic4me
//
//  Created by ibuildx on 11/17/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <Social/Social.h>
#import "ShareGridsViewController.h"
#import "ASJTagsView.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "LocationPopUp.h"
#import "UIViewController+CWPopup.h"
#import<CoreLocation/CoreLocation.h>
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "Photo.h"
#import<UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "ASJTagsView.h"
#import "SharedViewController.h"
#import "BrowserViewController.h"
#import "Video.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVBase.h>
#import <Foundation/Foundation.h>
#import <CoreMedia/CMTime.h>
#import <CoreGraphics/CoreGraphics.h>
#import <INTULocationManager/INTULocationManager.h>
#import <Contacts/Contacts.h>
#import "UIImageView+WebCache.h"
#import <CCMBorderView/CCMBorderView.h>
#import "LinkPreview.h"
#import "CustomCardView.h"

@interface ShareGridsViewController (){
    
    NSString *filePath;
    NSMutableArray *imagesAndUrls ;
    NSMutableArray *tempArray ;
    NSString *location ;
    LocationPopUp *locationPopUp;
    NSMutableArray *imagesToSave ;
    UIImage *selectedImage;
    NSMutableArray *gridImageUrls ;
    //CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    Grid *grid;
    Grid *gridToSend;
    NSMutableArray *tagsArray;
    NSMutableArray *VideosArray ;
    UIButton           *playBTN;
    NSURL           *VideoUrl;
    NSUInteger count ;
    BOOL gpsLocation;
    NSUInteger PlayerViewTagCount;
    NSMutableArray *cropViewsForCropping;
    NSMutableArray *AllPlayerViewControllers;
    NSMutableArray *CopyAllPlayerViewControllers1;
    NSMutableArray *CopyAllPlayerViewControllers2;
    NSMutableArray *CopyAllPlayerViewControllers3;
    NSMutableArray *CopyAllPlayerViewControllers4;
    NSMutableArray *CopyAllPlayerViewControllers5;
    NSMutableArray *CopyAllPlayerViewControllers6;
    
    AVPlayerViewController * playerViewController ;
    BOOL isEdited;
    NSMutableArray *linksArray ;

}

@end

@implementation ShareGridsViewController
@synthesize imageURLArray;
- (void)viewDidLoad {
//    NSLog(@"ShareGridsViewController");
    [super viewDidLoad];
    
    self.commentTextView.minHeight = 33;
    self.commentTextView.maxHeight = 78;
    isEdited = NO;
    CopyAllPlayerViewControllers1 =[[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"", nil];
    CopyAllPlayerViewControllers2 =[[NSMutableArray alloc] initWithObjects:@"",@"",@"", nil];
    CopyAllPlayerViewControllers3 =[[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"", nil];
    CopyAllPlayerViewControllers4 =[[NSMutableArray alloc] initWithObjects:@"",@"",@"", nil];
    CopyAllPlayerViewControllers5 =[[NSMutableArray alloc] initWithObjects:@"",@"", nil];
    CopyAllPlayerViewControllers6 =[[NSMutableArray alloc] initWithObjects:@"", nil];
    
    [self setRoundRectTrashBtn];
    self.imageURLArray = [[NSMutableArray alloc] init];
    self.imageTempURLs = [[NSMutableArray alloc] init];
    imagesToSave = [[NSMutableArray alloc] initWithCapacity:4];
    
    self.imageURLsToSave1 = [[NSMutableArray alloc] initWithCapacity:4];
     self.playerViewController.delegate = self;
    int strt = 4;
    PlayerViewTagCount = 0;
    self.ContainerView_a.hidden = strt != 0 ;
    self.ContainerView_b.hidden = strt != 1 ;
    self.ContainerView_c.hidden = strt != 2 ;
    self.ContainerView_d.hidden = strt != 3 ;
    self.ContainerView_e.hidden = strt != 4 ;
    self.ContainerView_f.hidden = strt != 5 ;
    self.senderTag = 4;
    
    [self.gridUserProfileImage.layer setCornerRadius:self.gridUserProfileImage.frame.size.height/2];
    self.gridUserProfileImage.clipsToBounds=YES;
    self.gridUserProfileImage.layer.borderWidth=1.5;
    self.gridUserProfileImage.layer.borderColor=[[UIColor blackColor] CGColor];
    
    [self.gridUserProfileImage sd_setImageWithURL:[NSURL URLWithString:[backendless.userService.currentUser getProperty:@"profileImage"]]
                        placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                 options:SDWebImageRefreshCached];
    
    self.gridUserName.text=[NSString stringWithFormat:@"%@",backendless.userService.currentUser.name];
    self.lblTime.text = @"now";
    self.lblLocation1.text = @"";
    AllPlayerViewControllers =[[NSMutableArray alloc] initWithObjects:@"",@"", nil];
    
    tagsArray=[[NSMutableArray alloc] init];
    imagesAndUrls=[[NSMutableArray alloc] init];
    cropViewsForCropping = [[NSMutableArray alloc] initWithObjects:@"",@"", nil];
    self.imageTempURLs = [[NSMutableArray alloc] initWithObjects:@"",@"", nil];
    [self setup];
    [self.lblLocation setHidden:YES];
    
    [self.shareBtn.layer setCornerRadius:self.shareBtn.frame.size.height/2];
    
    self.dragAndDropController = [[DNDDragAndDropController alloc] init];
    for (UIView *dropTargetViewa in self.dropTargetView_a) {
        [self.dragAndDropController registerDropTarget:dropTargetViewa withDelegate:self];
    }
    for (UIView *dropTargetViewb in self.dropTargetView_b) {
        [self.dragAndDropController registerDropTarget:dropTargetViewb withDelegate:self];
    }
    for (UIView *dropTargetViewc in self.dropTargetView_c) {
        [self.dragAndDropController registerDropTarget:dropTargetViewc withDelegate:self];
    }
    for (UIView *dropTargetViewd in self.dropTargetView_d) {
        [self.dragAndDropController registerDropTarget:dropTargetViewd withDelegate:self];
    }
    for (UIView *dropTargetViewe in self.dropTargetView_e) {
        [self.dragAndDropController registerDropTarget:dropTargetViewe withDelegate:self];
    }
    for (UIView *dropTargetViewf in self.dropTarget_f) {
        [self.dragAndDropController registerDropTarget:dropTargetViewf withDelegate:self];
        
    }
    for (UIView *dragSources in self.dragSourceView) {
        [self.dragAndDropController registerDragSource:dragSources withDelegate:self];
        [dragSources setHidden:YES];
    }
    
    if(self.chosenImages.count>0){
        [self.selectedImagesView setHidden:NO];
        [imagesAndUrls addObjectsFromArray:_chosenImages];
        [self setImagesInDragSources ];
        for(int i = 0; i<self.chosenImages.count ; i++)
        {
            [self.imageURLArray addObject:@""];
        }
    }
    
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.gridSelectionViewBottom.constant=-self.gridSelectionView.frame.size.height;
    [self showTrashButtons];
    self.captionView.hidden = YES;
    
    self.commentTextView.delegate = self;
}

-(void)setRoundRectTrashBtn {
//    self.btnTrash.layer.cornerRadius = 15;
//    self.btnTrash.clipsToBounds = YES;

    self.btnTrash2.layer.cornerRadius = 15;
    self.btnTrash2.clipsToBounds = YES;

    self.btnTrash3.layer.cornerRadius = 15;
    self.btnTrash3.clipsToBounds = YES;

    self.btnTrash4.layer.cornerRadius = 15;
    self.btnTrash4.clipsToBounds = YES;

    self.btnTrash5.layer.cornerRadius = 15;
    self.btnTrash5.clipsToBounds = YES;

    self.btnTrash6.layer.cornerRadius = 15;
    self.btnTrash6.clipsToBounds = YES;

    self.btnTrash7.layer.cornerRadius = 15;
    self.btnTrash7.clipsToBounds = YES;

    self.btnTrash8.layer.cornerRadius = 15;
    self.btnTrash8.clipsToBounds = YES;

    self.btnTrash9.layer.cornerRadius = 15;
    self.btnTrash9.clipsToBounds = YES;

    self.btnTrash10.layer.cornerRadius = 15;
    self.btnTrash10.clipsToBounds = YES;

    self.btnTrash11.layer.cornerRadius = 15;
    self.btnTrash11.clipsToBounds = YES;

    self.btnTrash12.layer.cornerRadius = 15;
    self.btnTrash12.clipsToBounds = YES;

    self.btnTrash13.layer.cornerRadius = 15;
    self.btnTrash13.clipsToBounds = YES;

    self.btnTrash14.layer.cornerRadius = 15;
    self.btnTrash14.clipsToBounds = YES;

    self.btnTrash15.layer.cornerRadius = 15;
    self.btnTrash15.clipsToBounds = YES;

    self.btnTrash16.layer.cornerRadius = 15;
    self.btnTrash16.clipsToBounds = YES;

    self.btnTrash17.layer.cornerRadius = 15;
    self.btnTrash17.clipsToBounds = YES;

}

-(void)imagesCount:(NSUInteger )imgCounts {
    
    if (imgCounts > 6) {
         [imagesAndUrls removeLastObject];
        
        [self.imageURLArray removeLastObject];
         [self imagesCount:imagesAndUrls.count];
    }
}

-(void)setImagesInDragSources{
    
    if (imagesAndUrls.count>6)
    {
        
        NSUInteger imgCount = imagesAndUrls.count;
        [self imagesCount:imgCount];
    }
    
//    if (imagesAndUrls.count == 6 )
    {
        [self.btnTrashSelectedImg6 setHidden:NO];
        [self.btnTrashSelectedImg5 setHidden:NO];
        [self.btnTrashSelectedImg4 setHidden:NO];
        [self.btnTrashSelectedImg3 setHidden:NO];
        [self.btnTrashSelectedImg2 setHidden:NO];
        [self.btnTrashSelectedImg setHidden:NO];
        
        [self.btnTrashCorner setHidden:NO];
        [self.btnTrashCorner2 setHidden:NO];
        [self.btnTrashCorner3 setHidden:NO];
        [self.btnTrashCorner4 setHidden:NO];
        [self.btnTrashCorner5 setHidden:NO];
        [self.btnTrashCorner6 setHidden:NO];
    }

    [self.selectedImagesView setHidden:NO];
    
    for (int i =0 ;i<_dragSourceView.count ;i++ )
    {
        UIView *tempView = self.dragSourceView[i];
        [tempView setHidden:YES];
    }
    
    for (int i =0 ;i<imagesAndUrls.count ;i++ ){
        id obj = imagesAndUrls[i];
        UIView *tempView = self.dragSourceView[i];
        [tempView setHidden:NO];
        
        if([obj isKindOfClass:[UIImage class]]){
            
            UIImageView *img =self.imageViews[i];
            img.image=imagesAndUrls[i];
        }
        else{
            NSURL *currentURL = imagesAndUrls[i] ;
            
            NSDictionary *videoDict = [obj mutableCopy];
            UIImageView *img =self.imageViews[i];
            
            if ([videoDict objectForKey:@"videoThumbnail"]) {
                img.image = [videoDict objectForKey:@"videoThumbnail"];
            }
            else if ([videoDict objectForKey:@"image"]) {
                
                NSArray *imagesDicts = [videoDict objectForKey:@"image"];
                NSString *imageStr ;
                if(imagesDicts.count){
                    NSDictionary *imageDict = [imagesDicts firstObject];
                    imageStr = [NSString stringWithFormat:@"%@" , [imageDict objectForKey:@"url"]];
                }
                
                
                
                img.contentMode = UIViewContentModeScaleAspectFit;
                img.backgroundColor = [UIColor grayColor];
                [img sd_setImageWithURL:[NSURL URLWithString:imageStr]
                      placeholderImage:[UIImage imageNamed:@"placeHolder"]
                               options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   if (error) {
                                       img.image = [UIImage imageNamed:@"placeHolder"];
                                   }
                                   else{
                                      img.image = image;
                                   }
                                   [img.superview layoutIfNeeded];
                               }];
                
                
                
                img.image = [videoDict objectForKey:@"videoThumbnail"];
            }else{
                img.image = [UIImage imageNamed:@"placeHolder"];

            }
            
        
            //[self createAntPlayVideo:tempView andUrl:currentURL isForPlay:NO];
        }
    }
    
    
//    [self.view bringSubviewToFront:self.btnTrashSelectedImg6];
//    [self.view bringSubviewToFront:self.btnTrashSelectedImg5];
//    [self.view bringSubviewToFront:self.btnTrashSelectedImg4];
//    [self.view bringSubviewToFront:self.btnTrashSelectedImg3];
//    [self.view bringSubviewToFront:self.btnTrashSelectedImg2];
//    [self.view bringSubviewToFront:self.btnTrashSelectedImg];
//    
//    [self.view bringSubviewToFront:self.btnTrashCorner];
//    [self.view bringSubviewToFront:self.btnTrashCorner2];
//    [self.view bringSubviewToFront:self.btnTrashCorner3];
//    [self.view bringSubviewToFront:self.btnTrashCorner4];
//    [self.view bringSubviewToFront:self.btnTrashCorner5];
//    [self.view bringSubviewToFront:self.btnTrashCorner6];
}

-(void)viewDidAppear:(BOOL)animated{
    
    if(self.imageFromUrl != nil){
        
        [imagesAndUrls addObject:self.imageFromUrl];
        [self.imageURLArray addObject:self.imageURL];
        [self setImagesInDragSources ];
        self.imageFromUrl = nil;
    }
    else if(self.responseForPreview != nil){
        [imagesAndUrls addObject:self.responseForPreview];
        NSString *urlString = [NSString stringWithFormat:@"%@" , [self.responseForPreview objectForKey:@"url"]];

        [self.imageURLArray addObject:urlString];
        [self setImagesInDragSources ];
        self.responseForPreview = nil;
    }
}

-(void)viewWillAppear:(BOOL)animated {
    
}

- (void)setup
{
    _tagsView.tagColor = [UIColor grayColor];
    [self handleTagBlocks];
    
}

#pragma mark - Tag blocks

- (void)handleTagBlocks
{
    
    __weak typeof(self) weakSelf = self;
    [_tagsView setTapBlock:^(NSString *tagText, NSInteger idx)
     {
         
     }];
    
    [_tagsView setDeleteBlock:^(NSString *tagText, NSInteger idx)
     {
         
         
         [weakSelf.tagsView deleteTagAtIndex:idx];
         
     }];
}

- (void)addTapped
{
    tempArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < self.selectedFriendsArray.count; i++) {
        if ([self.selectedFriendsArray[i] isKindOfClass:[BackendlessUser class]]) {
            
            BackendlessUser *user = self.selectedFriendsArray[i];
            NSString *name=user.name;
            [_tagsView addTag:name];
            [tagsArray addObject:name];
            [tempArray addObject:user];
        }
        else {
            CNContact *contact=  self.selectedFriendsArray[i];
            NSString *name = [NSString stringWithFormat:@"%@ %@",contact.givenName,contact.familyName];
            
            [_tagsView addTag:name];
            [tagsArray addObject:name];
            [tempArray addObject:contact];
        }
    }
    
    /*
    for(BackendlessUser *user in self.selectedFriendsArray){
        NSString *name=user.name;
        [_tagsView addTag:name];
        [tagsArray addObject:name];
        [tempArray addObject:user];
        
    }
     */
    
}
- (void)shareGridToBrowser:(NSObject*)something{}

#pragma mark - CLLocation Delegates
-(void)GetCurrentLocation{
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyNeighborhood
                                       timeout:10.0
                          delayUntilAuthorized:YES  // This parameter is optional, defaults to NO if omitted
                                         block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                             NSLog(@"%ld",(long)status);
                                             if (status == INTULocationStatusSuccess) {
                                                 
                                                 [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
                                                     if (error == nil&& [placemarks count] >0) {
                                                         placemark = [placemarks lastObject];
                                                         NSString *latitude, *longitude, *state, *country ,*street , *sublocality;
                                                         latitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
                                                         longitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
                                                         state = placemark.administrativeArea;
                                                         country = placemark.country;
                                                         sublocality = placemark.subLocality;
                                                         street = placemark.thoroughfare;
                                                         
                                                         NSLog(@"placemark.ISOcountryCode =%@",placemark.ISOcountryCode);
                                                         NSLog(@"placemark.country =%@",placemark.country);
                                                         NSLog(@"placemark.postalCode =%@",placemark.postalCode);
                                                         NSLog(@"placemark.administrativeArea =%@",placemark.administrativeArea);
                                                         NSLog(@"placemark.locality =%@",placemark.locality);
                                                         NSLog(@"placemark.subLocality =%@",placemark.subLocality);
                                                         NSLog(@"placemark.subThoroughfare =%@",placemark.thoroughfare);
                                                         NSLog(@"placemark.subThoroughfare =%@",placemark.subThoroughfare);
                                                         
                                                         
                                                         location = [NSString stringWithFormat:@"%@ %@", placemark.locality, placemark.administrativeArea];
                                                         self.lblLocation.text = [NSString stringWithFormat:@"@%@",location];
                                                         self.lblLocation1.text = [NSString stringWithFormat:@"%@",location];
                                                         [self dismissPopup];
                                                         [SVProgressHUD dismiss];
                                                         
                                                     } else {
                                                         [SVProgressHUD dismiss];
                                                         NSLog(@"%@", error.debugDescription);
                                                     }
                                                 }];
                                                 
                                             }
                                             else if (status == INTULocationStatusTimedOut) {
                                                 [SVProgressHUD dismiss];
                                                 AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"TimeOut" andCancelButton:NO forAlertType:AlertFailure];
                                                 
                                                 [alert show];
                                             }
                                             else {
                                                 [SVProgressHUD dismiss];
                                                 AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:@"Can't find your Location." andCancelButton:NO forAlertType:AlertFailure];
                                                 
                                                 [alert show];
                                             }
                                         }];
    
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Drag Source Delegate

- (UIView *)draggingViewForDragOperation:(DNDDragOperation *)operation
{
    UIView *dragView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    dragView.backgroundColor=[UIColor grayColor];
    
    NSUInteger i =operation.dragSourceView.tag;
    id obj =[imagesAndUrls objectAtIndex:i];
    if([obj isKindOfClass:[UIImage class]]){
        UIImage *img = [imagesAndUrls objectAtIndex:i];
        
        //UIView *view = self.dragSourceView[i];
        //  [view setHidden:YES];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:dragView.frame];
        imageView.image = img;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
//        imageView.center=dragView.center;
        [dragView addSubview:imageView];
    }
    else{
        
        NSLog(@"this is video");
        
    }
    [UIView animateWithDuration:0.2 animations:^{
        dragView.alpha = 1.0f;
    }];
    return dragView;
}

- (void)dragOperationWillCancel:(DNDDragOperation *)operation {
    NSLog(@"cancelled");
    NSUInteger i =operation.dragSourceView.tag;
    UIView *view = self.dragSourceView[i];
    [view setHidden:NO];}


#pragma mark - Drop Target Delegate
- (void)dragOperation:(DNDDragOperation *)operation didDropInDropTarget:(UIView *)target {
    
    UIView *view = [[UIView alloc ]initWithFrame:target.bounds];
    [target addSubview:view];
    
    NSUInteger i =operation.dragSourceView.tag;
    id obj = [imagesAndUrls objectAtIndex:i];
    
    if([obj isKindOfClass:[UIImage class]])
    {
        
        UIImage *Img=[imagesAndUrls objectAtIndex:i];
        MELDynamicCropView *cropView;
        id obj = [cropViewsForCropping objectAtIndex:target.tag];
        if([obj isKindOfClass:[MELDynamicCropView class]])
        {
            cropView = (MELDynamicCropView *) obj;
            cropView.frame = target.bounds;
            cropView.cropFrame = target.bounds;
        }
        else
        {
            cropView = [[MELDynamicCropView alloc]initWithFrame:target.bounds cropFrame:target.bounds];
        }
        [cropView setContentMode:UIViewContentModeScaleAspectFill];
        
        [cropView setImage:Img];
        [cropView setCropColor:[UIColor clearColor]];
        [cropView setCropAlpha:0.4f];
        [view addSubview:cropView];
        [target sendSubviewToBack:view];
        
        [cropViewsForCropping replaceObjectAtIndex:target.tag withObject:cropView];
        
        [self.imageTempURLs replaceObjectAtIndex:target.tag withObject:[self.imageURLArray objectAtIndex:i]];
        //
    }
    else
    {
        
        //set video
        
        NSDictionary *videoDict = imagesAndUrls[i];
        
        if ([videoDict objectForKey:@"videoURL"])
        {
            NSURL *currentURL =  [videoDict objectForKey:@"videoURL"];
            [imagesToSave addObject:currentURL];
            [self createAntPlayVideo:target andUrl:currentURL isForPlay:YES];
            [self.imageURLsToSave1 addObject:@""];
        }
        else if([videoDict objectForKey:@"image"]){
            //let customView : CustomCardView = (Bundle.main.loadNibNamed("CustomCardView", owner: self, options: nil)?[0] as? CustomCardView)!
            
            
            NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"LinkPreview" owner:self options:nil];
            
            for (UIView *view in views) {
                if ([view isKindOfClass:[LinkPreview class]]) {
                    NSLog(@"CUSTOM VIEW DETECTED");
                }
            }

            LinkPreview *customView  = (LinkPreview *)[[[NSBundle mainBundle] loadNibNamed:@"LinkPreview" owner:self options:nil] objectAtIndex:0];
            
            
            customView.frame = target.bounds;
            
            customView.linkTitle.text = [videoDict objectForKey:@"title"];
            customView.linkdescription.text = [videoDict objectForKey:@"description"];
            
            
            NSString *urlString = [NSString stringWithFormat:@"%@" , [videoDict objectForKey:@"url"]];
            
            customView.linkLink.text = urlString;
            
            
            NSArray *imagesDicts = [videoDict objectForKey:@"image"];
            NSString *imageStr ;
            if(imagesDicts.count){
                NSDictionary *imageDict = [imagesDicts firstObject];
                imageStr = [NSString stringWithFormat:@"%@" , [imageDict objectForKey:@"url"]];
            }
            customView.linkImageView.backgroundColor = [UIColor grayColor];
            [customView.linkImageView sd_setImageWithURL:[NSURL URLWithString:imageStr]
                                      placeholderImage:[UIImage imageNamed:@"placeHolder"]
                                               options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                                   if (error) {
                                                       customView.linkImageView.image = [UIImage imageNamed:@"placeHolder"];
                                                   }
                                                   else{
                                                       customView.linkImageView.image = image;
                                                   }
                                                   [customView layoutIfNeeded];
                                               }];
            [target addSubview:customView];

        }
        
    }
}

- (void)dragOperation:(DNDDragOperation *)operation didEnterDropTarget:(UIView *)target {
    
    NSLog(@"didEnterDropTarget");
    target.layer.borderColor = [operation.draggingView.backgroundColor CGColor];
}

- (void)dragOperation:(DNDDragOperation *)operation didLeaveDropTarget:(UIView *)target {
    NSLog(@"didLeaveDropTarget");
    target.layer.borderColor = [[UIColor whiteColor] CGColor];
}

-(void)createAntPlayVideo:(UIView *)view andUrl:(NSURL *)url isForPlay:(BOOL)isforPlay{
    
    if (isforPlay) {
        
    playerViewController = [[AVPlayerViewController alloc] init];
    playerViewController.delegate = self;
    playerViewController.view.frame = view.bounds;
    playerViewController.showsPlaybackControls = NO;
    
    // First create an AVPlayerItem
    AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:url];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
    playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
    
    UIImage *normImage = [UIImage imageNamed:@"play-button-2.png"];
    playBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    [playBTN addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
    [playBTN setImage:normImage forState:UIControlStateNormal];
    playBTN.frame = CGRectMake(0, 0, normImage.size.width, normImage.size.height);
    playBTN.center = CGPointMake(playerViewController.view.frame.size.width/2, playerViewController.view.frame.size.height/2);
    playBTN.tag = view.tag;
        //PlayerViewTagCount++;
        
    NSInteger viewIndex = view.tag;
    [AllPlayerViewControllers replaceObjectAtIndex:viewIndex withObject:playerViewController];
        
        
        // Copy players Array
        if (!self.ContainerView_a.hidden) {
            
            if (isEdited) {
                [CopyAllPlayerViewControllers1 replaceObjectAtIndex:viewIndex withObject:playerViewController];
            }
            else {
                CopyAllPlayerViewControllers1 = [AllPlayerViewControllers mutableCopy];
            }
            
        }
        else if (!self.ContainerView_b.hidden) {
            if (isEdited) {
                [CopyAllPlayerViewControllers2 replaceObjectAtIndex:viewIndex withObject:playerViewController];
            }
            else {
                CopyAllPlayerViewControllers2 = [AllPlayerViewControllers mutableCopy];
            }
            
        }
        else if (!self.ContainerView_c.hidden) {
            if (isEdited) {
                [CopyAllPlayerViewControllers3 replaceObjectAtIndex:viewIndex withObject:playerViewController];
            }
            else {
                CopyAllPlayerViewControllers3 = [AllPlayerViewControllers mutableCopy];
            }

        }
        else if (!self.ContainerView_d.hidden) {
            if (isEdited) {
                [CopyAllPlayerViewControllers4 replaceObjectAtIndex:viewIndex withObject:playerViewController];
            }
            else {
                CopyAllPlayerViewControllers4 = [AllPlayerViewControllers mutableCopy];
            }
        }
        else if (!self.ContainerView_e.hidden) {
            if (isEdited) {
                [CopyAllPlayerViewControllers5 replaceObjectAtIndex:viewIndex withObject:playerViewController];
            }
            else {
                CopyAllPlayerViewControllers5 = [AllPlayerViewControllers mutableCopy];
            }
        }
        else if (!self.ContainerView_f.hidden) {
            if (isEdited) {
                [CopyAllPlayerViewControllers6 replaceObjectAtIndex:viewIndex withObject:playerViewController];
            }
            else {
                CopyAllPlayerViewControllers6 = [AllPlayerViewControllers mutableCopy];
            }
        }
        
        
    //[AllPlayerViewControllers addObject:playerViewController];
    [view addSubview:playerViewController.view];
    //target.layer.contents = ;
    [playerViewController.view addSubview:playBTN];
    
    
    [ playerViewController.view setAutoresizingMask:( UIViewAutoresizingFlexibleWidth |
                                                      UIViewAutoresizingFlexibleHeight )];
    [view setAutoresizesSubviews:YES ];
     playBTN.hidden = NO;
    
        [view bringSubviewToFront:_btnTrash];
        [view bringSubviewToFront:_btnTrash2];
        [view bringSubviewToFront:_btnTrash3];
        [view bringSubviewToFront:_btnTrash4];
        [view bringSubviewToFront:_btnTrash5];
        [view bringSubviewToFront:_btnTrash6];
        [view bringSubviewToFront:_btnTrash7];
        [view bringSubviewToFront:_btnTrash8];
        [view bringSubviewToFront:_btnTrash9];
        [view bringSubviewToFront:_btnTrash10];
        [view bringSubviewToFront:_btnTrash11];
        [view bringSubviewToFront:_btnTrash12];
        [view bringSubviewToFront:_btnTrash13];
        [view bringSubviewToFront:_btnTrash14];
        [view bringSubviewToFront:_btnTrash15];
        [view bringSubviewToFront:_btnTrash16];
        [view bringSubviewToFront:_btnTrash17];
        
        [view bringSubviewToFront:_borderView];
        [view bringSubviewToFront:_borderView2];
        [view bringSubviewToFront:_borderView3];
        [view bringSubviewToFront:_borderView4];
        [view bringSubviewToFront:_borderView5];
        [view bringSubviewToFront:_borderView6];
        [view bringSubviewToFront:_borderView7];
        [view bringSubviewToFront:_borderView8];
        [view bringSubviewToFront:_borderView9];
        [view bringSubviewToFront:_borderView10];
        [view bringSubviewToFront:_borderView11];
        [view bringSubviewToFront:_borderView12];
        [view bringSubviewToFront:_borderView13];
        [view bringSubviewToFront:_borderView14];
        [view bringSubviewToFront:_borderView15];
        [view bringSubviewToFront:_borderView16];
        [view bringSubviewToFront:_borderView17];
        
        [view bringSubviewToFront:_lblGrid];
        [view bringSubviewToFront:_lblGrid2];
        [view bringSubviewToFront:_lblGrid3];
        [view bringSubviewToFront:_lblGrid4];
        [view bringSubviewToFront:_lblGrid5];
        [view bringSubviewToFront:_lblGrid6];
        [view bringSubviewToFront:_lblGrid7];
        [view bringSubviewToFront:_lblGrid8];
        [view bringSubviewToFront:_lblGrid9];
        [view bringSubviewToFront:_lblGrid10];
        [view bringSubviewToFront:_lblGrid11];
        [view bringSubviewToFront:_lblGrid12];
        [view bringSubviewToFront:_lblGrid13];
        [view bringSubviewToFront:_lblGrid14];
        [view bringSubviewToFront:_lblGrid15];
        [view bringSubviewToFront:_lblGrid16];
        [view bringSubviewToFront:_lblGrid17];
        
    }
    
    else{
        AVPlayerViewController * playerViewControllerMini = [[AVPlayerViewController alloc] init];
        playerViewControllerMini.delegate = self;
        playerViewControllerMini.view.frame = view.bounds;
        playerViewControllerMini.showsPlaybackControls = NO;
        
        // First create an AVPlayerItem
        
        AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:url];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        playerViewControllerMini.player = [AVPlayer playerWithPlayerItem:playerItem];
        playerViewControllerMini.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
        [view addSubview:playerViewControllerMini.view];
        [ playerViewControllerMini.view setAutoresizingMask:( UIViewAutoresizingFlexibleWidth |
                                                         UIViewAutoresizingFlexibleHeight )];
        [view setAutoresizesSubviews:YES ];
       
    }
    
    
    
}

#pragma mark ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    
    for (UIView *dragSources in self.dragSourceView) {
        [self.dragAndDropController registerDragSource:dragSources withDelegate:self];
        [dragSources setHidden:YES];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    //imagesAndUrls = [[NSMutableArray alloc]init];
    
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
    VideosArray = [[NSMutableArray alloc]init];
    for (NSDictionary *dict in info) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                [images addObject:image];
                
                UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
                [imageview setContentMode:UIViewContentModeScaleAspectFit];
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        } else if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypeVideo){
            if ([dict objectForKey:UIImagePickerControllerReferenceURL]){
                NSURL *url =[dict objectForKey:UIImagePickerControllerReferenceURL];
                
                
                UIImage* imageThumbnail;
                if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                    imageThumbnail=[dict objectForKey:UIImagePickerControllerOriginalImage];
                    //[images addObject:imageThumbnail];
                    
                    UIImageView *imageview = [[UIImageView alloc] initWithImage:imageThumbnail];
                    [imageview setContentMode:UIViewContentModeScaleAspectFit];
                } else {
                    NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
                }
                
                NSMutableDictionary *videoDict = [[NSMutableDictionary alloc] init];
                [videoDict setObject:imageThumbnail forKey:@"videoThumbnail"];
                [videoDict setObject:url forKey:@"videoURL"];
                
                [VideosArray addObject:videoDict];
                NSLog(@"UIImagePickerControllerReferenceURL = %@", url);

                
                //  UIImageView *imageview = [[UIImageView alloc] initWithImage:image];
                //  [imageview setContentMode:UIViewContentModeScaleAspectFit];
                
            } else {
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
                
            }
        } else {
            NSLog(@"Uknown asset type");
        }
    }
    [self.selectedImagesView setHidden:NO];
    self.chosenImages = images;
    [imagesAndUrls addObjectsFromArray:self.chosenImages];
    [imagesAndUrls addObjectsFromArray:VideosArray];
    
    [self setImagesInDragSources];
    for(int i = 0; i<self.chosenImages.count ; i++)
    {
        [self.imageURLArray addObject:@""];
    }
    for(int i = 0; i<VideosArray.count ; i++)
    {
        [self.imageURLArray addObject:@""];
    }
    
}

- (void)playVideo:(UIButton *)sender
{
    
   
        NSUInteger index = sender.tag;
        NSLog(@"Index %lu",(unsigned long)index);
    
    
        if (!self.ContainerView_a.hidden) {
            playBTN = sender;
            [sender setHidden:YES];
            self.playerViewController = CopyAllPlayerViewControllers1[index];
            [[_playerViewController player] play];
        }
        else if (!self.ContainerView_b.hidden) {
            playBTN = sender;
            [sender setHidden:YES];
            self.playerViewController = CopyAllPlayerViewControllers2[index];
            [[_playerViewController player] play];
        }
        else if (!self.ContainerView_c.hidden) {
            playBTN = sender;
            [sender setHidden:YES];
            self.playerViewController = CopyAllPlayerViewControllers3[index];
            [[_playerViewController player] play];
        }
        else if (!self.ContainerView_d.hidden) {
            playBTN = sender;
            [sender setHidden:YES];
            self.playerViewController = CopyAllPlayerViewControllers4[index];
            [[_playerViewController player] play];
        }
        else if (!self.ContainerView_e.hidden) {
            playBTN = sender;
            [sender setHidden:YES];
            self.playerViewController = CopyAllPlayerViewControllers5[index];
            [[_playerViewController player] play];
        }
        else if (!self.ContainerView_f.hidden) {
            playBTN = sender;
            [sender setHidden:YES];
            self.playerViewController = CopyAllPlayerViewControllers6[index];
            [[_playerViewController player] play];
}
    
    
    /*
     timeSpent = 0.0;
     previewTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
     target:self
     selector:@selector(checkPlayerTimer:)
     userInfo:nil
     repeats:YES];
     */
}

 

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    [playBTN setHidden:NO];
    AVPlayerItem *p = [notification object];
    
    
    [p seekToTime:kCMTimeZero];
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - upload photos methods
-(void)uploadAsyncWithImageNameWithGrid :(Grid *)localGrid withIndex:(NSInteger)selectedImageIndex ResponseBlock: (Pic4meResponseBlock)responseBlock{
    
    NSLog(@"\n============ Uploading image file  with the ASYNC API ============");
    selectedImage = [self scaleAndRotateImage:selectedImage];
    
    
    NSString *imageName =   [NSString stringWithFormat:@"%u", arc4random() % 100000];
    int REQUIRED_SIZE = 100;
    selectedImage = [self getScaledToRequiredSizedImage:REQUIRED_SIZE :selectedImage];
    
    NSData *imageData = UIImageJPEGRepresentation(selectedImage, 1.0);
    //if
    [backendless.fileService upload:[NSString stringWithFormat:@"myfiles/%@.jpg", imageName]  content:imageData
                           response:^(BackendlessFile *uploadedFile) {
                               NSLog(@"File has been uploaded. File URL is - %@", uploadedFile.fileURL);
                               
                               Photo *photo = [Photo new];
                               photo.photo =uploadedFile.fileURL;
                               photo.webImageLink = [self.imageURLsToSave1 objectAtIndex:selectedImageIndex];
                               photo.User = backendless.userService.currentUser;
                               if(!localGrid.Photos){
                                   localGrid.Photos = [[NSMutableArray alloc] init];
                               }
                               [localGrid.Photos addObject:photo];
                               
                               id<IDataStore> dataStore = [backendless.persistenceService of:[Grid class]];
                               [dataStore save:grid response:^(id savedGrid) {
                                   responseBlock((Grid *)savedGrid , YES , nil);
                               } error:^(Fault *error) {
                                   responseBlock(nil, NO , error);
                               }];
                           }
                              error:^(Fault *fault) {
                                  responseBlock(nil, NO , fault);
                                  
                              }];
    
    
}
-(void)uploadVideo :(Grid *)localGrid ResponseBlock: (Pic4meResponseBlock)responseBlock{
    
    
    NSLog(@"\n============ Uploading Video file  with the ASYNC API ============");
    
    NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
    
    
    NSURL *fileURL = nil;
    __block NSData *assetData ;
    
    NSString* documentsDirectory= [self applicationDocumentsDirectory];
    NSTimeInterval timeInSeiconds = [[NSDate date] timeIntervalSince1970];
    NSString *timeInSeicondsStr = [NSString stringWithFormat:@"%f.mp4",timeInSeiconds];
    NSString* myDocumentPath= [documentsDirectory stringByAppendingPathComponent:timeInSeicondsStr];
    fileURL = [[NSURL alloc] initFileURLWithPath: myDocumentPath];
    
    AVAsset *asset = [AVAsset assetWithURL:VideoUrl];
    // asset is you AVAsset object
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
    
    exportSession.outputURL = fileURL;
    // e.g .mov type
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    
    
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        assetData = [NSData dataWithContentsOfURL:fileURL];
        NSLog(@"AVAsset saved to NSData.");
        dispatch_semaphore_signal(semaphore);
        
    }];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    [backendless.fileService upload:[NSString stringWithFormat:@"media/%f.mp4",timeInSeconds] content:assetData response:^(BackendlessFile *savedFile) {
        
        NSLog(@"saved file:%@",savedFile.fileURL);
        
        Video *video = [Video new];
        video.VideoURL =savedFile.fileURL;
        video.User = backendless.userService.currentUser;
        if(!localGrid.Videos){
            localGrid.Videos = [[NSMutableArray alloc] init];
        }
        [localGrid.Videos addObject:video];
        
        id<IDataStore> dataStore = [backendless.persistenceService of:[Grid class]];
        [dataStore save:grid response:^(id savedGrid) {
            responseBlock((Grid *)savedGrid , YES , nil);
        } error:^(Fault *error) {
            responseBlock(nil, NO , error);
            NSLog(@"%@",error.description);
        }];
        
    }
                              error:^(Fault *fault) {
                                  responseBlock(nil, NO , fault);
                                  NSLog(@"%@",fault.description);
                                  
                              }];
    
    
}
-(NSString*) applicationDocumentsDirectory
{
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString* basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    NSLog(@"%@",basePath);
    return basePath;
    
}
-(UIImage *)getScaledToRequiredSizedImage :(int)requiredSize :(UIImage *)sentImage{
    int scale = 1;
    
    while (sentImage.size.width / scale / 2 >= requiredSize
           && sentImage.size.height / scale / 2 >= requiredSize)
        scale *= 2;
    
    return [self imageWithImage:selectedImage scaledToSize:CGSizeMake(selectedImage.size.width/scale, selectedImage.size.height/scale)];
}
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (UIImage *)scaleAndRotateImage:(UIImage *) image {
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"goToSelectFriends"]) {
        
        FriendsViewController *vc = segue.destinationViewController ;
        vc.delegate=self;
        
    }
    if ([segue.identifier isEqualToString:@"gridToSharedGrid"]) {
        
        SharedViewController *vc = segue.destinationViewController ;
        
        vc.grid=gridToSend;
        
    }
    if ([segue.identifier isEqualToString:@"dragDropToWeb"]) {
        
        BrowserViewController  *vc = segue.destinationViewController ;
        vc.delegate=self;
        
    }
    
}


- (IBAction)backAction:(UIButton *)sender 
{
    if(self.doneBtn.isHidden) // this is send view
    {
//        self.viewsTopSpace.constant = 190;
        self.viewsTopSpace.active = YES;
        self.viewsTopSpaceToCaption.active = NO;
        [self.bottomView setHidden:YES];
        [self.sendBtn setHidden:YES];
        self.captionView.hidden = YES;
        [self.doneBtn setHidden:NO];
        [self.changeGridBtn setHidden:NO];
        [self.commentTextView setHidden:NO];
        self.linkSelectionView.hidden = NO;
        self.photoSelectionView.hidden = NO;
        self.selectedImagesView.hidden = NO;
        self.changeGridBtn.hidden = NO;
        [self.doneBtn setHidden:NO];
        [self showTrashButtons];
    }
    else
    {
        [self performSegueWithIdentifier:@"newDecesionToList" sender:self];
    }
    
}
- (IBAction)openPhotosAction:(UIButton *)sender {
    
    [self openActionSheet];
}

- (IBAction)openLinksAction:(UIButton *)sender {
    
   // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.google.com"]];
    
    [self performSegueWithIdentifier:@"dragDropToWeb" sender:self];
    
}
- (IBAction)doneAction:(UIButton *)sender
{
//    NSLog(@"%lu",(unsigned long)imagesToSave.count);
    
    for (int i=0;i<cropViewsForCropping.count; i++)
    {
        MELDynamicCropView *cropView = [cropViewsForCropping objectAtIndex:i];
        if ([cropView isKindOfClass:[MELDynamicCropView class]])
        {
            UIImage *img = [cropView image];
            [imagesToSave addObject:img];
            [self.imageURLsToSave1 addObject:[self.imageTempURLs objectAtIndex:i]];
        }
    }
    
//    NSLog(@"%lu",(unsigned long)imagesToSave.count);
    if(imagesToSave.count==0 && imagesAndUrls.count == 0)
    {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"Please add Images, Videos or Links" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        NSLog(@"0 images and videos");
    }
    else
    {
        [self.bottomView setHidden:NO];
        [self.sendBtn setHidden:NO];
        self.captionView.hidden = NO;
        self.linkSelectionView.hidden = YES;
        self.photoSelectionView.hidden = YES;
        self.selectedImagesView.hidden = YES;
        self.changeGridBtn.hidden = YES;
        [self.doneBtn setHidden:YES];
        [self.changeGridBtn setHidden:YES];
        [self.commentTextView setHidden:YES];
        self.captionView.hidden = NO;
        [self hideTrashButtons];
        
        self.lblComment.text = self.commentTextView.text;
        
        self.viewsTopSpace.active = NO;
        self.viewsTopSpaceToCaption.active = YES;
        self.viewsTopSpaceToCaption.constant = 0;
//        self.viewsTopSpace.constant = self.captionView.frame.size.height;

    }
}

-(void)hideTrashButtons
{
    [self.btnTrash setHidden:YES];
    [self.btnTrash2 setHidden:YES];
    [self.btnTrash3 setHidden:YES];
    [self.btnTrash4 setHidden:YES];
    [self.btnTrash5 setHidden:YES];
    [self.btnTrash6 setHidden:YES];
    [self.btnTrash7 setHidden:YES];
    [self.btnTrash8 setHidden:YES];
    [self.btnTrash9 setHidden:YES];
    [self.btnTrash10 setHidden:YES];
    [self.btnTrash11 setHidden:YES];
    [self.btnTrash12 setHidden:YES];
    [self.btnTrash13 setHidden:YES];
    [self.btnTrash14 setHidden:YES];
    [self.btnTrash15 setHidden:YES];
    [self.btnTrash16 setHidden:YES];
    [self.btnTrash17 setHidden:YES];
    
    
    [self.borderView setHidden:YES];
    [self.borderView2 setHidden:YES];
    [self.borderView3 setHidden:YES];
    [self.borderView4 setHidden:YES];
    [self.borderView5 setHidden:YES];
    [self.borderView6 setHidden:YES];
    [self.borderView7 setHidden:YES];
    [self.borderView8 setHidden:YES];
    [self.borderView9 setHidden:YES];
    [self.borderView10 setHidden:YES];
    [self.borderView11 setHidden:YES];
    [self.borderView12 setHidden:YES];
    [self.borderView13 setHidden:YES];
    [self.borderView14 setHidden:YES];
    [self.borderView15 setHidden:YES];
    [self.borderView16 setHidden:YES];
    [self.borderView17 setHidden:YES];
}

-(void)showTrashButtons {
    [self.btnTrash setHidden:NO];
    [self.btnTrash2 setHidden:NO];
    [self.btnTrash3 setHidden:NO];
    [self.btnTrash4 setHidden:NO];
    [self.btnTrash5 setHidden:NO];
    [self.btnTrash6 setHidden:NO];
    [self.btnTrash7 setHidden:NO];
    [self.btnTrash8 setHidden:NO];
    [self.btnTrash9 setHidden:NO];
    [self.btnTrash10 setHidden:NO];
    [self.btnTrash11 setHidden:NO];
    [self.btnTrash12 setHidden:NO];
    [self.btnTrash13 setHidden:NO];
    [self.btnTrash14 setHidden:NO];
    [self.btnTrash15 setHidden:NO];
    [self.btnTrash16 setHidden:NO];
    [self.btnTrash17 setHidden:NO];
    
    [self.borderView setHidden:NO];
    [self.borderView2 setHidden:NO];
    [self.borderView3 setHidden:NO];
    [self.borderView4 setHidden:NO];
    [self.borderView5 setHidden:NO];
    [self.borderView6 setHidden:NO];
    [self.borderView7 setHidden:NO];
    [self.borderView8 setHidden:NO];
    [self.borderView9 setHidden:NO];
    [self.borderView10 setHidden:NO];
    [self.borderView11 setHidden:NO];
    [self.borderView12 setHidden:NO];
    [self.borderView13 setHidden:NO];
    [self.borderView14 setHidden:NO];
    [self.borderView15 setHidden:NO];
    [self.borderView16 setHidden:NO];
    [self.borderView17 setHidden:NO];
}

- (IBAction)addLocationAction:(UIButton *)sender {
    
    locationPopUp = [[LocationPopUp alloc] initWithNibName:@"LocationPopUp" bundle:nil];
    [self presentPopupViewController:locationPopUp animated:YES completion:^(void) {
        
        [locationPopUp.closeBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        [locationPopUp.radioAddLocation addTarget:self action:@selector(radioButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [locationPopUp.radioUseGps addTarget:self action:@selector(radioButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [locationPopUp.doneBtn addTarget:self action:@selector(saveLocationAndDissmiss) forControlEvents:UIControlEventTouchUpInside];
    }];
    
}
-(void)saveLocationAndDissmiss{
    
    if(gpsLocation)
    {
        [SVProgressHUD showWithStatus:@"Getting Location..."];
        geocoder = [[CLGeocoder alloc] init];
        [self GetCurrentLocation];
        [self.lblLocation setHidden:NO];
    }
    else
    {
        if(locationPopUp.txtLocation.text.length){
            location = locationPopUp.txtLocation.text;
            [self.lblLocation setHidden:NO];
            self.lblLocation.text = [NSString stringWithFormat:@"@%@",location];
            self.lblLocation1.text = [NSString stringWithFormat:@"%@",location];
            [self dismissPopup];
        }
    }
}
- (IBAction)radioButtonPressed:(UIButton *)sender {
    location = @"";
    [locationPopUp.doneBtn setHidden:NO];
    
    if (sender.tag==11)
    {
        gpsLocation = NO;
        locationPopUp.imgAddLocation.image = [UIImage imageNamed:@"oval.png"];
        locationPopUp.imgUseGps.image = [UIImage imageNamed:@"radio-on-button.png"];
        [locationPopUp.txtLocation setHidden:NO];
        [locationPopUp.txtLocation becomeFirstResponder];
    }
    else if(sender.tag==22)
    {
        gpsLocation = YES;
        [locationPopUp.txtLocation resignFirstResponder];
        locationPopUp.imgUseGps.image = [UIImage imageNamed:@"oval.png"];
        locationPopUp.imgAddLocation.image = [UIImage imageNamed:@"radio-on-button.png"];
        [locationPopUp.txtLocation setHidden:YES];
    }
}

-(void)dismissPopup{
    
    [self dismissPopupViewControllerAnimated:YES completion:nil];
}
- (IBAction)addFriendsAction:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"goToSelectFriends" sender:self];
}
- (void)tellRegisterDelegateSomething:(NSObject*)something
{
    
}
- (IBAction)sendAction:(UIButton *)sender {
    
    if(tempArray.count==0){
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"No Friends Selected" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        NSLog(@"No friends selected");
    }
    else{
        [SVProgressHUD showWithStatus:@"Saving Photos and videos..."];
        
        grid = [Grid new];
        grid.Photos = [[NSMutableArray alloc]init];
        grid.Videos = [[NSMutableArray alloc]init];
        gridImageUrls = [[NSMutableArray alloc] init];
        grid.friends = [[NSMutableArray alloc]init];
        grid.comment = self.commentTextView.text;
        grid.location= location;
        grid.style = self.senderTag;
        grid.User = backendless.userService.currentUser;
        grid.friends = tempArray;
        
        [self recursiveApiCallsForLinks:imagesAndUrls andCurrentIndex:0];

        //[self recursiveApiCallsWithUrlArray:imagesToSave andCurrentIndex:0];
    }
}
-(void)recursiveApiCallsForLinks :(NSMutableArray *)urls andCurrentIndex :(int)indexOfArray{
    __block int index = indexOfArray ;
    if (indexOfArray == urls.count) {
        [self recursiveApiCallsWithUrlArray:imagesToSave andCurrentIndex:0];
    }
    else{
        id obj = urls[index];
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *videoDict = (NSDictionary *)obj;
            if([videoDict objectForKey:@"image"]){
                Photo *photo = [Photo new];
                NSArray *imagesDicts = [videoDict objectForKey:@"image"];
                NSString *imageStr ;
                if(imagesDicts.count){
                    NSDictionary *imageDict = [imagesDicts firstObject];
                    imageStr = [NSString stringWithFormat:@"%@" , [imageDict objectForKey:@"url"]];
                }
                photo.photo = imageStr;
                NSString *urlString = [NSString stringWithFormat:@"%@" , [videoDict objectForKey:@"url"]];

                photo.webImageLink = urlString;
                photo.linkTitle = [videoDict objectForKey:@"title"];
                photo.linkDescription = [videoDict objectForKey:@"description"];

                photo.User = backendless.userService.currentUser;
                [grid.Photos addObject:photo];
                id<IDataStore> dataStore = [backendless.persistenceService of:[Grid class]];
                [dataStore save:grid response:^(id savedGrid) {
                    grid = savedGrid;
                    NSLog(@"GRID SAVED......");
                    index = index + 1;
                    [self recursiveApiCallsForLinks:imagesAndUrls andCurrentIndex:index];
                } error:^(Fault *error) {
                    NSLog(@"ERROR GOTTEN = %@" , error.description);
                    [SVProgressHUD dismiss];

                }];
            }
            else{
                index = index + 1;
                [self recursiveApiCallsForLinks:imagesAndUrls andCurrentIndex:index];
            }
        }
        else{
            index = index + 1;
            [self recursiveApiCallsForLinks:imagesAndUrls andCurrentIndex:index];
        }
    }
    
    
    
    
}


-(void)recursiveApiCallsWithUrlArray :(NSMutableArray *)urls andCurrentIndex :(int)indexOfArray{
    NSLog(@"%lu",(unsigned long)imagesToSave.count);
    __block int index = indexOfArray ;
    if (indexOfArray == urls.count) {
        [SVProgressHUD dismiss];
        gridToSend=grid;
        [self performSegueWithIdentifier:@"gridToSharedGrid" sender:self];
        return ;
    }
    else{
        id obj= [imagesToSave objectAtIndex:indexOfArray];
        
        if([obj isKindOfClass:[UIImage class]]){
            UIImage *img =   (UIImage *)obj;
            selectedImage = img;
            [self uploadAsyncWithImageNameWithGrid:grid withIndex:indexOfArray ResponseBlock:^(Grid *gridObj, BOOL status, Fault *error) {
                if (status) {
                    grid = gridObj;
                    index = index + 1;
                    [self recursiveApiCallsWithUrlArray:imagesToSave andCurrentIndex:index];

                }
                else{
                    
                    [SVProgressHUD dismiss];
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                    
                    [alert show];
                }
            } ];
        }
        else{
            
            VideoUrl =   obj;
            
            
            [self uploadVideo:grid ResponseBlock:^(Grid *gridObj, BOOL status, Fault *error) {
                if (status) {
                    grid = gridObj;
                    index = index + 1;
                    [self recursiveApiCallsWithUrlArray:imagesToSave andCurrentIndex:index];

                }
                else{
                    
                    [SVProgressHUD dismiss];
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:error.description andCancelButton:NO forAlertType:AlertFailure];
                    
                    [alert show];
                    
                }
            } ];
        }
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 6;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:{
            static NSString *identifier = @"cell1";
            UICollectionViewCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            return cell;
            
            break;
        }
        case 1:{
            static NSString *identifier = @"cell2";
            UICollectionViewCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            return cell;
            break;
        }
            
        case 2:{
            static NSString *identifier = @"cell3";
            UICollectionViewCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            return cell;
            break;
        }
            
        case 3:{
            static NSString *identifier = @"cell4";
            UICollectionViewCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            return cell;
            break;
        }
            
        case 4:{
            static NSString *identifier = @"cell5";
            UICollectionViewCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            return cell;
            break;
        }
            
        case 5:{
            static NSString *identifier = @"cell6";
            UICollectionViewCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
            return cell;
            break;
        }
            
        default:
            break;
    }
    return 0;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
   // AllPlayerViewControllers =[[NSMutableArray alloc] init];
    
    self.ContainerView_a.hidden = indexPath.row != 0 ;
    self.ContainerView_b.hidden = indexPath.row != 1 ;
    self.ContainerView_c.hidden = indexPath.row != 2 ;
    self.ContainerView_d.hidden = indexPath.row != 3 ;
    self.ContainerView_e.hidden = indexPath.row != 4 ;
    self.ContainerView_f.hidden = indexPath.row != 5 ;
    
    self.senderTag = indexPath.row;
    [self gridSelectionDoneAction:nil];
    
    isEdited = YES;
    
    if (!self.ContainerView_a.hidden) {
        cropViewsForCropping = [[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"", nil];
        AllPlayerViewControllers =[[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"", nil];
        self.imageTempURLs = [[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"", nil];
    }
    else if (!self.ContainerView_b.hidden) {
        cropViewsForCropping = [[NSMutableArray alloc] initWithObjects:@"",@"",@"", nil];
        AllPlayerViewControllers =[[NSMutableArray alloc] initWithObjects:@"",@"",@"", nil];
        self.imageTempURLs = [[NSMutableArray alloc] initWithObjects:@"",@"",@"", nil];

    }
    else if (!self.ContainerView_c.hidden) {
        cropViewsForCropping = [[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"", nil];
        AllPlayerViewControllers =[[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"", nil];
        self.imageTempURLs = [[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"", nil];

    }
    else if (!self.ContainerView_d.hidden) {
        cropViewsForCropping = [[NSMutableArray alloc] initWithObjects:@"",@"",@"", nil];
        AllPlayerViewControllers =[[NSMutableArray alloc] initWithObjects:@"",@"",@"", nil];
        self.imageTempURLs = [[NSMutableArray alloc] initWithObjects:@"",@"",@"", nil];
    }
    else if (!self.ContainerView_e.hidden) {
        cropViewsForCropping = [[NSMutableArray alloc] initWithObjects:@"",@"", nil];
        AllPlayerViewControllers =[[NSMutableArray alloc] initWithObjects:@"",@"", nil];
        self.imageTempURLs = [[NSMutableArray alloc] initWithObjects:@"",@"", nil];
    }
    else if (!self.ContainerView_f.hidden) {
        cropViewsForCropping = [[NSMutableArray alloc] initWithObjects:@"", nil];
        AllPlayerViewControllers =[[NSMutableArray alloc] initWithObjects:@"", nil];
        self.imageTempURLs = [[NSMutableArray alloc] initWithObjects:@"", nil];
    }
    
//    NSLog(@"ARRAY COUNT :%lu", (unsigned long)cropViewsForCropping.count);
    
    
}

-(IBAction)unwindToShareGrid:(UIStoryboardSegue*)sender
{
    [self addTapped];
}
- (IBAction)gridSelectionDoneAction:(UIButton *)sender {
    [UIView animateWithDuration:0.5 animations:^{
        
        self.gridSelectionViewBottom.constant=-self.gridSelectionView.frame.size.height;
        [self.gridSelectionView layoutIfNeeded];
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        
    }];
    // self.transitionViewBottomSpace.constant=0;
    
}
- (IBAction)changeGridAction:(UIButton *)sender {
    PlayerViewTagCount = 0;
    [UIView animateWithDuration:0.5 animations:^{
        
        self.gridSelectionViewBottom.constant=0;
        [self.gridSelectionView layoutIfNeeded];
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        
    }];
    
}

#pragma mark - Choose Image

-(void)openActionSheet
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Camera"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              NSLog(@"You pressed button one");
                                                              
                                                              [self showCameraImagePicker];
                                                              
                                                          }]; // 2
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Gallery"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               NSLog(@"You pressed button two");
                                                               
                                                               [self showGalleryImagePicker];
                                                               
                                                               // 3
                                                           }];
    
    UIAlertAction *thirdAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                          style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                                                              NSLog(@"You pressed button two");
                                                          }]; // 4
    
    [alert addAction:firstAction]; // 4
    [alert addAction:secondAction]; // 5
    [alert addAction:thirdAction]; // 5
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)showCameraImagePicker {
#if TARGET_IPHONE_SIMULATOR
    [self showErrorWithMessage:@"Info!" message:@"Device not found."];
#elif TARGET_OS_IPHONE
    
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        // do your logic
    }
    else if(authStatus == AVAuthorizationStatusDenied)
    {
        // denied
        if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                // until iOS 8. So for iOS 7 permission will always be granted.
                
                NSLog(@"DENIED");
                
                if (granted) {
                    // Permission has been granted. Use dispatch_async for any UI updating
                    // code because this block may be executed in a thread.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[self doStuff];
                    });
                } else {
                    
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Not Authorized" message:@"Please go to Settings and enable the camera for this app to use this feature." preferredStyle:UIAlertControllerStyleAlert]; // 1
                    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"OK"                                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                                  {
                                                      
                                                      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];

                                                      
                                                  }];
                    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Cancel"                                                                          style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                                  {
                                                      
                                                      
                                                      
                                                  }];
                    [alert addAction:firstAction]; // 5
                    [alert addAction:secondAction]; // 5
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
            }];
        }
    }
    else if(authStatus == AVAuthorizationStatusRestricted)
    {
        // restricted, normally won't happen
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        // not determined?!
        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
            if(granted){
                NSLog(@"Granted access to %@", mediaType);
            } else {
                NSLog(@"Not granted access to %@", mediaType);
            }
        }];
    }
    else
    {
        // impossible, unknown authorization status
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie,(NSString *) kUTTypeImage, nil];
    
    [self presentViewController:picker animated:YES completion:NULL];
#endif
}

- (void)showGalleryImagePicker {
    ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
    
    elcPicker.maximumImagesCount = 6; //Set the maximum number of images to select to 100
    elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
    elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
    elcPicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie]; //Supports image and movie types
    
    elcPicker.imagePickerDelegate = self;
    [self presentViewController:elcPicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        
        // Media is an image
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        [imagesAndUrls addObject:chosenImage];
        [picker dismissViewControllerAnimated:YES completion:NULL];
        [self setImagesInDragSources];
        
        [self.imageURLArray addObject:@""];
        
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeVideo] || [mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
        // Media is a video
        NSURL *videoUrl = (NSURL *)[info valueForKey:UIImagePickerControllerMediaURL];
        
        AVAsset *asset = [AVAsset assetWithURL:videoUrl];
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        CMTime time = CMTimeMake(1, 1);
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);
        
        
        NSMutableDictionary *videoDict = [[NSMutableDictionary alloc] init];
        if (thumbnail) {
            [videoDict setObject:thumbnail forKey:@"videoThumbnail"];
        }
        if (videoUrl) {
            [videoDict setObject:videoUrl forKey:@"videoURL"];
        }
        
        [imagesAndUrls addObject:videoDict];
        [picker dismissViewControllerAnimated:YES completion:NULL];
        [self setImagesInDragSources];
        [self.imageURLArray addObject:@""];

    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    //profile_img = nil;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Show Error Controller
-(void)showErrorWithMessage:(NSString *)title message:(NSString *)message
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                               }];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)btnSelectedImgTrashAction:(UIButton *)sender {
    
    NSUInteger index = sender.tag;
    NSLog(@"Index %lu",(unsigned long)index);
    [imagesAndUrls removeObjectAtIndex:index];
    [self.imageURLArray removeObjectAtIndex:index];
    UIImageView *imgView = [self.imageViews objectAtIndex:index];
    NSLog(@"%@",imgView);
    imgView.image=nil;
    
    for (UIImageView *imgview in self.imageViews) {
        imgview.image = nil;
    }
    

    if (imagesAndUrls.count == 5 ) {
        [self.btnTrashSelectedImg6 setHidden:YES];

        [self.btnTrashCorner6 setHidden:YES];
    }
    else if (imagesAndUrls.count == 4 ) {
        [self.btnTrashSelectedImg5 setHidden:YES];
        [self.btnTrashCorner4 setHidden:YES];
    }
    else if (imagesAndUrls.count == 3 ) {
        [self.btnTrashSelectedImg4 setHidden:YES];
        [self.btnTrashCorner4 setHidden:YES];
    }
    else if (imagesAndUrls.count == 2 ) {
        [self.btnTrashSelectedImg3 setHidden:YES];
        [self.btnTrashCorner3 setHidden:YES];
    }
    else if (imagesAndUrls.count == 1 ) {
        [self.btnTrashSelectedImg2 setHidden:YES];
        [self.btnTrashCorner2 setHidden:YES];
    }
    else {
        [self.btnTrashSelectedImg setHidden:YES];
        [self.btnTrashCorner setHidden:YES];
    }

    [self setImagesInDragSources];
}

- (IBAction)btnTrashAction:(UIButton *)sender {
    
    NSUInteger index = sender.tag;
    NSLog(@"Index %lu",(unsigned long)index);
    
    NSArray *selectedViewArray = [[NSArray alloc] init];
    
    if (!self.ContainerView_a.hidden) {
        selectedViewArray = [self.dropTargetView_a mutableCopy];
    }
    else if (!self.ContainerView_b.hidden) {
        selectedViewArray = [self.dropTargetView_b mutableCopy];
    }
    else if (!self.ContainerView_c.hidden) {
        selectedViewArray = [self.dropTargetView_c mutableCopy];
    }
    else if (!self.ContainerView_d.hidden) {
        selectedViewArray = [self.dropTargetView_d mutableCopy];
    }
    else if (!self.ContainerView_e.hidden) {
        selectedViewArray = [self.dropTargetView_e mutableCopy];
    }
    else if (!self.ContainerView_f.hidden) {
        selectedViewArray = [self.dropTarget_f mutableCopy];
    }
    
    UIView *view = [selectedViewArray objectAtIndex:index];
    NSLog(@"%@",view.subviews);
    
    NSArray *viewsToRemove = [view subviews];
    
    for (UIView *v in viewsToRemove) {
        if ([v isKindOfClass:[UIView class]]) {
            
            UIView *remView = v;
            NSLog(@"%@",remView.subviews);
            
            if ([v isKindOfClass:[playerViewController.view class]]) {
                [v removeFromSuperview];
            }
            
            NSArray *remSubviews = [remView subviews];
            for (UIView *viw in remSubviews) {
                if ([viw isKindOfClass:[MELDynamicCropView class]]) {
                    
                    [v removeFromSuperview];
                    // remove, set as empty object in array
                    [cropViewsForCropping replaceObjectAtIndex:index withObject:@""];
                }
            }
        }
    }
}
-(void)textViewDidChangeHeight:(GrowingTextView *)textView height:(CGFloat)height{
    NSLog(@"Height Changed");
}
@end
