//
//  AppDelegate.h
//  pic4me
//
//  Created by ibuildx on 7/3/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//
// com.saqibdb.pic4me

#import <UIKit/UIKit.h>
#import "MediaService.h"
#import "Backendless.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
-(void)displayAlertWithTitle:(NSString *)title andMessage:(NSString *)message;
@end

