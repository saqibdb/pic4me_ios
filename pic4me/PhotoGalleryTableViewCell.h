//
//  PhotoGalleryTableViewCell.h
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/14/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoGalleryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoA;
@property (weak, nonatomic) IBOutlet UIImageView *photoB;
@property (weak, nonatomic) IBOutlet UIButton *photoBtnA;
@property (weak, nonatomic) IBOutlet UIButton *photoBtnB;
@property (weak, nonatomic) IBOutlet UIImageView *photoC;
@property (weak, nonatomic) IBOutlet UIButton *photoBtnC;


@end
