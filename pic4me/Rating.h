//
//  Rating.h
//  pic4me
//
//  Created by ibuildx on 7/29/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"
#import "Photo.h"
#import "Video.h"
@interface Rating : NSObject


@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) BackendlessUser *User;
@property (nonatomic, strong) Photo *photo;
@property (nonatomic, strong) Video *video;
@property (nonatomic, strong) NSString *count;
@property (nonatomic, strong) NSString *GridId;

@end
