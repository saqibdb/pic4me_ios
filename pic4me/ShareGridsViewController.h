//
//  ShareGridsViewController.h
//  pic4me
//
//  Created by ibuildx on 11/17/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//
#import "ASJTagsView.h"
#import <UIKit/UIKit.h>
#import "DNDDragAndDrop.h"
#import <UIKit/UIKit.h>
#import "ELCImagePickerHeader.h"
#import <CoreLocation/CoreLocation.h>
#import "FriendsViewController.h"
#import "BrowserViewController.h"
#import <AVKit/AVKit.h>
#import "Backendless.h"
#import "Grid.h"
#import "MELDynamicCropView.h"
#import "uChoose-Swift.h"

typedef void (^Pic4meResponseBlock) (Grid * gridObj, BOOL status, Fault *error);//block as a typedef

@class CCMBorderView, GrowingTextView;
@interface ShareGridsViewController : UIViewController<ELCImagePickerControllerDelegate,DNDDragSourceDelegate,DNDDropTargetDelegate,UIScrollViewDelegate,CLLocationManagerDelegate,UploadVideoViewDelegate,BrowserDelegate,AVPlayerViewControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource , GrowingTextViewDelegate>
@property (nonatomic, retain) CLLocationManager *locationManager; 
@property (nonatomic, copy) NSArray *chosenImages;
@property (strong, nonatomic) IBOutlet DNDDragAndDropController *dragAndDropController;
@property (nonatomic, copy) NSArray *selectedFriendsArray;



@property (copy, nonatomic) NSArray *tags;
@property (weak, nonatomic) IBOutlet ASJTagsView *tagsView;
@property (strong, nonatomic) IBOutlet UITextField* commentTextField;

@property (strong, nonatomic) IBOutlet GrowingTextView* commentTextView;
@property (nonatomic, strong) UIImage *imageFromUrl;

@property (nonatomic, strong) NSDictionary *responseForPreview;



@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSMutableArray *imageURLArray;

@property (nonatomic, strong) NSMutableArray *imageTempURLs;

@property (nonatomic, strong) NSMutableArray *imageURLsToSave1;
@property (nonatomic) AVPlayerViewController *playerViewController;

//one
@property (weak, nonatomic) IBOutlet UIView *ContainerView_a;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *dropTargetView_a;
//two
@property (weak, nonatomic) IBOutlet UIView *ContainerView_b;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *dropTargetView_b;
//three
@property (weak, nonatomic) IBOutlet UIView *ContainerView_c;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *dropTargetView_c;
//four
@property (weak, nonatomic) IBOutlet UIView *ContainerView_d;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *dropTargetView_d;
//five
@property (weak, nonatomic) IBOutlet UIView *ContainerView_e;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *dropTargetView_e;
//six
@property (weak, nonatomic) IBOutlet UIView *ContainerView_f;
@property (strong, nonatomic) IBOutletCollection(MELDynamicCropView) NSArray *dropTarget_f;


////

@property (weak, nonatomic) IBOutlet UIButton *btnTrash;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash2;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash3;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash4;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash5;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash6;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash7;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash8;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash9;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash10;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash11;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash12;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash13;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash14;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash15;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash16;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash17;


@property (weak, nonatomic) IBOutlet UIView *borderView;
@property (weak, nonatomic) IBOutlet UIView *borderView2;
@property (weak, nonatomic) IBOutlet UIView *borderView3;
@property (weak, nonatomic) IBOutlet UIView *borderView4;
@property (weak, nonatomic) IBOutlet UIView *borderView5;
@property (weak, nonatomic) IBOutlet UIView *borderView6;
@property (weak, nonatomic) IBOutlet UIView *borderView7;
@property (weak, nonatomic) IBOutlet UIView *borderView8;
@property (weak, nonatomic) IBOutlet UIView *borderView9;
@property (weak, nonatomic) IBOutlet UIView *borderView10;
@property (weak, nonatomic) IBOutlet UIView *borderView11;
@property (weak, nonatomic) IBOutlet UIView *borderView12;
@property (weak, nonatomic) IBOutlet UIView *borderView13;
@property (weak, nonatomic) IBOutlet UIView *borderView14;
@property (weak, nonatomic) IBOutlet UIView *borderView15;
@property (weak, nonatomic) IBOutlet UIView *borderView16;
@property (weak, nonatomic) IBOutlet UIView *borderView17;

@property (weak, nonatomic) IBOutlet UIView *lblGrid;
@property (weak, nonatomic) IBOutlet UIView *lblGrid2;
@property (weak, nonatomic) IBOutlet UIView *lblGrid3;
@property (weak, nonatomic) IBOutlet UIView *lblGrid4;
@property (weak, nonatomic) IBOutlet UIView *lblGrid5;
@property (weak, nonatomic) IBOutlet UIView *lblGrid6;
@property (weak, nonatomic) IBOutlet UIView *lblGrid7;
@property (weak, nonatomic) IBOutlet UIView *lblGrid8;
@property (weak, nonatomic) IBOutlet UIView *lblGrid9;
@property (weak, nonatomic) IBOutlet UIView *lblGrid10;
@property (weak, nonatomic) IBOutlet UIView *lblGrid11;
@property (weak, nonatomic) IBOutlet UIView *lblGrid12;
@property (weak, nonatomic) IBOutlet UIView *lblGrid13;
@property (weak, nonatomic) IBOutlet UIView *lblGrid14;
@property (weak, nonatomic) IBOutlet UIView *lblGrid15;
@property (weak, nonatomic) IBOutlet UIView *lblGrid16;
@property (weak, nonatomic) IBOutlet UIView *lblGrid17;


@property (weak, nonatomic) IBOutlet UIButton *btnTrashSelectedImg;
@property (weak, nonatomic) IBOutlet UIButton *btnTrashSelectedImg2;
@property (weak, nonatomic) IBOutlet UIButton *btnTrashSelectedImg3;
@property (weak, nonatomic) IBOutlet UIButton *btnTrashSelectedImg4;
@property (weak, nonatomic) IBOutlet UIButton *btnTrashSelectedImg5;
@property (weak, nonatomic) IBOutlet UIButton *btnTrashSelectedImg6;


@property (weak, nonatomic) IBOutlet CCMBorderView *btnTrashCorner;
@property (weak, nonatomic) IBOutlet CCMBorderView *btnTrashCorner6;
@property (weak, nonatomic) IBOutlet CCMBorderView *btnTrashCorner2;
@property (weak, nonatomic) IBOutlet CCMBorderView *btnTrashCorner3;
@property (weak, nonatomic) IBOutlet CCMBorderView *btnTrashCorner4;
@property (weak, nonatomic) IBOutlet CCMBorderView *btnTrashCorner5;


- (IBAction)btnSelectedImgTrashAction:(id)sender;

- (IBAction)btnTrashAction:(id)sender;

////

- (IBAction)backAction:(UIButton *)sender;
@property NSInteger senderTag;


@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *dragSourceView;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *imageViews;

- (IBAction)openPhotosAction:(UIButton *)sender;
- (IBAction)openLinksAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *selectedImagesView;
- (IBAction)doneAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewsTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewsTopSpaceToCaption;
- (IBAction)addLocationAction:(UIButton *)sender;
- (IBAction)addFriendsAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *linkSelectionView;
@property (weak, nonatomic) IBOutlet UIView *photoSelectionView;

@property (weak, nonatomic) IBOutlet UIView *captionView;
@property (weak, nonatomic) IBOutlet UILabel *gridUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation1;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblComment;
@property (weak, nonatomic) IBOutlet UIImageView *gridUserProfileImage;

@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
- (IBAction)sendAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
- (IBAction)gridSelectionDoneAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *gridSelectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *gridSelectionViewBottom;
- (IBAction)changeGridAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *changeGridBtn;

@end
