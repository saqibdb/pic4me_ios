//
//  GridListsCell.h
//  pic4me
//
//  Created by ibuildx on 11/25/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GridListsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *gridImage;
@property (weak, nonatomic) IBOutlet UILabel *lblGridDetails;

@end
