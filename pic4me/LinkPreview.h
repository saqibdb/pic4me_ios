//
//  LinkPreview.h
//  uChoose
//
//  Created by iBuildX on 18/05/2018.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinkPreview : UIView

@property (weak, nonatomic) IBOutlet UIImageView *linkImageView;

@property (weak, nonatomic) IBOutlet UILabel *linkTitle;
@property (weak, nonatomic) IBOutlet UILabel *linkdescription;
@property (weak, nonatomic) IBOutlet UILabel *linkLink;




@end
