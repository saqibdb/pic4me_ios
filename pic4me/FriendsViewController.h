//
//  FriendsViewController.h
//  pic4me
//
//  Created by ibuildx on 7/4/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>
#import "Backendless.h"


@protocol UploadVideoViewDelegate <NSObject>
- (void)tellRegisterDelegateSomething:(NSObject*)something;
@end
@interface FriendsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate ,CLLocationManagerDelegate,MFMessageComposeViewControllerDelegate,UITextFieldDelegate>

@property (weak, nonatomic) id <UploadVideoViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITableView *friendsTableView;
@property (strong,nonatomic) BackendlessUser *selectedUser;
- (IBAction)friendsBackAction:(id)sender;
@property (strong) NSMutableArray *selectedfriendsForChatArray;
@property (assign, nonatomic) BOOL longPressActive;

@property (nonatomic) BOOL isCalled;

@property (weak, nonatomic) IBOutlet UIButton *doneSelectContactbtn;

- (IBAction)doneSelectContactAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *searchView;
- (IBAction)searchBackAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
- (IBAction)searchGoAction:(UIButton *)sender;
- (IBAction)searchAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBtnTrailingSpace;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *segmentViewHeight;
@property (weak, nonatomic) IBOutlet UIView *segmentView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentController;

@property (strong) NSMutableArray *appFriendsArray;

@property (strong) NSMutableArray *inviteFriendsArray;
@end
