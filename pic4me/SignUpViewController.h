//
//  SignUpViewController.h
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/21/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TNRadioButtonGroup.h"
@interface SignUpViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *profileBackview;

@property (weak, nonatomic) IBOutlet UITextField *userNameTxt;

@property (weak, nonatomic)

IBOutlet UITextField *emailTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;
@property (weak, nonatomic) IBOutlet UITextField *genderTxt;

@property (weak, nonatomic) IBOutlet UITextField *phoneTxt;

@property (weak, nonatomic) IBOutlet UITextField *ageTxt;
- (IBAction)signUpAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;
@property (weak, nonatomic) IBOutlet UIView *signUpSprite;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicture;
- (IBAction)selectProfilePictureAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *profileImageBtn;
- (IBAction)backAction:(id)sender;
@property (nonatomic, strong) TNRadioButtonGroup *sexGroup;
@property (weak, nonatomic) IBOutlet UIView *OuterView;

@end
