//
//  FriendsViewController.m
//  pic4me
//
//  Created by ibuildx on 7/4/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//
#import "ShareGridsViewController.h"

#import "FriendsViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "FriendListTableViewCell.h"
#import "UIViewController+CWPopup.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "Backendless.h"
#import <AddressBook/ABAddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "ProfileViewController.h"
#import <Contacts/Contacts.h>
#import <MessageUI/MessageUI.h>
#import "FriendProfileViewController.h"
#import "ShareGridsViewController.h"
#import "FlatChatViewController.h"

@interface FriendsViewController ()
{
    NSMutableArray *friendsArray;
    NSMutableArray *temporaryFriendsArray;
    NSMutableArray *fbFriendsArray;
    NSMutableArray *allUsersArray;
    NSMutableArray *contactList;
    
    //alphabetical
    NSMutableArray *mergedAndSortedArray;
    NSString *selectedContact;
    NSString *searchTextString;
    NSMutableArray *friendsFiltertedArray;
    NSMutableArray *contactsFiltertedArray;
    BOOL isFiltered;
    NSArray *combinedArray;
}

@end


@implementation FriendsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    isFiltered = NO;
   
    [self fetchFacebookFriends];
    self.txtSearch.delegate = self;
    self.friendsTableView.dataSource =self;
    self.friendsTableView.delegate=self;
   
    [self.searchView setHidden:YES];
    
    temporaryFriendsArray=[[NSMutableArray alloc] init];
    self.selectedfriendsForChatArray=[[NSMutableArray alloc] init];
     friendsArray = [[NSMutableArray alloc] init];
//    friendsArray = [[NSMutableArray alloc]initWithArray:[backendless.userService.currentUser getProperty:@"friends"]] ;
//    NSLog(@"friends found =%lu",(unsigned long)friendsArray.count);
    
    if ([self.delegate respondsToSelector:@selector(tellRegisterDelegateSomething:)])
    {
        self.searchBtnTrailingSpace.constant = 5 ;
//        self.segmentView.hidden = NO;
//        self.segmentViewHeight.constant = 50;
        self.friendsTableView.tag = 0;
        
        [self.segmentController setTitle:@"App Friends" forSegmentAtIndex:0];
        [self.segmentController setTitle:@"Invite Friends" forSegmentAtIndex:1];
    }
    else
    {
        self.searchBtnTrailingSpace.constant = -40;
//        self.segmentView.hidden = YES;
//        self.segmentViewHeight.constant = 0;
        
        self.friendsTableView.tag = 1;
        [self.segmentController setTitle:@"Chat With Friends" forSegmentAtIndex:0];
        [self.segmentController setTitle:@"Invite Friends For Chat" forSegmentAtIndex:1];
    
    }
    _isCalled = NO;

//    [self loadContactList];
    //filtertedArray = [[NSMutableArray alloc] init];
    [self.txtSearch addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

-(void)viewWillAppear:(BOOL)animated{
    if(!_isCalled)
    {
        [self fetchUsers];
        _isCalled = YES;
    }
    
    self.navigationController.navigationBar.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

/*
- (BOOL) textField: (UITextField *)theTextField shouldChangeCharactersInRange: (NSRange)range replacementString: (NSString *)string {
    
    if(!theTextField.text.length){
        friendsArray = [[NSMutableArray alloc]init];
       
        [self loadContactList];
    }
    else{
        [self searchUserWithText:string];
    }
    return theTextField.text;
}
*/

-(void)textFieldDidChange:(UITextField*)textField
{
    searchTextString = textField.text;
    [self updateSearchArray];
}
//update seach method where the textfield acts as seach bar


-(void)updateSearchArray
{
    isFiltered = [self.txtSearch.text length] ? YES : NO;
    
    if (searchTextString.length != 0)
    {
        friendsFiltertedArray = [NSMutableArray array];
        
        for (int i = 0; i < friendsArray.count; i++)
        {

            BackendlessUser *user = [friendsArray objectAtIndex:i];
            NSString *name = user.name;
            if ([name.lowercaseString containsString:searchTextString.lowercaseString])
            {
                [friendsFiltertedArray addObject:user];
            }
        }
    }
    else
    {
        isFiltered = NO;
        friendsArray = [friendsArray mutableCopy];
    }
    
    
    // For Contacts
    
    if (searchTextString.length != 0)
    {
        contactsFiltertedArray = [NSMutableArray array];
        
        for (CNContact *contact in contactList)
        {
            
            NSString *firstName = contact.givenName;
            NSString *lastName = contact.familyName;
            if ([firstName.lowercaseString containsString:searchTextString.lowercaseString] || [lastName.lowercaseString containsString:searchTextString.lowercaseString] ) {
                [contactsFiltertedArray addObject:contact];
            }
        }
        
        
    }
    else
    {
        isFiltered = NO;
        contactList = [contactList mutableCopy];
    }
    
    //Reload data array after filtering
    if (isFiltered)
    {
        mergedAndSortedArray = [self sortContactsArrays:contactsFiltertedArray
                                        andFriendsArray:friendsFiltertedArray];
    }
    else {
        mergedAndSortedArray = [self sortContactsArrays:contactList
                                        andFriendsArray:friendsArray];
    }
    
    if (isFiltered)
    {
        self.appFriendsArray = [self sortContactsArrays:nil
                                        andFriendsArray:friendsFiltertedArray];
        self.inviteFriendsArray = [self sortContactsArrays:contactsFiltertedArray
                                           andFriendsArray:nil];
    }
    else
    {
        
        self.appFriendsArray = [self sortContactsArrays:nil
                                        andFriendsArray:friendsArray];
        self.inviteFriendsArray = [self sortContactsArrays:contactList
                                           andFriendsArray:nil];
    }
    
    [self.friendsTableView reloadData];
}

-(NSMutableArray *) sortContactsArrays:(NSArray<CNContact*>*)contactList andFriendsArray:(NSArray<BackendlessUser*>*)friendsArray{
    NSMutableArray *mergedArr = [NSMutableArray arrayWithArray:contactList];
    [mergedArr addObjectsFromArray:friendsArray];
    
    mergedArr = [[mergedArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
        NSString *string1 = @"";
        NSString *string2 = @"";
        if ([obj1 isKindOfClass:[CNContact class]]) {
             //NSString *lastName = contact.familyName;
            string1 = [obj1 valueForKey:@"givenName"];
            if(string1.length == 0 )
            {
                string1 = [obj1 valueForKey:@"familyName"];
            }
            
            
        } else {
            //BackendlessUser *user
            string1 = [obj1 valueForKey:@"name"];
        }
        
        if ([obj2 isKindOfClass:[CNContact class]]) {
            string2 = [obj2 valueForKey:@"givenName"];
            if(string2.length == 0)
            {
                string2 = [obj2 valueForKey:@"familyName"];
            }
            
        } else {
            //BackendlessUser *user
            //Assume having only name without second name
            string2 = [obj2 valueForKey:@"name"];
        }
        
        return [string1 compare:string2];
    }] copy];
    
    return mergedArr;
}

-(void)fetchUsers{
    
    [SVProgressHUD show];
    allUsersArray =[[NSMutableArray alloc]init];
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    
    @try {
//        BackendlessCollection *collection = [dataStore find:nil];
        
        [dataStore find:nil response:^(BackendlessCollection *gridColection) {
            
            for(BackendlessUser *user in gridColection.data)
            {
                [allUsersArray addObject:user];
            }
            [SVProgressHUD dismiss];
            [self loadContactList];
            
            }
        error:^(Fault *error) {
            NSLog(@"grid Collection Error  %@",error.detail);
            [SVProgressHUD dismiss];
            [self loadContactList];
        }];
        
    }
    @catch (Fault *fault) {
        NSLog(@"FAULT = %@ <%@>", fault.message, fault.detail);
        [SVProgressHUD dismiss];
        [self loadContactList];
    }
 
}

-(void)loadContactList {
     contactList =[[NSMutableArray alloc]init];
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    if( status == CNAuthorizationStatusDenied || status == CNAuthorizationStatusRestricted)
    {
        NSLog(@"access denied");
    }
    else
    {
        //Create repository objects contacts
        CNContactStore *contactStore = [[CNContactStore alloc] init];
        //Select the contact you want to import the key attribute  ( https://developer.apple.com/library/watchos/documentation/Contacts/Reference/CNContact_Class/index.html#//apple_ref/doc/constant_group/Metadata_Keys )
        
        //Here you need to add ALL keys that you are using later!
        NSArray *keys = [[NSArray alloc]initWithObjects:CNContactIdentifierKey,  CNContactFamilyNameKey, CNContactGivenNameKey,CNContactEmailAddressesKey, CNContactBirthdayKey, CNContactImageDataKey, CNContactPhoneNumbersKey, nil];
        
        // Create a request object
        CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
        request.predicate = nil;
        
        [contactStore enumerateContactsWithFetchRequest:request
                                                  error:nil
                                             usingBlock:^(CNContact* __nonnull contact, BOOL* __nonnull stop)
         {
             // Contact one each function block is executed whenever you get
             NSString *phoneNumber = @"";
             if( contact.phoneNumbers){
                 phoneNumber = [[[contact.phoneNumbers firstObject] value] stringValue];
             
             [contactList addObject:contact];
             }
             
             for (BackendlessUser *fUser in allUsersArray){
                 
                
                 if([[[[contact.phoneNumbers firstObject] value] stringValue] isEqualToString:[fUser valueForKey:@"phone"]]  || [[[contact.emailAddresses firstObject] value] isEqualToString:[fUser valueForKey:@"email"]]){
                     [friendsArray addObject:fUser];
                 }
             }

             temporaryFriendsArray = [[NSMutableArray alloc]initWithArray:friendsArray];
             for (int i =0;i<friendsArray.count;i++){
             
                 
                 if([[[[contact.phoneNumbers firstObject] value] stringValue] isEqualToString:[friendsArray[i] valueForKey:@"phone"]] || [[[contact.emailAddresses firstObject] value] isEqualToString:[friendsArray[i] valueForKey:@"email"]]){
                     [contactList removeObject:contact];
                 }

                 if([[[contact.emailAddresses firstObject] value] isEqualToString:[friendsArray[i] valueForKey:@"email"]]){
                     [contactList removeObject:contact];
             }
             
             }
}];
         contactList = [[contactList sortedArrayUsingComparator:^NSComparisonResult(CNContact *p1, CNContact *p2){
             return [p1.givenName compare:p2.givenName];
         }] copy];

        
        [self updateSearchArray];
        //[self.friendsTableView reloadData];
    }
    
}

-(void)fetchFacebookFriends{
    fbFriendsArray =[[NSMutableArray alloc]init];

    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/me/friends?fields=name,picture"
                                  parameters:nil
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        // Handle the result
        fbFriendsArray=[result objectForKey:@"data"];
        NSLog(@"%lu", (unsigned long)fbFriendsArray.count);
        //[self.friendsTableView reloadData];
    }];
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - tableView delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    //was 2 for unsorted data

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.segmentController.selectedSegmentIndex == 0)
    {
        return self.appFriendsArray.count;
    }
    else
    {
        return self.inviteFriendsArray.count;
    }
    
    {
        if (mergedAndSortedArray != nil)
        {
            return  mergedAndSortedArray.count;
        }
    }
    if (section==0)
    {
        if (isFiltered)
        {
            return friendsFiltertedArray.count;
        }
        else
        {
            return friendsArray.count;
        }
    }
    else if(section==1){
        
        if (isFiltered) {
            return contactsFiltertedArray.count;
        }
        else {
            return contactList.count;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"friendsCell";
    
    FriendListTableViewCell  *cell = [_friendsTableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[FriendListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:MyIdentifier];
    }
    id contact;
//    if(tableView.tag == 0)
//    {
//        if(self.segmentController.selectedSegmentIndex == 0)
//        {
//            contact=  [self.appFriendsArray objectAtIndex:indexPath.row];
//        }
//        else
//        {
//            contact=  [self.inviteFriendsArray objectAtIndex:indexPath.row];
//        }
//    }
//    else
//    {
//        contact=  [mergedAndSortedArray objectAtIndex:indexPath.row];
//    }
    
    if(self.segmentController.selectedSegmentIndex == 0)
    {
        contact=  [self.appFriendsArray objectAtIndex:indexPath.row];
    }
    else
    {
        contact=  [self.inviteFriendsArray objectAtIndex:indexPath.row];
    }
    
    
    if([self.selectedfriendsForChatArray containsObject:contact])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    NSInteger i = indexPath.row;
    cell.showProfileBrn.tag = i;
    [cell.showProfileBrn addTarget:self action:@selector(showProfileBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.inviteBtn setTag:indexPath.row];
    [cell.inviteBtn addTarget:self action:@selector(inviteBtnclicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view layoutIfNeeded];
    [cell layoutIfNeeded];
    
    [self populateContactCell:cell withContactItem:contact];
    return cell;
    
 ///Speghetti code, hard to hadnle and support, refactored a bit to use merged data array
    
//     if (indexPath.section==0) {
//
//        if (isFiltered) {
//            cell.userName.text= [NSString stringWithFormat:@"%@",[friendsFiltertedArray[i] valueForKey:@"name"]];
//            cell.lblPhone.text=@"phone No";
//            cell.lblEmail.text=[NSString stringWithFormat:@"%@",[friendsFiltertedArray[i] valueForKey:@"email"]];        [cell.userProfileImage  sd_setImageWithURL:[NSURL URLWithString:[friendsFiltertedArray[i] valueForKey:@"profileImage"]]
//                                                                                                                                          placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
//                                                                                                                                                   options:SDWebImageRefreshCached];
//
//
//            cell.showProfileBrn.tag = i;
//            [cell.showProfileBrn addTarget:self action:@selector(showProfileBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
//
//
//            [cell.userProfileImage.layer setCornerRadius:cell.userProfileImage.frame.size.height/2];
//            cell.userProfileImage.clipsToBounds=YES;
//            cell.inviteBtn.hidden=YES;
//
//            return cell;
//        }
//        else{
//            cell.userName.text= [NSString stringWithFormat:@"%@",[friendsArray[i] valueForKey:@"name"]];
//            cell.lblPhone.text=@"phone No";
//            cell.lblEmail.text=[NSString stringWithFormat:@"%@",[friendsArray[i] valueForKey:@"email"]];        [cell.userProfileImage  sd_setImageWithURL:[NSURL URLWithString:[friendsArray[i] valueForKey:@"profileImage"]]
//                                                                                                                                          placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
//                                                                                                                                                   options:SDWebImageRefreshCached];
//
//
//            cell.showProfileBrn.tag = i;
//            [cell.showProfileBrn addTarget:self action:@selector(showProfileBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
//
//
//            [cell.userProfileImage.layer setCornerRadius:cell.userProfileImage.frame.size.height/2];
//            cell.userProfileImage.clipsToBounds=YES;
//            cell.inviteBtn.hidden=YES;
//
//            return cell;
//        }
//    }
//    else if(indexPath.section == 1){
//        [self.view layoutIfNeeded];
//        [cell layoutIfNeeded];
//
//
//        if (isFiltered) {
//            CNContact *contact=  [contactsFiltertedArray objectAtIndex:indexPath.row];
//
//            [self populateContactCell:cell withContactItem:contact];
//            [cell.inviteBtn setTag:indexPath.row];
//            [cell.inviteBtn addTarget:self action:@selector(inviteBtnclicked:) forControlEvents:UIControlEventTouchUpInside];
//
//            return cell;
//        }
//        else {
//            CNContact *contact=  [contactList objectAtIndex:indexPath.row];
//            [self populateContactCell:cell withContactItem:contact];
//            [cell.inviteBtn setTag:indexPath.row];
//            [cell.inviteBtn addTarget:self action:@selector(inviteBtnclicked:) forControlEvents:UIControlEventTouchUpInside];
//            return cell;
//        }
//    }
//
    
    return 0;
}

-(void) populateContactCell:(FriendListTableViewCell*)cell
                                withContactItem:(id)item{
    if ([item isKindOfClass:[CNContact class]]) {
        
        CNContact *contact = (CNContact *)item;
        cell.userName.text= [NSString stringWithFormat:@"%@ %@",contact.givenName,contact.familyName];
        cell.lblPhone.text=[[[contact.phoneNumbers firstObject] value] stringValue];
        cell.lblEmail.text=[[contact.emailAddresses firstObject] value];
        [cell.userProfileImage.layer setCornerRadius:cell.userProfileImage.frame.size.height/2];
        cell.userProfileImage.clipsToBounds=YES;
        [cell.userProfileImage setImage:[UIImage imageNamed:@"avatar.png"]];
        cell.inviteBtn.layer.cornerRadius = 5;
        cell.inviteBtn.clipsToBounds = YES;
        
        cell.showProfileBrn.enabled=NO;
        cell.inviteBtn.hidden=NO;

    }
    else
    {
        BackendlessUser *user = item;
        cell.userName.text= [NSString stringWithFormat:@"%@",[user valueForKey:@"name"]];
        cell.lblPhone.text=@"phone No";
        cell.lblEmail.text=[NSString stringWithFormat:@"%@",[user valueForKey:@"email"]];
        [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:[user valueForKey:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"avatar.png"] options:SDWebImageRefreshCached];
        
        [cell.userProfileImage.layer setCornerRadius:cell.userProfileImage.frame.size.height/2];
        cell.userProfileImage.clipsToBounds=YES;
        cell.inviteBtn.hidden=YES;
        
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FriendListTableViewCell  *cell = [_friendsTableView cellForRowAtIndexPath:indexPath];
    if ([self.delegate respondsToSelector:@selector(tellRegisterDelegateSomething:)])
    {
        if(self.segmentController.selectedSegmentIndex == 0)
        {
            
        }
        else // Invite Friend
        {
            CNContact *contact=  [self.inviteFriendsArray objectAtIndex:indexPath.row];
            if([contact.phoneNumbers count] == 0)
            {
                [self displayNoPhoneAlert];
                return;
            }
            selectedContact = [[[contact.phoneNumbers firstObject] value] stringValue];
//            NSLog(@"%@",selectedContact);
            [self showSMS:selectedContact];
            return;
        }
        [self.doneSelectContactbtn setHidden:NO];
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            if (indexPath.section==1) // This never will execute, only 1 section in table view
            {
                if (isFiltered)
                {
                    [self.selectedfriendsForChatArray removeObject:contactsFiltertedArray[indexPath.row]];
                    NSLog(@"%lu",(unsigned long)self.selectedfriendsForChatArray.count);
                }
                else
                {
                    [self.selectedfriendsForChatArray removeObject:contactList[indexPath.row]];
                    NSLog(@"%lu",(unsigned long)self.selectedfriendsForChatArray.count);
                }
                
            }
            else
            {
                if (isFiltered)
                {
                    [self.selectedfriendsForChatArray removeObject:self.appFriendsArray[indexPath.row]];
                    NSLog(@"%lu",(unsigned long)self.selectedfriendsForChatArray.count);
                }
                else
                {
                    [self.selectedfriendsForChatArray removeObject:self.appFriendsArray[indexPath.row]];
                    NSLog(@"%lu",(unsigned long)self.selectedfriendsForChatArray.count);
                }
            }
        }
        else
        {
            
            if (indexPath.section==1)
            {
                if (isFiltered)
                {
                    [self.selectedfriendsForChatArray addObject:contactsFiltertedArray[indexPath.row]];
                    NSLog(@"%lu",(unsigned long)self.selectedfriendsForChatArray.count);
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                else
                {
                    
                    [self.selectedfriendsForChatArray addObject:contactList[indexPath.row]];
                    NSLog(@"%lu",(unsigned long)self.selectedfriendsForChatArray.count);
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                
            }
            else
            {
                if (isFiltered)
                {
//                    mergedAndSortedArray
                    [self.selectedfriendsForChatArray addObject:self.appFriendsArray[indexPath.row]];
                    NSLog(@"%lu",(unsigned long)self.selectedfriendsForChatArray.count);
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                else
                {
                    [self.selectedfriendsForChatArray addObject:self.appFriendsArray[indexPath.row]];
                    NSLog(@"%lu",(unsigned long)self.selectedfriendsForChatArray.count);
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
            }
        }

    }

    else
    {
        if (indexPath.section==0)
        {
            /*
            if (isFiltered)
            {
                CNContact *contact=  [contactsFiltertedArray objectAtIndex:indexPath.row];
                selectedContact = [[[contact.phoneNumbers firstObject] value] stringValue];
//                NSLog(@"%@",selectedContact);
                [self showSMS:selectedContact];
            }
            else
            {
                CNContact *contact=  [contactList objectAtIndex:indexPath.row];
                selectedContact = [[[contact.phoneNumbers firstObject] value] stringValue];
//                NSLog(@"%@",selectedContact);
                [self showSMS:selectedContact];
            }
            */
            
            if(self.segmentController.selectedSegmentIndex == 0) // Start chat with app friends
            {
                self.selectedUser = self.appFriendsArray[indexPath.row];
                [self performSegueWithIdentifier:@"friendToMessage" sender:self];
            }
            else // Invite friends
            {
                CNContact *contact=  [self.inviteFriendsArray objectAtIndex:indexPath.row];
                if([contact.phoneNumbers count] == 0)
                {
                    [self displayNoPhoneAlert];
                    return;
                }
                
                selectedContact = [[[contact.phoneNumbers firstObject] value] stringValue];
                //                NSLog(@"%@",selectedContact);
                
                [self showSMS:selectedContact];
            }
        }
        else
        {
            
            if (isFiltered)
            {
                self.selectedUser = friendsFiltertedArray[indexPath.row];
                [self performSegueWithIdentifier:@"friendToMessage" sender:self];
            }
            else
            {
                self.selectedUser = friendsArray[indexPath.row];
                [self performSegueWithIdentifier:@"friendToMessage" sender:self];
            }
        }
    }
}

-(void)showProfileBtnClicked:(UIButton *)sender
{
    self.selectedUser = friendsArray[sender.tag];
    [self performSegueWithIdentifier:@"showProfile" sender:nil];
}

-(void)inviteBtnclicked:(UIButton *)sender{
    
    if (isFiltered) {
        CNContact *contact= contactsFiltertedArray[sender.tag];
        if([contact.phoneNumbers count] == 0)
        {
            [self displayNoPhoneAlert];
            return;
        }
        selectedContact = [[[contact.phoneNumbers firstObject] value] stringValue];
        [self showSMS:selectedContact];
    }
    else {
        CNContact *contact= contactList[sender.tag];
        if([contact.phoneNumbers count] == 0)
        {
            [self displayNoPhoneAlert];
            return;
        }
        selectedContact = [[[contact.phoneNumbers firstObject] value] stringValue];
//        NSLog(@"%@",selectedContact);
        [self showSMS:selectedContact];
    }
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"showProfile"]) {
        FriendProfileViewController *vc = segue.destinationViewController ;
        vc.selectedUser = self.selectedUser ;
    }
    if ([segue.identifier isEqualToString:@"BackWithSelectedFriends"]) {
        ShareGridsViewController *vc = segue.destinationViewController ;
        vc.selectedFriendsArray=self.selectedfriendsForChatArray;
        
    }
    if ([segue.identifier isEqualToString:@"friendToMessage"]) {
        FlatChatViewController *vc = segue.destinationViewController ;
        vc.selectedUser=self.selectedUser;
        
    }
}


- (void)showSMS:(NSString*)file {
    
    if(![MFMessageComposeViewController canSendText]) {
        
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Your device doesn't support SMS!" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
       
        [self presentViewController:alertController animated:YES completion:nil];
               return;
    }
    
    NSArray *recipent = @[selectedContact];
    NSLog(@"%@",recipent);
    NSString *message = [NSString stringWithFormat:@"Hi! Check out uChoose, it's a great app to use for help with decisions!"];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipent];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
    
    
    /*
    NSString *text = @"Click on this link!!";
    NSURL *url = [NSURL URLWithString:@"http://google.com"];
    
    NSArray *itemsToShare = @[text, url];
    
    UIActivity *activity = [[UIActivity alloc] init];
    NSArray *applicationActivities = [[NSArray alloc] initWithObjects:activity, nil];
    UIActivityViewController *activityVC =
    [[UIActivityViewController alloc] initWithActivityItems:itemsToShare
                                      applicationActivities:applicationActivities];
    
    [self presentViewController:activityVC
                       animated:YES
                     completion:nil];
    */
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Failed to send SMS!" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
           
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (IBAction)unwindToFriends:(UIStoryboardSegue*)sender
{
    
}
- (IBAction)friendsBackAction:(id)sender {
    if (self.parentViewController) {
        [self .navigationController popViewControllerAnimated:YES];
        
        if ([self.delegate respondsToSelector:@selector(tellRegisterDelegateSomething:)])
        {
            [self performSegueWithIdentifier:@"BackWithSelectedFriends" sender:self];
        }
        else
        {
            [self performSegueWithIdentifier:@"contactsToHome" sender:self];
        }
    }else{
        [self dismissViewControllerAnimated:true completion:nil];
    }
}
- (IBAction)doneSelectContactAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"BackWithSelectedFriends" sender:self];
}


- (IBAction)searchBackAction:(UIButton *)sender {
    isFiltered = NO;
    self.txtSearch.text = @"";
    [self.txtSearch resignFirstResponder];
    friendsArray = [[NSMutableArray alloc]init];
    [self.searchView setHidden:YES];
    [self loadContactList];
}
- (IBAction)searchGoAction:(UIButton *)sender {
    
    [self doneSelectContactAction:sender];

    //[self searchUserWithText:self.txtSearch.text ];
}
-(void)searchUserWithText:(NSString *)text{

    //contactList = [[NSMutableArray alloc]init];
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    for (BackendlessUser *user in temporaryFriendsArray) {
        if([user.name rangeOfString:text].location == NSNotFound){
            
            
        }
        else{
            [tempArray addObject:user];
        }
    }
    
    friendsArray = [[NSMutableArray alloc]initWithArray:tempArray];
    
    
    NSMutableArray *tempArray2 = [[NSMutableArray alloc]init];
    for (CNContact *contact in contactList) {
        if([contact.givenName rangeOfString:text].location == NSNotFound){
            
            
        }
        else{
            [tempArray2 addObject:contact];
        }
    }
    
    contactList = [[NSMutableArray alloc]initWithArray:tempArray2];
    
    [self updateSearchArray];
    //[self.friendsTableView reloadData];


}

- (IBAction)searchAction:(UIButton *)sender {
    [self.searchView setHidden:NO];
    [self resignFirstResponder];
}
-(IBAction)SegemntControllerClicked:(id)sender
{
    UISegmentedControl *Segment = (UISegmentedControl *) sender;
    
    if(Segment.selectedSegmentIndex == 0)
    {
        
    }
    else
    {
        
    }
    [self.friendsTableView reloadData];
}
-(void)displayNoPhoneAlert
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"No phone number found for this contact." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
    return;
}
@end
