//
//  BrowserViewController.m
//  pic4me
//
//  Created by ibuildx on 11/23/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//
#import "AMSmoothAlertView.h"
#import "BrowserViewController.h"
#import "ShareGridsViewController.h"
#import "SVProgressHUD.h"
#import <SwiftLinkPreview/SwiftLinkPreview-Swift.h>
#import "UIImageView+WebCache.h"


@interface BrowserViewController () <UIGestureRecognizerDelegate, UIWebViewDelegate> {
    
    NSArray *resultDict;
    NSMutableArray *resultDictModified;
    UIImage *imagefromUrl;
    
    
    BOOL theBool;
    NSTimer *myTimer;
    
    NSMutableDictionary<NSString *,id> *responseForPreview;
    
    NSArray *carouselImages;
}

@end

@implementation BrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.delegate = self;
    self.txtFieldUrl.delegate = self;
    [self.complitionVie setHidden:NO];
    
    NSURL *url = [NSURL URLWithString:@"http://www.google.com"];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
    // Do any additional setup after loading the view.
    /*
    UILongPressGestureRecognizer* longTap = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longTapGesture:)];
    [longTap setMinimumPressDuration:0.3];
    longTap.delegate = self;
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:nil];
    tap.delegate = self;
    [tap requireGestureRecognizerToFail:longTap];
    
    [self.webView addGestureRecognizer:longTap];
    [self.webView addGestureRecognizer:tap];
     */
    self.webView.delegate = self;
    
    self.imagescarousel.delegate = self;
    self.imagescarousel.dataSource = self;

}
    
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"uChoose" message:@"Open a Link and Press Done to Set the Link." preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:ac animated:true completion:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    if (myTimer) {
        [myTimer invalidate];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
    
- (void)longTapGesture:(UILongPressGestureRecognizer*)gestureRecognizer {
    NSLog(@"longTapGesture");
    
}
    
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    NSLog(@"TAPPED %@", gestureRecognizer);
    
    
    return YES;
}

-(void)gtUrlAndTitle{
   
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if(imagefromUrl != nil)
    {
        if ([segue.identifier isEqualToString:@"webToShareGrid"])
        {
            ShareGridsViewController *vc = segue.destinationViewController ;
            vc.imageFromUrl=imagefromUrl;
            vc.imageURL = self.imageUrl;
            vc.responseForPreview = responseForPreview;
        }
    }
    if(responseForPreview != nil)
    {
        if ([segue.identifier isEqualToString:@"webToShareGrid"])
        {
            ShareGridsViewController *vc = segue.destinationViewController ;
            vc.responseForPreview = responseForPreview;
        }
    }
    if ([segue.identifier isEqualToString:@"webToUploadGrid"]) {
        
    }
}
- (IBAction)goAction:(UIButton *)sender {
    self.carouselSuperView.hidden = YES;
    resultDict = [[NSArray alloc] init];
    [self.view endEditing:YES];
}

- (IBAction)PActionTriggered:(UITextField *)sender {
    NSLog(@"TRIGERRED............................");
    [self.view endEditing:YES];
    
    NSString *urlStr = self.txtFieldUrl.text ;
    NSURL *url;
    
    if ([urlStr.lowercaseString hasPrefix:@"http://"] || [urlStr.lowercaseString hasPrefix:@"https://"]) {
        url = [NSURL URLWithString:urlStr];
    } else {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",urlStr]];
    }
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView setScalesPageToFit:YES];
    [self.webView loadRequest:requestObj];
}

-(IBAction)doneAction:(UIButton *)sender {
    NSURL *currentURL = self.webView.request.URL;
    NSLog(@"____________________________%@",currentURL.absoluteString);
        
    [SVProgressHUD showWithStatus:@"Fetching Preview"];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"0a5ef27c-bd90-ffa8-a6e3-5f64ccdbf5d7" };
    
    //NSString *urlString = [NSString stringWithFormat:@"https://graph.facebook.com/v3.0/?scrape=true&id=%@&access_token=105974196513226%7CxFUrg0GoaOirGC9WADTxamzUTd4" , @"https://www.amazon.com/dp/B07B3WCSQ1/ref=sspa_mw_detail_1?psc=1"];
    //@"https://www.amazon.com/dp/B07B3WCSQ1/ref=sspa_mw_detail_1?psc=1"
    NSString *unescaped = currentURL.absoluteString;
    NSString *escapedString = [unescaped stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSLog(@"escapedString: %@", escapedString);
    
    
    NSString *escapedStringToken = [@"105974196513226|xFUrg0GoaOirGC9WADTxamzUTd4" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];

    
    NSString *urlString = [NSString stringWithFormat:@"https://graph.facebook.com/v3.0/?scrape=true&id=%@&access_token=%@",escapedString , escapedStringToken];

    //105974196513226|xFUrg0GoaOirGC9WADTxamzUTd4
    
    
    
    NSLog(@"urlString: %@", urlString);

    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            if (error) {
                NSLog(@"ERROR GOTTEN = %@" , error.localizedDescription);
                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Failed" andText:@"Cannot get link information from this website." andCancelButton:NO forAlertType:AlertFailure];
                
                [alert show];
                
            } else {
                NSError* error;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                if (json[@"error"] != nil) {
                    
                    
                    SwiftLinkPreview *linkPreview = [[SwiftLinkPreview alloc] init];
                    
                    [SVProgressHUD showWithStatus:@"Fetching Images..."];
                    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
                    
                    
                    [linkPreview previewLink:currentURL.absoluteString onSuccess:^(NSDictionary<NSString *,id>  * _Nonnull responseDict) {
                        [SVProgressHUD dismiss];
                        NSLog(@"responseDict GOTTEN = %@" , responseDict);
                        if (responseDict[@"images"]) {
                            carouselImages = [[NSArray alloc] initWithArray:responseDict[@"images"]];
                            self.carouselSuperView.hidden = NO;
                            
                            NSMutableDictionary *responseDictMutable = [[NSMutableDictionary alloc] initWithDictionary:responseDict];
                            
                            
                            [responseDictMutable removeObjectForKey:@"image"];
                            
                            responseForPreview = responseDictMutable;

                        }
                        else{
                            carouselImages = [[NSArray alloc] init];
                            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Failed" andText:@"No Images Found in this Link." andCancelButton:NO forAlertType:AlertFailure];
                            
                            [alert show];
                            self.carouselSuperView.hidden = YES;

                        }
                        [self.imagescarousel reloadData];
                        
                        
                    } onError:^(NSError * _Nonnull error) {
                        NSLog(@"ERROR GOTTEN = %@" , error.localizedDescription);
                        [SVProgressHUD dismiss];
                        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Failed" andText:@"Cannot get link information from this website." andCancelButton:NO forAlertType:AlertFailure];
                        
                        [alert show];
                    }];
                    
                    
                    
                }
                else{
                    responseForPreview =  [[NSMutableDictionary alloc] initWithDictionary:json];
                    if ([self.delegate respondsToSelector:@selector(uploadGridToBrowser:)]){
                        [self performSegueWithIdentifier:@"webToUploadGrid" sender:self];
                    }
                    else if ([self.delegate respondsToSelector:@selector(shareGridToBrowser:)]){
                        [self performSegueWithIdentifier:@"webToShareGrid" sender:self];
                    }
                }
                NSLog(@"json: %@", json);
            }
        });
        
    }];
    [dataTask resume];
    /*
    SwiftLinkPreview *linkPreview = [[SwiftLinkPreview alloc] init];
    
    [linkPreview previewLink:currentURL.absoluteString onSuccess:^(NSDictionary<NSString *,id>  * _Nonnull responseDict) {
        [SVProgressHUD dismiss];
        NSLog(@"responseDict GOTTEN = %@" , responseDict);
        responseForPreview = responseDict;
        
        if ([self.delegate respondsToSelector:@selector(uploadGridToBrowser:)])
        {
            [self performSegueWithIdentifier:@"webToUploadGrid" sender:self];
        }
        else if ([self.delegate respondsToSelector:@selector(shareGridToBrowser:)])
        {
            [self performSegueWithIdentifier:@"webToShareGrid" sender:self];
        }
        
        
    } onError:^(NSError * _Nonnull error) {
        NSLog(@"ERROR GOTTEN = %@" , error.localizedDescription);
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Failed" andText:@"Cannot get link information from this website." andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];
    }];
     */
}

- (IBAction)backAction:(UIButton *)sender {
    [self.complitionVie setHidden:YES];
}

- (IBAction)backToShareGrid:(UIButton *)sender {
    [SVProgressHUD dismiss];
    if([self.webView canGoBack]){
        [self.webView goBack];
    }
    //[self performSegueWithIdentifier:@"webToShareGrid" sender:self];
}

- (IBAction)txtUrlDoneAction:(id)sender {
    [self.view endEditing:YES];
   

    NSString *urlStr = self.txtFieldUrl.text ;
    NSURL *url;
    
    if ([urlStr.lowercaseString hasPrefix:@"http://"] || [urlStr.lowercaseString hasPrefix:@"https://"]) {
        url = [NSURL URLWithString:urlStr];
    } else {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",urlStr]];
    }
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView setScalesPageToFit:YES];
    [self.webView loadRequest:requestObj];
    //self.txtFieldUrl.text = @"";
}

- (IBAction)selectImageForCarouselAction:(UIButton *)sender {
    
    NSDictionary *imageDict = [NSDictionary dictionaryWithObject:[carouselImages objectAtIndex:self.imagescarousel.currentItemIndex] forKey:@"url"];
    
    
    [responseForPreview setObject:[NSArray arrayWithObjects:imageDict, nil] forKey:@"image"];
    
    
    
    if ([self.delegate respondsToSelector:@selector(uploadGridToBrowser:)]){
        [self performSegueWithIdentifier:@"webToUploadGrid" sender:self];
    }
    else if ([self.delegate respondsToSelector:@selector(shareGridToBrowser:)]){
        [self performSegueWithIdentifier:@"webToShareGrid" sender:self];
    }
}

#pragma mark - Webview Delegate Methods

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self.webProgressView setProgress:1.0 animated:YES];
    NSLog(@"%@",error.localizedDescription);
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self.webProgressView setProgress:0.0 animated:NO];
    theBool = false;
    //0.01667 is roughly 1/60, so it will update at 60 FPS
    myTimer = [NSTimer scheduledTimerWithTimeInterval:0.01667 target:self selector:@selector(timerCallback) userInfo:nil repeats:YES];

}

-(void)timerCallback {
    if (theBool) {
        if (self.webProgressView.progress >= 1) {
            [myTimer invalidate];
        }
        else {
            self.webProgressView.progress += 0.1;
        }
    }
    else {
        self.webProgressView.progress += 0.05;
        if (self.webProgressView.progress >= 0.95) {
            self.webProgressView.progress = 0.95;
        }
    }
}

-(void)showErrorWithMessage:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:action];
    [self presentViewController:alert animated:YES completion:nil];
}


-(void)webViewDidFinishLoad:(UIWebView *)webView{
    theBool = true;
    [self.webProgressView setProgress:1.0 animated:YES];
//self.txtFieldUrl.text = webView.url
    NSURL *currentURL = [[webView request] URL];
    NSLog(@"____________________________%@",currentURL.absoluteString);
    
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.webkitTouchCallout='none'; document.body.style.KhtmlUserSelect='none'"];
    
    NSString* urrentURL = self.webView.request.mainDocumentURL.absoluteString;
    NSLog(@"%@",urrentURL);
    
    NSString *resultStr = [self.webView stringByEvaluatingJavaScriptFromString:
                           @"(function(){"
                           "var i, metaFields = document.getElementsByTagName(\"meta\"), result = [];"
                           "for (i = 0; i< metaFields.length; i++) {"
                           
                           "var key = metaFields[i].getAttribute(\"property\");"
                           "var obj = {};"
                           "obj[key]= metaFields[i].content;"
                           
                           "result.push(obj);"
                           
                           
                           "}"
                           "return JSON.stringify(result);"
                           "})();"
                           ];
    resultDict = [NSJSONSerialization JSONObjectWithData:[resultStr dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
    resultStr = [resultStr stringByReplacingOccurrencesOfString:@"|" withString:@""];
    
    
    [self.complitionVie setHidden:NO];
    
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel{
    return carouselImages.count;
}


- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, carousel.frame.size.width, carousel.frame.size.height)];
        ((UIImageView *)view).image = [UIImage imageNamed:@"placeHolder"];
        view.contentMode = UIViewContentModeScaleAspectFit;
        
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:50];
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    NSString *imageStr = [carouselImages objectAtIndex:index];
    
    [(UIImageView *)view sd_setImageWithURL:[NSURL URLWithString:imageStr]
           placeholderImage:[UIImage imageNamed:@"placeHolder"]
                    options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        if (error) {
                            ((UIImageView *)view).image = [UIImage imageNamed:@"placeHolder"];
                        }
                        else{
                            ((UIImageView *)view).image = image;
                        }
                        [(UIImageView *)view.superview layoutIfNeeded];
                    }];
    
    
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    return view;
}

@end
