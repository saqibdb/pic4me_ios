//
//  SharedViewController.m
//  pic4me
//
//  Created by ibuildx on 11/21/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//
#import "SingleContentViewController.h"
#import "SingleContentCommentsTableViewCell.h"
#import "UIViewController+CWPopup.h"
#import "Grid.h"
#import "SharedViewController.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "Photo.h"
#import "Rating.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SingleContentViewController.h"
#import "sharePopUp.h"
#import <Social/Social.h>
#import "Video.h"
#import "Comments.h"
#import "Utility.h"
#import "uChoose-Bridging-Header.h"
#import "CustomImageView.h"
#import <SafariServices/SafariServices.h>
#import "LinkPreview.h"

//#import <INSPhotoGallery/INSPhotoGallery-Swift.h>

@interface SharedViewController ()<SFSafariViewControllerDelegate>

{
    
    UIButton *playBTN;
    NSInteger ViewTag;
    Grid *gridToDisplay;
    NSMutableArray *allPhotosAndVideosArray;
    Photo *photoClass;
    NSArray *viewsToSet ;
    NSArray *templates;
    NSUInteger thumbsUpCount ;
    NSInteger *thumbsDownCount ;
    
    Rating *alreadyRatingForCurrentUser;
    
    Rating *alreadyRatingGridA;
    Rating *alreadyRatingGridB;
    Rating *alreadyRatingGridC;
    Rating *alreadyRatingGridD;
    
    
    
    Rating *alreadyRatingDownGridA;
    Rating *alreadyRatingDownGridB;
    Rating *alreadyRatingDownGridC;
    Rating *alreadyRatingDownGridD;
    
    
    
    
    
    NSMutableArray *allRatings ;
}

@end

@implementation SharedViewController 

- (void)viewDidLoad {
    NSLog(@"SharedViewController");
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    appDel = (AppDelegate *) [[UIApplication sharedApplication] delegate];
//    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
//    doubleTapRecognizer.numberOfTapsRequired = 2;
//    doubleTapRecognizer.numberOfTouchesRequired = 1;
//    [self.imageScrollView addGestureRecognizer:doubleTapRecognizer];
//
//    // Add two finger recognizer to the scrollView
//    UITapGestureRecognizer *twoFingerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTwoFingerTapped:)];
//    twoFingerTapRecognizer.numberOfTapsRequired = 1;
//    twoFingerTapRecognizer.numberOfTouchesRequired = 2;
//    [self.imageScrollView addGestureRecognizer:twoFingerTapRecognizer];
    [self.imageScrollView setHidden:YES];
    allPhotosAndVideosArray = [[NSMutableArray alloc]init];
    self.writeCommntTxt.delegate = self;
    self.writeCommentTxtField.delegate = self;
    self.senderTag = _grid.style;
    self.container_a.hidden = self.senderTag != 0 ;
    self.container_b.hidden = self.senderTag != 1 ;
    self.container_c.hidden = self.senderTag != 2 ;
    self.container_d.hidden = self.senderTag != 3 ;
    self.container_e.hidden = self.senderTag != 4 ;
    self.container_f.hidden = self.senderTag != 5 ;
    
    [self.gridUserProfileImage.layer setCornerRadius:self.gridUserProfileImage.frame.size.height/2];
    self.gridUserProfileImage.clipsToBounds=YES;
    self.gridUserProfileImage.layer.borderWidth=1.5;
    self.gridUserProfileImage.layer.borderColor=[[UIColor blackColor] CGColor];
    
    [self.gridUserProfileImage sd_setImageWithURL:[NSURL URLWithString:[self.grid.User getProperty:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"avatar.png"] options:SDWebImageRefreshCached];
    
    self.gridUserName.text = _grid.User.name;
    if (_grid.location != nil) {
        self.lblLocation.text = _grid.location;
    }
    else{
        self.lblLocation.text = @" ";
    }
    if (_grid.comment != nil) {
        self.lblComment.text = _grid.comment;
    }
    NSString *commentDateString = [Utility getTimeFormatForDate:_grid.created] ;
    self.lblTime.text = commentDateString;
    
    [self setGrids];
    
    templates = [[NSArray alloc] initWithObjects:@"Love it!", @"You look ridiculous", @"wow ",@"nice look dude",@"awesome", @"Lit", @"Lit AF", @"Dope", @"No comment", nil];
    
    self.commentsTableView.dataSource =self;
    self.commentsTableView.delegate=self;
    self.commentsTableView.estimatedRowHeight = 100;
    self.commentsTableView.rowHeight = UITableViewAutomaticDimension;
    self.templateCommetnsTableView.dataSource =self;
    self.templateCommetnsTableView.delegate=self;
    [self fetchComments];
//    [self fetchAllRatings];
    
    NSInteger numberOfGrids = 0;
    
    if(_grid.style == 0)
    {
        numberOfGrids = 4;
    }
    else if(_grid.style == 1)
    {
        numberOfGrids = 3;
    }
    else if(_grid.style == 2)
    {
        numberOfGrids = 4;
    }
    else if(_grid.style == 3)
    {
        numberOfGrids = 3;
    }
    else if(_grid.style == 4)
    {
        numberOfGrids = 2;
    }
    else if(_grid.style == 5)
    {
        numberOfGrids = 1;
    }
    [self.thumbsUpA setTitle:@"0" forState:UIControlStateNormal];
    [self.thumbsUpB setTitle:@"0" forState:UIControlStateNormal];
    [self.thumbsUpC setTitle:@"0" forState:UIControlStateNormal];
    [self.thumbsUpD setTitle:@"0" forState:UIControlStateNormal];
    
    [self.thumbsDownA setTitle:@"0" forState:UIControlStateNormal];
    [self.thumbsDownB setTitle:@"0" forState:UIControlStateNormal];
    [self.thumbsDownC setTitle:@"0" forState:UIControlStateNormal];
    [self.thumbsDownD setTitle:@"0" forState:UIControlStateNormal];
    
    self.thumsUpCountA = 0;
    self.thumsUpCountB = 0;
    self.thumsUpCountC = 0;
    self.thumsUpCountD = 0;
    
    self.thumsDownCountA = 0;
    self.thumsDownCountB = 0;
    self.thumsDownCountC = 0;
    self.thumsDownCountD = 0;
    
    for (int i = 0 ; i<allPhotosAndVideosArray.count;i++)
    {
        if (i >= numberOfGrids)
        {
            break;
        }
        if(i==0)
        {
            [self FetchRatingsForGridA:i];
        }
        else if(i==1)
        {
            [self FetchRatingsForGridB:i];
        }
        else if(i==2)
        {
            [self FetchRatingsForGridC:i];
        }
        else if(i==3)
        {
            [self FetchRatingsForGridD:i];
        }
    }
    
    //[self fetchThumbsUps];
    //[self fetchThumbsdown];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
//    [self.view addGestureRecognizer:tap];
    
    self.writeCommentTxtField.enablesReturnKeyAutomatically = NO;
    self.writeCommntTxt.enablesReturnKeyAutomatically = NO;
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [_playerViewController.player.currentItem removeObserver:self forKeyPath:@"status"];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
-(void)FetchRatingsForGridA:(NSInteger )index
{
    id obj = allPhotosAndVideosArray[index];
    NSString *itemsObject;
    NSString *itemsObjectId;
    if ([obj isKindOfClass:[Photo class]])
    {
        Photo *photo = obj;
        itemsObjectId = photo.objectId;
        itemsObject = @"photo";
    }
    else
    {
        Video *video = obj;
        itemsObjectId = video.objectId;
        itemsObject = @"video";
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"%@.objectId =\'%@\' ",itemsObject,itemsObjectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Rating class]];
    [dataStore find:query response:^(BackendlessCollection *foundRatings) {
        NSInteger upCount = 0;
        NSInteger downCount = 0;
        if (foundRatings.data.count)
        {
            NSArray *tempAllRating = [[NSMutableArray alloc] initWithArray:foundRatings.data];
            ratingsGridA = [[NSMutableArray alloc] initWithArray:tempAllRating];
            
            for (Rating *rating in tempAllRating)
            {
                if ([rating.type isEqualToString:@"thumbsUp"])
                {
                    [self.thumbsUpFound addObject:rating];
                    upCount = upCount + 1;
                }
                else
                {
                    [self.thumbsDownFound addObject:rating];
                    downCount = downCount + 1;
                }
                if([rating.User.objectId isEqualToString: backendless.userService.currentUser.objectId])
                {
                    if ([rating.type isEqualToString:@"thumbsUp"]){
                        alreadyRatingGridA = rating;
                    }
                    else{
                        alreadyRatingDownGridA = rating;
                    }
                
                
                
                }
            }
            
        }
        [self.thumbsUpA setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)upCount] forState:UIControlStateNormal];
        
        [self.thumbsDownA setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)downCount] forState:UIControlStateNormal];
        self.thumsUpCountA = upCount;
        self.thumsDownCountA = downCount;
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
}
-(void)FetchRatingsForGridB:(NSInteger )index
{
    id obj = allPhotosAndVideosArray[index];
    NSString *itemsObject;
    NSString *itemsObjectId;
    if ([obj isKindOfClass:[Photo class]])
    {
        Photo *photo = obj;
        itemsObjectId = photo.objectId;
        itemsObject = @"photo";
    }
    else
    {
        Video *video = obj;
        itemsObjectId = video.objectId;
        itemsObject = @"video";
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"%@.objectId =\'%@\' ",itemsObject,itemsObjectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Rating class]];
    [dataStore find:query response:^(BackendlessCollection *foundRatings) {
        
        NSInteger upCount = 0;
        NSInteger downCount = 0;
        if (foundRatings.data.count)
        {
            NSArray *tempAllRating = [[NSMutableArray alloc] initWithArray:foundRatings.data];
            ratingsGridB = [[NSMutableArray alloc] initWithArray:tempAllRating];
            
            for (Rating *rating in tempAllRating)
            {
                if ([rating.type isEqualToString:@"thumbsUp"])
                {
                    [self.thumbsUpFound addObject:rating];
                    upCount = upCount + 1;
                }
                else
                {
                    [self.thumbsDownFound addObject:rating];
                    downCount = downCount + 1;
                }
                if([rating.User.objectId isEqualToString: backendless.userService.currentUser.objectId])
                {

                    
                    if ([rating.type isEqualToString:@"thumbsUp"]){
                        alreadyRatingGridB = rating;
                    }
                    else{
                        alreadyRatingDownGridB = rating;
                    }
                
                
                
                }
            }
            
        }
        [self.thumbsUpB setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)upCount] forState:UIControlStateNormal];
        
        [self.thumbsDownB setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)downCount] forState:UIControlStateNormal];
        
        self.thumsUpCountB = upCount;
        self.thumsDownCountB = downCount;
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
}
-(void)FetchRatingsForGridC:(NSInteger )index
{
    id obj = allPhotosAndVideosArray[index];
    NSString *itemsObject;
    NSString *itemsObjectId;
    if ([obj isKindOfClass:[Photo class]])
    {
        Photo *photo = obj;
        itemsObjectId = photo.objectId;
        itemsObject = @"photo";
    }
    else
    {
        Video *video = obj;
        itemsObjectId = video.objectId;
        itemsObject = @"video";
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"%@.objectId =\'%@\' ",itemsObject,itemsObjectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Rating class]];
    [dataStore find:query response:^(BackendlessCollection *foundRatings) {
        NSInteger upCount = 0;
        NSInteger downCount = 0;
        if (foundRatings.data.count)
        {
            NSArray *tempAllRating = [[NSMutableArray alloc] initWithArray:foundRatings.data];
            ratingsGridC = [[NSMutableArray alloc] initWithArray:tempAllRating];
            
            for (Rating *rating in tempAllRating)
            {
                if ([rating.type isEqualToString:@"thumbsUp"])
                {
                    [self.thumbsUpFound addObject:rating];
                    upCount = upCount + 1;
                }
                else
                {
                    [self.thumbsDownFound addObject:rating];
                    downCount = downCount + 1;
                }
                if([rating.User.objectId isEqualToString: backendless.userService.currentUser.objectId])
                {
                    if ([rating.type isEqualToString:@"thumbsUp"]){
                        alreadyRatingGridC = rating;
                    }
                    else{
                        alreadyRatingDownGridC = rating;
                    }
                
                }
            }
            
        }
        [self.thumbsUpC setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)upCount] forState:UIControlStateNormal];
        
        [self.thumbsDownC setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)downCount] forState:UIControlStateNormal];
        
        self.thumsUpCountC = upCount;
        self.thumsDownCountC = downCount;
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
}
-(void)FetchRatingsForGridD:(NSInteger )index
{
    id obj = allPhotosAndVideosArray[index];
    NSString *itemsObject;
    NSString *itemsObjectId;
    if ([obj isKindOfClass:[Photo class]])
    {
        Photo *photo = obj;
        itemsObjectId = photo.objectId;
        itemsObject = @"photo";
    }
    else
    {
        Video *video = obj;
        itemsObjectId = video.objectId;
        itemsObject = @"video";
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"%@.objectId =\'%@\' ",itemsObject,itemsObjectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Rating class]];
    [dataStore find:query response:^(BackendlessCollection *foundRatings) {
        
        NSInteger upCount = 0;
        NSInteger downCount = 0;
        if (foundRatings.data.count)
        {
            NSArray *tempAllRating = [[NSMutableArray alloc] initWithArray:foundRatings.data];
            ratingsGridD = [[NSMutableArray alloc] initWithArray:tempAllRating];
            
            for (Rating *rating in tempAllRating)
            {
                if ([rating.type isEqualToString:@"thumbsUp"])
                {
                    [self.thumbsUpFound addObject:rating];
                    upCount = upCount + 1;
                }
                else
                {
                    [self.thumbsDownFound addObject:rating];
                    downCount = downCount + 1;
                }
                if([rating.User.objectId isEqualToString: backendless.userService.currentUser.objectId])
                {
                    if ([rating.type isEqualToString:@"thumbsUp"]){
                        alreadyRatingGridD = rating;
                    }
                    else{
                        alreadyRatingDownGridD = rating;
                    }
                    
                }
            }
        }
        [self.thumbsUpD setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)upCount] forState:UIControlStateNormal];
        
        [self.thumbsDownD setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)downCount] forState:UIControlStateNormal];
        
        self.thumsUpCountD = upCount;
        self.thumsDownCountD = downCount;
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
}
-(void)fetchAllRatings
{
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"GridId =\'%@\' " ,_grid.objectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Rating class]];
    [dataStore find:query response:^(BackendlessCollection *foundRatings) {
        
        
        if (foundRatings.data.count)
        {
            allRatings = [[NSMutableArray alloc] initWithArray:foundRatings.data];
            self.thumbsUpFound = [[NSMutableArray alloc] init];
            self.thumbsDownFound = [[NSMutableArray alloc] init];

            for (Rating *rating in allRatings)
            {
                if ([rating.type isEqualToString:@"thumbsUp"])
                {
                    [self.thumbsUpFound addObject:rating];
                }
                else
                {
                    [self.thumbsDownFound addObject:rating];

                }
                if([rating.User.objectId isEqualToString: backendless.userService.currentUser.objectId])
                {
                    alreadyRatingForCurrentUser = rating;
                    break;
                }
            }
            
        }
        [self.showThumbsUpBtn setTitle:[NSString stringWithFormat:@"%lu Thumbs Up ",(unsigned long)self.thumbsUpFound.count] forState:UIControlStateNormal];
        thumbsUpCount=self.thumbsUpFound.count;
        [_showThumbsDownBtn setTitle:[NSString stringWithFormat:@"%lu Thumbs Down ",(unsigned long)self.thumbsDownFound.count] forState:UIControlStateNormal];
    } error:^(Fault *error) {
        NSLog(@"Error Found at Ratings  %@",error.detail);
    }];
}

-(void)fetchThumbsUps{
    
    NSString *itemsObject;
    NSString *itemsObjectId;
    if(self.selectedPhoto != nil){
        
        itemsObjectId = self.selectedPhoto.objectId;
        itemsObject = @"photo";
    }
    else {
        
        itemsObjectId = self.selectedVideo.objectId;
        itemsObject = @"video";
    }
    
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"type = 'thumbsUp' AND %@.objectId =\'%@\' " ,itemsObject,itemsObjectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Rating class]];
    [dataStore find:query response:^(BackendlessCollection *thumbsUpColection) {
        
        
        self.thumbsUpFound = [[NSMutableArray alloc] initWithArray:thumbsUpColection.data];
        
        [self.showThumbsUpBtn setTitle:[NSString stringWithFormat:@"%lu Thumbs Up ",(unsigned long)self.thumbsUpFound.count] forState:UIControlStateNormal];
        thumbsUpCount=self.thumbsUpFound.count;
        
        for (Rating *rating in self.thumbsUpFound){
            
            
            if([rating.User.objectId isEqualToString: backendless.userService.currentUser.objectId]){
                
                //self.BtnThumbsUp.enabled=NO;
                //self.btnThumbsDown.enabled=NO;
                alreadyRatingForCurrentUser = rating;
                break;
            }
        }
        
        
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
    
    
}

-(void)fetchThumbsdown{
    
    NSString *itemsObject;
    NSString *itemsObjectId;
    if(self.selectedPhoto != nil){
        itemsObjectId = self.selectedPhoto.objectId;
        itemsObject = @"photo";
    }
    else {
        itemsObjectId = self.selectedVideo.objectId;
        itemsObject = @"video";
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"type = 'thumbsDown' AND %@.objectId =\'%@\' ",itemsObject,itemsObjectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Rating class]];
    [dataStore find:query response:^(BackendlessCollection *thumbsdownColection) {
        
        
        self.thumbsDownFound = [[NSMutableArray alloc] initWithArray:thumbsdownColection.data];
        
        [_showThumbsDownBtn setTitle:[NSString stringWithFormat:@"%lu Thumbs Down ",(unsigned long)self.thumbsDownFound.count] forState:UIControlStateNormal];
        
        for (Rating *rating in self.thumbsDownFound){
            
            
            if([rating.User.objectId isEqualToString :backendless.userService.currentUser.objectId  ]){
                
                //self.btnThumbsDown.enabled=NO;
                //self.BtnThumbsUp.enabled=NO;
                
                alreadyRatingForCurrentUser = rating;
                break;
                
            }
        }
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
    
    
}
-(void)fetchComments{
    
    NSString *itemsObject;
    NSString *itemsObjectId;
    id obj = allPhotosAndVideosArray[0];
    Photo *selectedPhoto1;
    Video *selectedVideo1;
    if ([obj isKindOfClass:[Photo class]])
    {
        selectedPhoto1 = obj;
        itemsObjectId = selectedPhoto1.objectId;
        itemsObject = @"photo";
    }
    else
    {
        selectedVideo1 = obj;
        itemsObjectId = selectedVideo1.objectId;
        itemsObject = @"video";
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"%@.objectId = '%@' ",itemsObject,itemsObjectId];

    id<IDataStore> dataStore = [backendless.persistenceService of:[Comments class]];
    [dataStore find:query response:^(BackendlessCollection *Comments) {
        
        self.CommentsFound = [[NSMutableArray alloc] initWithArray:Comments.data];
        [SVProgressHUD dismiss];
        
        
        [self.commentsTableView reloadData];
        
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setGrids
{
    
    for (Photo *photo in _grid.Photos){
        
        [allPhotosAndVideosArray addObject:photo];
        NSLog(@"%@",photo.objectId);
        
    }
    for (Video *video in _grid.Videos){
        
        [allPhotosAndVideosArray addObject:video];
        NSLog(@"%@",video.objectId);
        
    }
    
    NSLog(@"%@",allPhotosAndVideosArray);
    switch (_grid.style)
    {
        case 0:
            
            [self setImagesAndVideosInGrid:self.views_a];
            [self AddWhiteviewForAllCurrentGrid:self.views_a];
            break;
            
        case 1:
            
            [self setImagesAndVideosInGrid:self.view_b];
            [self AddWhiteviewForAllCurrentGrid:self.view_b];
            break;
        case 2:
            
            [self setImagesAndVideosInGrid:self.views_c];
            [self AddWhiteviewForAllCurrentGrid:self.views_c];
            break;
            
        case 3:
            
            [self setImagesAndVideosInGrid:self.views_d];
            [self AddWhiteviewForAllCurrentGrid:self.views_d];
            
            break;
        case 4:
            
            [self setImagesAndVideosInGrid:self.view_e];
            [self AddWhiteviewForAllCurrentGrid:self.view_e];
            break;
            
        case 5:
            
            [self setImagesAndVideosInGrid:self.views_f];
            [self AddWhiteviewForAllCurrentGrid:self.views_f];
            
            break;
            
        default:
            
            NSLog(@"default");
            
            break;
            
    }
 
}
-(void)AddWhiteviewForAllCurrentGrid:(NSArray *)views
{
    for (int i = 0 ; i<views.count;i++)
    {
        UIView *view = views[i];
        UILabel *label = (UILabel *)[[view subviews] objectAtIndex:0];
        UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(-14, -14, 40, 40)];
        [whiteView setBackgroundColor:[UIColor whiteColor]];
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:whiteView.bounds byRoundingCorners:(UIRectCornerBottomRight) cornerRadii:CGSizeMake(25.0, 25.0)];
        
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = self.view.bounds;
        maskLayer.path  = maskPath.CGPath;
        whiteView.layer.mask = maskLayer;
        [view addSubview: whiteView];
        [view bringSubviewToFront: label];
        [view setClipsToBounds:YES];
    }
    
}

-(void)setImagesAndVideosInGrid:(NSArray *)Views
{
    
    NSInteger totalCount = 0;
    for (int i = 0 ; i<allPhotosAndVideosArray.count;i++)
    {
        if (i >= Views.count)
        {
            break;
        }
        totalCount = i + 1;
        
        UIView *view = Views[i];
        
        UILabel *label = (UILabel *)[[view subviews] objectAtIndex:0];
        
        id obj = allPhotosAndVideosArray[i];
        
        
        if ([obj isKindOfClass:[Photo class]]){
            Photo *photo = obj;
            
            //                float topOffsetY = 14;
            float topOffsetY = 0;
            
            CGRect imageViewFrame = view.bounds;
            imageViewFrame.origin.y += topOffsetY;
            imageViewFrame.size.height -=topOffsetY;
            
            view.backgroundColor = [UIColor clearColor];
            
            
            
            if (photo.webImageLink.length > 1) {
                NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"LinkPreview" owner:self options:nil];
                
                for (UIView *view in views) {
                    if ([view isKindOfClass:[LinkPreview class]]) {
                        NSLog(@"CUSTOM VIEW DETECTED");
                    }
                }
                
                LinkPreview *customView  = (LinkPreview *)[[[NSBundle mainBundle] loadNibNamed:@"LinkPreview" owner:self options:nil] objectAtIndex:0];
                
                
                customView.frame = imageViewFrame;
                
                customView.linkTitle.text = photo.linkTitle;
                customView.linkdescription.text = photo.linkDescription;
                customView.linkLink.text = photo.webImageLink;
                
                NSString *imageStr = photo.photo;
                
                
                UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyleGray)];
                [activity startAnimating];
                activity.center = customView.linkImageView.center;
                [customView.linkImageView addSubview:activity];
                customView.backgroundColor = [UIColor grayColor];
                [customView.linkImageView sd_setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@"placeHolder"] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
                 {
                     [activity stopAnimating];
                     [activity removeFromSuperview];
                 }];
                
                [view addSubview:customView];
            }
            else{
                UIImageView *imageView = [[UIImageView alloc]initWithFrame:imageViewFrame];
                view.clipsToBounds=YES;
                view.contentMode = UIViewContentModeScaleAspectFill;
                imageView.layer.masksToBounds=YES;
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                
                UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyleGray)];
                [activity startAnimating];
                activity.center = imageView.center;
                [imageView addSubview:activity];
                
                [imageView sd_setImageWithURL:[NSURL URLWithString:photo.photo] placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"] options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
                 {
                     [activity stopAnimating];
                     [activity removeFromSuperview];
                 }];
                
                [view addSubview:imageView];
                [ imageView setAutoresizingMask:( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight )];

            }
            
            
            
            
            //[view sendSubviewToBack:imageView];
            
            UIButton *openSinglePhotoBtn = [[UIButton alloc]initWithFrame:view.bounds];
            openSinglePhotoBtn.backgroundColor=[UIColor clearColor];
            [openSinglePhotoBtn addTarget:self action:@selector(openSinglePhotoBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            openSinglePhotoBtn.tag = i;
            
            [ openSinglePhotoBtn setAutoresizingMask:( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight )];
            [ view setAutoresizesSubviews:YES ];
            
            [view addSubview:openSinglePhotoBtn];
        }
        else
        {
            Video *video = obj;
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",video.VideoURL]];
            
            [self createAntPlayVideo:view andUrl:url];
//            float topOffsetY = 0;
//
//            CGRect imageViewFrame = view.bounds;
//            imageViewFrame.origin.y += topOffsetY;
//            imageViewFrame.size.height -=topOffsetY;
//            UIImageView *imageView = [[UIImageView alloc]initWithFrame:imageViewFrame];
//            view.clipsToBounds=YES;
//            view.contentMode = UIViewContentModeScaleAspectFill;
//            imageView.layer.masksToBounds=YES;
//            imageView.contentMode = UIViewContentModeScaleAspectFill;
//
//            UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:(UIActivityIndicatorViewStyleGray)];
//            [activity startAnimating];
//            activity.center = imageView.center;
//            [imageView addSubview:activity];
//            [view addSubview:imageView];
//            [ imageView setAutoresizingMask:( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight )];
            
            UIButton *openVideoBtn = [[UIButton alloc]initWithFrame:view.bounds];
            openVideoBtn.backgroundColor=[UIColor clearColor];
            [openVideoBtn addTarget:self action:@selector(openVideoBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
            [ openVideoBtn setAutoresizingMask:( UIViewAutoresizingFlexibleWidth |
                                                UIViewAutoresizingFlexibleHeight )];
            openVideoBtn.tag = i;
            [view addSubview:openVideoBtn];
        }
        
//        UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(-14, -14, 40, 40)];
//        [whiteView setBackgroundColor:[UIColor whiteColor]];
//        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:whiteView.bounds byRoundingCorners:(UIRectCornerBottomRight) cornerRadii:CGSizeMake(25.0, 25.0)];
//
//        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
//        maskLayer.frame = self.view.bounds;
//        maskLayer.path  = maskPath.CGPath;
//        whiteView.layer.mask = maskLayer;
//        [view addSubview: whiteView];
//        [view bringSubviewToFront: label];
    }
    
    if(totalCount==1)
    {
        self.likeViewA.hidden = NO;
        self.likeViewB.hidden = YES;
        self.likeViewC.hidden = YES;
        self.likeViewD.hidden = YES;
        self.likeViewHeight.constant = 28;
    }
    if(totalCount==2)
    {
        self.likeViewA.hidden = NO;
        self.likeViewB.hidden = NO;
        self.likeViewC.hidden = YES;
        self.likeViewD.hidden = YES;
        self.likeViewHeight.constant = 28;
    }
    if(totalCount==3)
    {
        self.likeViewA.hidden = NO;
        self.likeViewB.hidden = NO;
        self.likeViewC.hidden = NO;
        self.likeViewD.hidden = YES;
        self.likeViewHeight.constant = 56;
    }
    if(totalCount==4)
    {
        self.likeViewA.hidden = NO;
        self.likeViewB.hidden = NO;
        self.likeViewC.hidden = NO;
        self.likeViewD.hidden = NO;
        self.likeViewHeight.constant = 56;
    }
}
    
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([object isKindOfClass:[AVPlayerItem class]])
    {
        AVPlayerItem *player = (AVPlayerItem *)object;
        if ([keyPath isEqualToString:@"status"])
        {
            if (player.status == AVPlayerStatusReadyToPlay)
            {
                NSLog(@"AVPlayerStatusReadyToPlay");
                
                [player removeObserver:self forKeyPath:@"status"];
            }
            else if (player.status == AVPlayerStatusFailed)
            {
                // something went wrong. player.error should contain some information
            }
        }
    }
    
    
}
-(void)createAntPlayVideo:(UIView *)view andUrl:(NSURL *)url
{
    
//    return;
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.delegate = self;
    _playerViewController.view.frame = view.bounds;
    _playerViewController.showsPlaybackControls = NO;
    
    // First create an AVPlayerItem
    
    AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:url];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
    _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;

    UIImage *normImage = [UIImage imageNamed:@""]; //play-button-2.png
    playBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    [playBTN addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
    [playBTN setImage:normImage forState:UIControlStateNormal];
    playBTN.frame = CGRectMake(0, 0, normImage.size.width, normImage.size.height);
    playBTN.center = CGPointMake(_playerViewController.view.frame.size.width/2, _playerViewController.view.frame.size.height/2);
    playBTN.tag = view.tag;
    [view addSubview:_playerViewController.view];
    //target.layer.contents = ;
    [_playerViewController.view addSubview:playBTN];
    
    [ _playerViewController.view setAutoresizingMask:( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight )];
    [view setAutoresizesSubviews:YES ];
    
    playBTN.hidden = YES ;
    
    CustomImageView *cImageView = [[CustomImageView alloc] initWithFrame:view.bounds];
    cImageView.backgroundColor = [UIColor clearColor];
//    cImageView.center = view.center;
    [view addSubview:cImageView];

    [cImageView displayIndicator];
    [_playerViewController.player.currentItem addObserver:cImageView forKeyPath:@"status" options:0 context:nil];
    
    
}
- (void)playVideo:(UIButton *)sender
{
    
    playBTN.hidden = YES;
    [[_playerViewController player] play];
    /*
     timeSpent = 0.0;
     previewTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
     target:self
     selector:@selector(checkPlayerTimer:)
     userInfo:nil
     repeats:YES];
     */
}
-(void)itemDidFinishPlaying:(NSNotification *) notification {
    [playBTN setHidden:NO];
    AVPlayerItem *p = [notification object];
    
    
    [p seekToTime:kCMTimeZero];
}
- (IBAction)CancelFromImageview:(UIButton *)sender
{
    [UIView transitionWithView:self.imageScrollView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
        [self.imageScrollView setHidden:YES];
    } completion:nil];
}
- (IBAction)openSinglePhotoBtnTapped:(UIButton *)sender
{
//    [self.imageScrollView setHidden:NO];
    self.clickedPhoto = allPhotosAndVideosArray[sender.tag];
    
    [self.singleImageView sd_setImageWithURL:[NSURL URLWithString:self.clickedPhoto.photo] placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"] options:SDWebImageRefreshCached];
    [self.singleImageView setContentMode:UIViewContentModeScaleAspectFit];
    [UIView transitionWithView:self.imageScrollView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^(void){
        [self.imageScrollView setHidden:NO];
//        [self setupScales];
    } completion:nil];
    
    if(!self.clickedPhoto.webImageLink)
    {
        [self.openInWebButton setHidden:YES];
        return;
    }
    if([self.clickedPhoto.webImageLink isEqualToString:@""])
    {
        [self.openInWebButton setHidden:YES];
    }
    else
    {
        [self.openInWebButton setHidden:NO];
    }
//    Photo *photo1 = allPhotosAndVideosArr/Users/mohitgorakhiya/Downloads/close.pngay[sender.tag];
//
//    INSPhoto *insphoto = [[INSPhoto alloc] initWithImageURL:[NSURL URLWithString:photo1.photo] thumbnailImage:nil];
//    NSArray *photos = [NSArray arrayWithObject:insphoto];
//
//    INSPhotosViewController *galaryView = [[INSPhotosViewController alloc] initWithPhotos:photos initialPhoto:nil referenceView:sender];
//
//    [galaryView setReferenceViewForPhotoWhenDismissingHandler:^UIView * _Nullable(id<INSPhotoViewable> photo) {
//
//        return sender;
//
//    }];
//    [self presentViewController:galaryView animated:YES completion:nil];
//    self.selectedVideo=nil;
//    _selectedPhoto= allPhotosAndVideosArray[sender.tag];
//    [self performSegueWithIdentifier:@"GridToSinglePhoto" sender:self];
}
- (IBAction)openVideoBtnTapped:(UIButton *)sender
{
//    self.selectedPhoto=nil;
    self.selectedVideo= allPhotosAndVideosArray[sender.tag];
    
    NSString *fullpath = self.selectedVideo.VideoURL;
    NSURL *vedioURL =[NSURL URLWithString:fullpath];
    AVPlayer *player = [AVPlayer playerWithURL:vedioURL];
    self.myplayerViewController = [AVPlayerViewController new];
    self.myplayerViewController.player = player;
//    [self.view addSubview:self.myplayerViewController.view];
    [self presentViewController:self.myplayerViewController animated:YES completion:nil];
    [player pause];

    [player play];
//    NSLog(@"%@", _selectedVideo.objectId);
    
//    [self performSegueWithIdentifier:@"GridToSinglePhoto" sender:self];
}
-(IBAction)backAction:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(tellGridListSomething:)])
    {
        [self performSegueWithIdentifier:@"sharedToListBack" sender:self];
        
    }else{
        
        [self performSegueWithIdentifier:@"sharedToListBack" sender:self];
    }
    
}
-(IBAction)unwindToSharedGrid:(UIStoryboardSegue*)sender
{
    
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"GridToSinglePhoto"]) {
        
        SingleContentViewController *vc = segue.destinationViewController ;
        vc.selectedPhoto=self.selectedPhoto;
        vc.selectedVideo = self.selectedVideo;
        vc.grid=self.grid;
        
    }
}


- (IBAction)shareSocialMediaAction:(UIButton *)sender {
    
    sharePopUp *sharePop = [[sharePopUp alloc] initWithNibName:@"sharePopUp" bundle:nil];
    
    [self presentPopupViewController:sharePop animated:YES completion:^(void) {
        
        [sharePop.closeBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        [sharePop.facebookBtn addTarget:self action:@selector(facebookShare) forControlEvents:UIControlEventTouchUpInside];
        [sharePop.twitterBtn addTarget:self action:@selector(twitterShare) forControlEvents:UIControlEventTouchUpInside];
        [sharePop.instagramBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        
}];
    
}
-(void)facebookShare{
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        
        SLComposeViewController *fbController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        // set up a completion handler (optional)
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
            
            [fbController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                    break;
                case SLComposeViewControllerResultDone:
                    break;
            }};
        
        //change here
        for (int i=0 ;i<allPhotosAndVideosArray.count ;i++){
            
            
            
            NSURL *imageURL = [NSURL URLWithString:allPhotosAndVideosArray[i]];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            UIImage *image = [UIImage imageWithData:imageData];
            [fbController addImage:image];
            
        }
        
        
        
        [fbController setInitialText:@""];
        [fbController addURL:[NSURL URLWithString:@""]];
        [fbController setCompletionHandler:completionHandler];
        [self presentViewController:fbController animated:YES completion:nil];
    }
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"you are not logged in your facebook account "] andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
        NSLog(@"you are not logged in ");
    }
}
-(void)twitterShare{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        
        SLComposeViewController *twitterController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        // set up a completion handler (optional)
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
            
            [twitterController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                    break;
                case SLComposeViewControllerResultDone:
                    break;
            }};
        
        //change here
        for (int i=0 ;i<allPhotosAndVideosArray.count ;i++){
            
            
            
            NSURL *imageURL = [NSURL URLWithString:allPhotosAndVideosArray[i]];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            UIImage *image = [UIImage imageWithData:imageData];
            [twitterController addImage:image];
            
        }
        
        
        [twitterController setInitialText:@""];
        [twitterController addURL:[NSURL URLWithString:@""]];
        [twitterController setCompletionHandler:completionHandler];
        [self presentViewController:twitterController animated:YES completion:nil];
    }
    
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"you are not logged in your twitter account"] andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
        
        NSLog(@"you are not logged in ");
    }
    
}

-(void)dismissPopup{
    
    [self dismissPopupViewControllerAnimated:YES completion:nil];
}
-(IBAction)clickOnWebLink:(id)sender
{
    NSURL *url;
    if ([self.clickedPhoto.webImageLink.lowercaseString hasPrefix:@"http://"] || [self.clickedPhoto.webImageLink.lowercaseString hasPrefix:@"https://"]) {
        url = [NSURL URLWithString:self.clickedPhoto.webImageLink];
    } else {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",self.clickedPhoto.webImageLink]];
    }
    
    SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:url];
    svc.delegate = self;
    [self presentViewController:svc animated:YES completion:nil];
}
- (void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
//    [self dismissViewControllerAnimated:true completion:nil];
}
#pragma mark - Actions

- (IBAction)openTemplateComments:(UIButton *)sender {
    
    self.templateCommetnsTableView.tag = 1 ;
    if (self.templateView.isHidden)
    {
        [self.templateView setHidden:NO];
        
        sender.tag = 1 ;
        _openTemplates.tag = 0;
        
        [self.templateCommetnsTableView reloadData];
        
    }
    else
    {
        [self.templateView setHidden:YES];
        
        sender.tag = 0 ;
    }
    
}
- (IBAction)thumbsUpActionForGrid:(UIButton *)sender{
    
    [SVProgressHUD showWithStatus:@"Saving..."];
    
    id obj = allPhotosAndVideosArray[sender.tag];
    Photo *selectedPhoto1;
    Video *selectedVideo1;
    if ([obj isKindOfClass:[Photo class]]){
        selectedPhoto1 = obj;
    }
    else{
        selectedVideo1 = obj;
    }
    [self SetEnableLikeView:NO];
    
    Rating *gridRating;
    
    if(sender.tag == 0)
    {
        if(alreadyRatingGridA)
        {
            gridRating = alreadyRatingGridA;
            self.thumsUpCountA = self.thumsUpCountA - 1;
            [self.thumbsUpA setTitle:[NSString stringWithFormat:@"%ld", self.thumsUpCountA] forState:UIControlStateNormal];
            
            [self.thumbsUpA setImage:[UIImage imageNamed:@"thumbsUpIcon"] forState:UIControlStateNormal];
            
            id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
            [dataStore1 remove:gridRating response:^(NSNumber *num) {
                NSLog(@"good HERE = %@" , num);
                alreadyRatingGridA = nil;
                [SVProgressHUD dismiss];
            } error:^(Fault *error) {
                NSLog(@"error HERE = %@" , error.description);
                self.thumsUpCountA = self.thumsUpCountA + 1;
                [self.thumbsUpA setTitle:[NSString stringWithFormat:@"%ld", self.thumsUpCountA] forState:UIControlStateNormal];
                
                [self.thumbsUpA setImage:[UIImage imageNamed:@"thumbsUpIconFill"] forState:UIControlStateNormal];
                
                [SVProgressHUD dismiss];
            }];

        }
    }
    if(sender.tag == 1)
    {
        if(alreadyRatingGridB)
        {
            gridRating = alreadyRatingGridB;
            self.thumsUpCountB = self.thumsUpCountB - 1;
            [self.thumbsUpB setTitle:[NSString stringWithFormat:@"%ld", self.thumsUpCountB] forState:UIControlStateNormal];
            
            [self.thumbsUpB setImage:[UIImage imageNamed:@"thumbsUpIcon"] forState:UIControlStateNormal];
            
            
            id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
            [dataStore1 remove:gridRating response:^(NSNumber *num) {
                NSLog(@"good HERE = %@" , num);
                alreadyRatingGridB = nil;
                [SVProgressHUD dismiss];

            } error:^(Fault *error) {
                NSLog(@"error HERE = %@" , error.description);
                [SVProgressHUD dismiss];
                self.thumsUpCountB = self.thumsUpCountB + 1;
                [self.thumbsUpB setImage:[UIImage imageNamed:@"thumbsUpIconFill"] forState:UIControlStateNormal];
                [self.thumbsUpB setTitle:[NSString stringWithFormat:@"%ld", self.thumsUpCountB] forState:UIControlStateNormal];
            }];
            
        }
    }
    if(sender.tag == 2)
    {
        if(alreadyRatingGridC)
        {
            gridRating = alreadyRatingGridC;
            self.thumsUpCountC = self.thumsUpCountC - 1;
            [self.thumbsUpC setTitle:[NSString stringWithFormat:@"%ld", self.thumsUpCountC] forState:UIControlStateNormal];
            
            [self.thumbsUpC setImage:[UIImage imageNamed:@"thumbsUpIcon"] forState:UIControlStateNormal];
            
            
            id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
            [dataStore1 remove:gridRating response:^(NSNumber *num) {
                NSLog(@"good HERE = %@" , num);
                alreadyRatingGridC = nil;
                [SVProgressHUD dismiss];
            } error:^(Fault *error) {
                NSLog(@"error HERE = %@" , error.description);
                [SVProgressHUD dismiss];
                self.thumsUpCountC = self.thumsUpCountC + 1;
                [self.thumbsUpC setImage:[UIImage imageNamed:@"thumbsUpIconFill"] forState:UIControlStateNormal];
                [self.thumbsUpC setTitle:[NSString stringWithFormat:@"%ld", self.thumsUpCountC] forState:UIControlStateNormal];
            }];
        }
    }
    if(sender.tag == 3)
    {
        if(alreadyRatingGridD)
        {
            gridRating = alreadyRatingGridD;
            self.thumsUpCountD = self.thumsUpCountD - 1;
            [self.thumbsUpD setTitle:[NSString stringWithFormat:@"%ld", self.thumsUpCountD] forState:UIControlStateNormal];
            
            [self.thumbsUpD setImage:[UIImage imageNamed:@"thumbsUpIcon"] forState:UIControlStateNormal];
            
            
            id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
            [dataStore1 remove:gridRating response:^(NSNumber *num) {
                NSLog(@"good HERE = %@" , num);
                alreadyRatingGridD = nil;
                [SVProgressHUD dismiss];
            } error:^(Fault *error) {
                NSLog(@"error HERE = %@" , error.description);
                [SVProgressHUD dismiss];
                self.thumsUpCountD = self.thumsUpCountD + 1;
                [self.thumbsUpD setImage:[UIImage imageNamed:@"thumbsUpIconFill"] forState:UIControlStateNormal];
                [self.thumbsUpD setTitle:[NSString stringWithFormat:@"%ld", self.thumsUpCountD] forState:UIControlStateNormal];
            }];
        }
    }
    NSString *likeCount = @"";
    
    if (!gridRating)
    {
        if(sender.tag == 0){
            if (alreadyRatingDownGridA) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"You cannot Like if you have already Disliked. "] andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
                [SVProgressHUD dismiss];
                [self SetEnableLikeView:YES];
                return;
            }
            
        }
        else if(sender.tag == 1){

            if (alreadyRatingDownGridB) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"You cannot Like if you have already Disliked. "] andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
                [SVProgressHUD dismiss];
                [self SetEnableLikeView:YES];
                return;
            }
            
        }
        else if(sender.tag == 2){
            if (alreadyRatingDownGridC) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"You cannot Like if you have already Disliked. "] andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
                [SVProgressHUD dismiss];
                [self SetEnableLikeView:YES];
                return;
            }
        }
        else if(sender.tag == 3){
            if (alreadyRatingDownGridD) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"You cannot Like if you have already Disliked. "] andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
                [SVProgressHUD dismiss];
                [self SetEnableLikeView:YES];
                return;
            }
        }
        
        gridRating = [Rating new];
        likeCount = @"1";
    }
    else
    {
        if ([gridRating.type isEqualToString:@"thumbsUp"])
        {
//            [self.BtnThumbsUp setEnabled:YES];
//            [self.btnThumbsDown setEnabled:YES];
            [self SetEnableLikeView:YES];
            likeCount = @"0";
            
            return;
        }
    }
    NSInteger thumsUp = 0;
    NSInteger thumsDown = 0;
    if(sender.tag == 0)
    {
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsUpCountA = self.thumsUpCountA - 1;
        }
        else
        {
            self.thumsUpCountA = self.thumsUpCountA + 1;
        }
        if(self.thumsDownCountA > 0)
        {
            //self.thumsDownCountA = self.thumsDownCountA - 1;
        }
        else
        {
            self.thumsDownCountA = 0;
        }
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsDownCountA = 0;
        }
        
        thumsUp = self.thumsUpCountA;
        thumsDown = self.thumsDownCountA;
        
        [self.thumbsUpA setTitle:[NSString stringWithFormat:@"%ld",(long)thumsUp] forState:UIControlStateNormal];
        [self.thumbsDownA setTitle:[NSString stringWithFormat:@"%ld",(long)thumsDown] forState:UIControlStateNormal];
        
        [self.thumbsUpA setImage:[UIImage imageNamed:@"thumbsUpIconFill"] forState:UIControlStateNormal];
    }
    else if(sender.tag == 1)
    {
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsUpCountB = self.thumsUpCountB - 1;
        }
        else
        {
            self.thumsUpCountB = self.thumsUpCountB + 1;
        }
        
        if(self.thumsDownCountB > 0)
        {
            //self.thumsDownCountB = self.thumsDownCountB - 1;
        }
        else
        {
            self.thumsDownCountB = 0;
        }
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsDownCountB = 0;
        }
        thumsUp = self.thumsUpCountB;
        thumsDown = self.thumsDownCountB;
        
        [self.thumbsUpB setTitle:[NSString stringWithFormat:@"%ld",(long)thumsUp] forState:UIControlStateNormal];
        [self.thumbsDownB setTitle:[NSString stringWithFormat:@"%ld",(long)thumsDown] forState:UIControlStateNormal];
        
        [self.thumbsUpB setImage:[UIImage imageNamed:@"thumbsUpIconFill"] forState:UIControlStateNormal];
    }
    else if(sender.tag == 2)
    {
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsUpCountC = self.thumsUpCountC - 1;
        }
        else
        {
            self.thumsUpCountC = self.thumsUpCountC + 1;
        }
        
        if(self.thumsDownCountC > 0)
        {
            //self.thumsDownCountC = self.thumsDownCountC - 1;
        }
        else
        {
            self.thumsDownCountC = 0;
        }
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsDownCountC = 0;
        }
        thumsUp = self.thumsUpCountC;
        thumsDown = self.thumsDownCountC;
        
        [self.thumbsUpC setTitle:[NSString stringWithFormat:@"%ld",(long)thumsUp] forState:UIControlStateNormal];
        [self.thumbsDownC setTitle:[NSString stringWithFormat:@"%ld",(long)thumsDown] forState:UIControlStateNormal];
        [self.thumbsUpC setImage:[UIImage imageNamed:@"thumbsUpIconFill"] forState:UIControlStateNormal];
    }
    else if(sender.tag == 3)
    {
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsUpCountD = self.thumsUpCountD - 1;
        }
        else
        {
            self.thumsUpCountD = self.thumsUpCountD + 1;
        }
        
       
        if(self.thumsDownCountD > 0)
        {
            //self.thumsDownCountD = self.thumsDownCountD - 1;
        }
        else
        {
            self.thumsDownCountD = 0;
        }
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsDownCountD = 0;
        }
        thumsUp = self.thumsUpCountD;
        thumsDown = self.thumsDownCountD;
        
        [self.thumbsUpD setTitle:[NSString stringWithFormat:@"%ld",(long)thumsUp] forState:UIControlStateNormal];
        [self.thumbsDownD setTitle:[NSString stringWithFormat:@"%ld",(long)thumsDown] forState:UIControlStateNormal];
        
        [self.thumbsUpD setImage:[UIImage imageNamed:@"thumbsUpIconFill"] forState:UIControlStateNormal];
    }
        
    gridRating.User = backendless.userService.currentUser;
    gridRating.photo=selectedPhoto1;
    gridRating.video=selectedVideo1;
    gridRating.type=@"thumbsUp";
//    gridRating.count = likeCount;
    //    alreadyRatingForCurrentUser.GridId = itemsObjectId;
    if(sender.tag == 0)
    {
        alreadyRatingGridA = gridRating;
    }
    else if(sender.tag == 1)
    {
        alreadyRatingGridB = gridRating;
    }
    else if(sender.tag == 2)
    {
        alreadyRatingGridC = gridRating;
    }
    else if(sender.tag == 3)
    {
       alreadyRatingGridD = gridRating;
    }
    
    id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
    [dataStore1 save:gridRating response:^(id cm) {
        
        [SVProgressHUD dismiss];

        if(sender.tag == 0){
            alreadyRatingGridA = (Rating *)cm;
            
        }
        else if(sender.tag == 1){
            alreadyRatingGridB = (Rating *)cm;
        }
        else if(sender.tag == 2){
            alreadyRatingGridC = (Rating *)cm;
        }
        else if(sender.tag == 3){
            alreadyRatingGridD = (Rating *)cm;
        }
        
//        [self.BtnThumbsUp setEnabled:YES];
//        [self.btnThumbsDown setEnabled:YES];
        [self SetEnableLikeView:YES];
        
    } error:^(Fault *error) {
        NSLog(@"something went wrong... %@",error.detail);
//        [self.BtnThumbsUp setEnabled:YES];
//        [self.btnThumbsDown setEnabled:YES];
        [SVProgressHUD dismiss];

        if(sender.tag == 0){
            alreadyRatingGridA = nil;
            self.thumsUpCountA = self.thumsUpCountA - 1;
            [self.thumbsUpA setTitle:[NSString stringWithFormat:@"%ld", self.thumsUpCountA] forState:UIControlStateNormal];
            
            [self.thumbsUpA setImage:[UIImage imageNamed:@"thumbsUpIcon"] forState:UIControlStateNormal];
        }
        else if(sender.tag == 1){
            alreadyRatingGridB = nil;
            self.thumsUpCountB = self.thumsUpCountB - 1;
            [self.thumbsUpB setTitle:[NSString stringWithFormat:@"%ld", self.thumsUpCountB] forState:UIControlStateNormal];
            [self.thumbsUpB setImage:[UIImage imageNamed:@"thumbsUpIcon"] forState:UIControlStateNormal];
        }
        else if(sender.tag == 2){
            alreadyRatingGridC = nil;
            self.thumsUpCountC = self.thumsUpCountC - 1;
            [self.thumbsUpC setTitle:[NSString stringWithFormat:@"%ld", self.thumsUpCountC] forState:UIControlStateNormal];
            [self.thumbsUpC setImage:[UIImage imageNamed:@"thumbsUpIcon"] forState:UIControlStateNormal];
        }
        else if(sender.tag == 3){
            alreadyRatingGridD = nil;
            self.thumsUpCountD = self.thumsUpCountD - 1;
            [self.thumbsUpD setTitle:[NSString stringWithFormat:@"%ld", self.thumsUpCountD] forState:UIControlStateNormal];
            [self.thumbsUpD setImage:[UIImage imageNamed:@"thumbsUpIcon"] forState:UIControlStateNormal];
        }
        

        [self SetEnableLikeView:YES];
    }];
    
}
-(void)SetEnableLikeView:(BOOL)isEnable
{
    [self.likeViewA setUserInteractionEnabled:isEnable];
    [self.likeViewB setUserInteractionEnabled:isEnable];
    [self.likeViewC setUserInteractionEnabled:isEnable];
    [self.likeViewD setUserInteractionEnabled:isEnable];
    
}
- (IBAction)thumbsDownActionForGrid:(UIButton *)sender{
    
    [SVProgressHUD showWithStatus:@"Saving..."];

    id obj = allPhotosAndVideosArray[sender.tag];
    
    Photo *selectedPhoto1;
    Video *selectedVideo1;
    if ([obj isKindOfClass:[Photo class]])
    {
        selectedPhoto1 = obj;
        
    }
    else
    {
        selectedVideo1 = obj;
    }
    
    [self SetEnableLikeView:NO];
    
    Rating *gridRating;
    
    if(sender.tag == 0)
    {
        if(alreadyRatingDownGridA)
        {
            gridRating = alreadyRatingDownGridA;
            
            self.thumsDownCountA = self.thumsDownCountA - 1;
            [self.thumbsDownA setTitle:[NSString stringWithFormat:@"%ld", self.thumsDownCountA] forState:UIControlStateNormal];
            
            [self.thumbsDownA setImage:[UIImage imageNamed:@"thumbsDownIcon"] forState:UIControlStateNormal];
            
            
            id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
            [dataStore1 remove:gridRating response:^(NSNumber *num) {
                NSLog(@"good HERE = %@" , num);
                alreadyRatingDownGridA = nil;
                
                [SVProgressHUD dismiss];
                
            } error:^(Fault *error) {
                NSLog(@"error HERE = %@" , error.description);
                [SVProgressHUD dismiss];
                self.thumsDownCountA = self.thumsDownCountA + 1;
                [self.thumbsDownA setTitle:[NSString stringWithFormat:@"%ld", self.thumsDownCountA] forState:UIControlStateNormal];
                
                [self.thumbsDownA setImage:[UIImage imageNamed:@"thumbsDownIconFill"] forState:UIControlStateNormal];
            }];
            
            
            
        }
    }
    if(sender.tag == 1)
    {
        if(alreadyRatingDownGridB)
        {
            gridRating = alreadyRatingDownGridB;
            
            self.thumsDownCountB = self.thumsDownCountB - 1;
            [self.thumbsDownB setTitle:[NSString stringWithFormat:@"%ld", self.thumsDownCountB] forState:UIControlStateNormal];
            [self.thumbsDownB setImage:[UIImage imageNamed:@"thumbsDownIcon"] forState:UIControlStateNormal];
            
            id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
            [dataStore1 remove:gridRating response:^(NSNumber *num) {
                NSLog(@"good HERE = %@" , num);
                alreadyRatingDownGridB = nil;
                [SVProgressHUD dismiss];

            } error:^(Fault *error) {
                NSLog(@"error HERE = %@" , error.description);
                [SVProgressHUD dismiss];
                self.thumsDownCountB = self.thumsDownCountB + 1;
                [self.thumbsDownB setTitle:[NSString stringWithFormat:@"%ld", self.thumsDownCountB] forState:UIControlStateNormal];
                
                [self.thumbsDownB setImage:[UIImage imageNamed:@"thumbsDownIconFill"] forState:UIControlStateNormal];

            }];
        }
    }
    if(sender.tag == 2)
    {
        if(alreadyRatingDownGridC)
        {
            gridRating = alreadyRatingDownGridC;
            
            
            
            self.thumsDownCountC = self.thumsDownCountC - 1;
            [self.thumbsDownC setTitle:[NSString stringWithFormat:@"%ld", self.thumsDownCountC] forState:UIControlStateNormal];
            
            [self.thumbsDownC setImage:[UIImage imageNamed:@"thumbsDownIcon"] forState:UIControlStateNormal];
            
            id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
            [dataStore1 remove:gridRating response:^(NSNumber *num) {
                NSLog(@"good HERE = %@" , num);
                alreadyRatingDownGridC = nil;
                [SVProgressHUD dismiss];

            } error:^(Fault *error) {
                NSLog(@"error HERE = %@" , error.description);
                [SVProgressHUD dismiss];
                self.thumsDownCountC = self.thumsDownCountC + 1;
                [self.thumbsDownC setTitle:[NSString stringWithFormat:@"%ld", self.thumsDownCountC] forState:UIControlStateNormal];
                [self.thumbsDownC setImage:[UIImage imageNamed:@"thumbsDownIconFill"] forState:UIControlStateNormal];

            }];
        }
    }
    if(sender.tag == 3)
    {
        if(alreadyRatingDownGridD)
        {
            gridRating = alreadyRatingDownGridD;
            
            
            self.thumsDownCountD = self.thumsDownCountD - 1;
            [self.thumbsDownD setTitle:[NSString stringWithFormat:@"%ld", self.thumsDownCountD] forState:UIControlStateNormal];
            
            [self.thumbsDownD setImage:[UIImage imageNamed:@"thumbsDownIcon"] forState:UIControlStateNormal];
            
            id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
            [dataStore1 remove:gridRating response:^(NSNumber *num) {
                NSLog(@"good HERE = %@" , num);
                alreadyRatingDownGridD = nil;
                [SVProgressHUD dismiss];

            } error:^(Fault *error) {
                NSLog(@"error HERE = %@" , error.description);
                [SVProgressHUD dismiss];
                self.thumsDownCountD = self.thumsDownCountD + 1;
                [self.thumbsDownD setTitle:[NSString stringWithFormat:@"%ld", self.thumsDownCountD] forState:UIControlStateNormal];
                [self.thumbsDownD setImage:[UIImage imageNamed:@"thumbsDownIconFill"] forState:UIControlStateNormal];

            }];
        }
    }
    NSString *likeCount = @"";
    
 
    if (!gridRating)
    {
        
        
        
        if(sender.tag == 0){
            if (alreadyRatingGridA) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"You cannot Dislike if you have already Liked. "] andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
                [SVProgressHUD dismiss];
                [self SetEnableLikeView:YES];
                return;
            }
        }
        else if(sender.tag == 1){
            if (alreadyRatingGridB) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"You cannot Dislike if you have already Liked. "] andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
                [SVProgressHUD dismiss];
                [self SetEnableLikeView:YES];
                return;
            }
        }
        else if(sender.tag == 2){
            if (alreadyRatingGridC) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"You cannot Dislike if you have already Liked. "] andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
                [SVProgressHUD dismiss];
                [self SetEnableLikeView:YES];
                return;
            }
        }
        else if(sender.tag == 3){
            if (alreadyRatingGridD) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"You cannot Dislike if you have already Liked. "] andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
                [SVProgressHUD dismiss];
                [self SetEnableLikeView:YES];
                return;
            }
        }
        
        
        
        gridRating = [Rating new];
        likeCount = @"1";
    }
    else
    {
        if ([gridRating.type isEqualToString:@"thumbsDown"])
        {
            [self SetEnableLikeView:YES];
            likeCount = @"0";
            return;
        }
    }
    
    NSInteger thumsUp = 0;
    NSInteger thumsDown = 0;
    if(sender.tag == 0)
    {
        if(self.thumsUpCountA > 0)
        {
            //self.thumsUpCountA = self.thumsUpCountA - 1;
        }
        else
        {
            self.thumsUpCountA = 0;
        }
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsUpCountA = 0;
        }
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsDownCountA = self.thumsDownCountA - 1;
        }
        else
        {
            self.thumsDownCountA = self.thumsDownCountA + 1;
        }
        thumsUp = self.thumsUpCountA;
        thumsDown = self.thumsDownCountA;
        
        [self.thumbsUpA setTitle:[NSString stringWithFormat:@"%ld",(long)thumsUp] forState:UIControlStateNormal];
        [self.thumbsDownA setTitle:[NSString stringWithFormat:@"%ld",(long)thumsDown] forState:UIControlStateNormal];
        
        [self.thumbsDownA setImage:[UIImage imageNamed:@"thumbsDownIconFill"] forState:UIControlStateNormal];
    }
    else if(sender.tag == 1)
    {
        if(self.thumsUpCountB > 0)
        {
            //self.thumsUpCountB = self.thumsUpCountB - 1;
        }
        else
        {
            self.thumsUpCountB = 0;
        }
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsUpCountB = 0;
        }
        
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsDownCountB = self.thumsDownCountB - 1;
        }
        else
        {
            self.thumsDownCountB = self.thumsDownCountB + 1;
        }
        thumsUp = self.thumsUpCountB;
        thumsDown = self.thumsDownCountB;
        [self.thumbsUpB setTitle:[NSString stringWithFormat:@"%ld",(long)thumsUp] forState:UIControlStateNormal];
        [self.thumbsDownB setTitle:[NSString stringWithFormat:@"%ld",(long)thumsDown] forState:UIControlStateNormal];
        
        [self.thumbsDownB setImage:[UIImage imageNamed:@"thumbsDownIconFill"] forState:UIControlStateNormal];
    }
    else if(sender.tag == 2)
    {
        if(self.thumsUpCountC > 0)
        {
            //self.thumsUpCountC = self.thumsUpCountC - 1;
        }
        else
        {
            self.thumsUpCountC = 0;
        }
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsUpCountC = 0;
        }
        
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsDownCountC = self.thumsDownCountC - 1;
        }
        else
        {
            self.thumsDownCountC = self.thumsDownCountC + 1;
        }
        thumsUp = self.thumsUpCountC;
        thumsDown = self.thumsDownCountC;
        [self.thumbsUpC setTitle:[NSString stringWithFormat:@"%ld",(long)thumsUp] forState:UIControlStateNormal];
        [self.thumbsDownC setTitle:[NSString stringWithFormat:@"%ld",(long)thumsDown] forState:UIControlStateNormal];
        
        [self.thumbsDownC setImage:[UIImage imageNamed:@"thumbsDownIconFill"] forState:UIControlStateNormal];
    }
    else if(sender.tag == 3)
    {
        if(self.thumsUpCountD > 0)
        {
            //self.thumsUpCountD = self.thumsUpCountD - 1;
        }
        else
        {
            self.thumsUpCountD = 0;
        }
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsUpCountD = 0;
        }
        
        if([likeCount isEqualToString:@"0"])
        {
            self.thumsDownCountD = self.thumsDownCountD - 1;
        }
        else
        {
            self.thumsDownCountD = self.thumsDownCountD + 1;
        }
        
        thumsUp = self.thumsUpCountD;
        thumsDown = self.thumsDownCountD;
        [self.thumbsUpD setTitle:[NSString stringWithFormat:@"%ld",(long)thumsUp] forState:UIControlStateNormal];
        [self.thumbsDownD setTitle:[NSString stringWithFormat:@"%ld",(long)thumsDown] forState:UIControlStateNormal];
        
        [self.thumbsDownD setImage:[UIImage imageNamed:@"thumbsDownIconFill"] forState:UIControlStateNormal];
    }
   
    
//    [self.showThumbsUpBtn setTitle:[NSString stringWithFormat:@"%ld",(long)self.thumsUpCountA] forState:UIControlStateNormal];
//    [_showThumbsDownBtn setTitle:[NSString stringWithFormat:@"%ld",(long)self.thumsUpCountA] forState:UIControlStateNormal];
    
    gridRating.User = backendless.userService.currentUser;
    gridRating.photo=selectedPhoto1;
    gridRating.video=selectedVideo1;
    gridRating.type=@"thumbsDown";
    //    alreadyRatingForCurrentUser.GridId = itemsObjectId;
    if(sender.tag == 0)
    {
        alreadyRatingDownGridA = gridRating;
    }
    else if(sender.tag == 1)
    {
        alreadyRatingDownGridB = gridRating;
    }
    else if(sender.tag == 2)
    {
        alreadyRatingDownGridC = gridRating;
    }
    else if(sender.tag == 3)
    {
        alreadyRatingDownGridD = gridRating;
    }
    
    id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
    [dataStore1 save:gridRating response:^(id cm) {
        [SVProgressHUD dismiss];
        if(sender.tag == 0)
        {
            alreadyRatingDownGridA = (Rating *)cm;
        }
        else if(sender.tag == 1)
        {
            alreadyRatingDownGridB = (Rating *)cm;
        }
        else if(sender.tag == 2)
        {
            alreadyRatingDownGridC = (Rating *)cm;
        }
        else if(sender.tag == 3)
        {
            alreadyRatingDownGridD = (Rating *)cm;
        }
        [self SetEnableLikeView:YES];
        
    } error:^(Fault *error) {
        NSLog(@"something went wrong... %@",error.detail);
        [SVProgressHUD dismiss];
        if(sender.tag == 0)        {
            alreadyRatingDownGridA = nil;

            self.thumsDownCountA = self.thumsDownCountA - 1;
            [self.thumbsDownA setTitle:[NSString stringWithFormat:@"%ld",self.thumsDownCountA] forState:UIControlStateNormal];
            [self.thumbsDownA setImage:[UIImage imageNamed:@"thumbsDownIcon"] forState:UIControlStateNormal];

        }
        else if(sender.tag == 1){
            alreadyRatingDownGridB = nil;
            self.thumsDownCountB = self.thumsDownCountB - 1;
            [self.thumbsDownB setTitle:[NSString stringWithFormat:@"%ld",self.thumsDownCountB] forState:UIControlStateNormal];
            
            [self.thumbsDownB setImage:[UIImage imageNamed:@"thumbsDownIcon"] forState:UIControlStateNormal];
        }
        else if(sender.tag == 2){
            alreadyRatingDownGridC = nil;
            self.thumsDownCountC = self.thumsDownCountC - 1;
            [self.thumbsDownC setTitle:[NSString stringWithFormat:@"%ld",self.thumsDownCountC] forState:UIControlStateNormal];
            
            
            [self.thumbsDownC setImage:[UIImage imageNamed:@"thumbsDownIcon"] forState:UIControlStateNormal];
        }
        else if(sender.tag == 3){
            alreadyRatingDownGridD = nil;
            self.thumsDownCountD = self.thumsDownCountD - 1;
            [self.thumbsDownD setTitle:[NSString stringWithFormat:@"%ld",self.thumsDownCountD] forState:UIControlStateNormal];
           
            [self.thumbsDownD setImage:[UIImage imageNamed:@"thumbsDownIcon"] forState:UIControlStateNormal];
        }
        //        [self.BtnThumbsUp setEnabled:YES];
        //        [self.btnThumbsDown setEnabled:YES];
        [self SetEnableLikeView:YES];
    }];
    
}
- (IBAction)thumbsUpAction:(UIButton *)sender
{
    [self thumbsUpActionForGrid:sender];
    return;
    [self.BtnThumbsUp setEnabled:NO];
    [self.btnThumbsDown setEnabled:NO];
    
    if (!alreadyRatingForCurrentUser)
    {
        alreadyRatingForCurrentUser = [Rating new];
    }
    else
    {
        if ([alreadyRatingForCurrentUser.type isEqualToString:@"thumbsUp"])
        {
            [self.BtnThumbsUp setEnabled:YES];
            [self.btnThumbsDown setEnabled:YES];
            return;
        }
    }
//    [self.showThumbsUpBtn setTitle:[NSString stringWithFormat:@"1 Thumbs Up "] forState:UIControlStateNormal];
//
//    [_showThumbsDownBtn setTitle:[NSString stringWithFormat:@"0 Thumbs Down "] forState:UIControlStateNormal];
    
    [self.showThumbsUpBtn setTitle:[NSString stringWithFormat:@"%lu Thumbs Up ",(unsigned long)self.thumbsUpFound.count + 1] forState:UIControlStateNormal];
    
    if ([alreadyRatingForCurrentUser.type isEqualToString:@"thumbsDown"])
    {
        [_showThumbsDownBtn setTitle:[NSString stringWithFormat:@"%lu Thumbs Down ",(unsigned long)self.thumbsDownFound.count - 1] forState:UIControlStateNormal];
    }
    
    alreadyRatingForCurrentUser.User = backendless.userService.currentUser;
//    alreadyRatingForCurrentUser.photo=self.selectedPhoto;
//    alreadyRatingForCurrentUser.video=self.selectedVideo;
    alreadyRatingForCurrentUser.type=@"thumbsUp";
    alreadyRatingForCurrentUser.GridId = self.grid.objectId;
    
    id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
    [dataStore1 save:alreadyRatingForCurrentUser response:^(id cm) {
        
        [self fetchAllRatings];
        
        [self.BtnThumbsUp setEnabled:YES];
        [self.btnThumbsDown setEnabled:YES];
        
    } error:^(Fault *error) {
        NSLog(@"something went wrong... %@",error.detail);
        [self.BtnThumbsUp setEnabled:YES];
        [self.btnThumbsDown setEnabled:YES];
    }];
}

- (IBAction)thumbsDownAction:(UIButton *)sender {
    
    [self thumbsDownActionForGrid:sender];
    
    return;
    [self.BtnThumbsUp setEnabled:NO];
    [self.btnThumbsDown setEnabled:NO];
    if (!alreadyRatingForCurrentUser) {
        alreadyRatingForCurrentUser = [Rating new];
    }
    else
    {
        if ([alreadyRatingForCurrentUser.type isEqualToString:@"thumbsDown"])
        {
            [self.BtnThumbsUp setEnabled:YES];
            [self.btnThumbsDown setEnabled:YES];
            return;
        }
    }
//    [self.showThumbsUpBtn setTitle:[NSString stringWithFormat:@"0 Thumbs Up "] forState:UIControlStateNormal];
//
//    [_showThumbsDownBtn setTitle:[NSString stringWithFormat:@"1 Thumbs Down "] forState:UIControlStateNormal];
    
    [_showThumbsDownBtn setTitle:[NSString stringWithFormat:@"%lu Thumbs Down ",(unsigned long)self.thumbsDownFound.count + 1] forState:UIControlStateNormal];
    
    if ([alreadyRatingForCurrentUser.type isEqualToString:@"thumbsUp"])
    {
        [self.showThumbsUpBtn setTitle:[NSString stringWithFormat:@"%lu Thumbs Up ",(unsigned long)self.thumbsUpFound.count - 1] forState:UIControlStateNormal];
    }
    
    alreadyRatingForCurrentUser.User = backendless.userService.currentUser;
//    alreadyRatingForCurrentUser.photo=self.selectedPhoto;
//    alreadyRatingForCurrentUser.video=self.selectedVideo;
//    alreadyRatingForCurrentUser.type=@"thumbsDown";
    alreadyRatingForCurrentUser.GridId = self.grid.objectId;
    alreadyRatingForCurrentUser.type=@"thumbsDown";
    
    
    id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
    [dataStore1 save:alreadyRatingForCurrentUser response:^(id ch) {
        
        [self fetchAllRatings];
        
        [self.BtnThumbsUp setEnabled:YES];
        [self.btnThumbsDown setEnabled:YES];
        
    } error:^(Fault *error) {
        NSLog(@"something went wrong... %@",error.detail);
        [self.BtnThumbsUp setEnabled:YES];
        [self.btnThumbsDown setEnabled:YES];
    }];
    
}
- (IBAction)showThumbsUpAction:(UIButton *)sender {
    
    if (!sender.tag) {
        
        sender.tag = 1 ;
        _showThumbsDownBtn.tag = 0 ;
        
        
    }
    else{
        sender.tag = 0 ;
    }
//    [self fetchThumbsUps];
    
}

- (IBAction)showThumbsDownAction:(UIButton *)sender {
    
    if (!sender.tag) {
        
        sender.tag = 1 ;
        _showThumbsUpBtn.tag = 0 ;
        
    }
    else{
        sender.tag = 0 ;
    }
//    [self fetchThumbsdown];
    
}

- (IBAction)doneCommentAction:(id)sender
{

    NSString *commentStr = [self.writeCommntTxt.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(commentStr.length == 0)
    {
        [appDel displayAlertWithTitle:@"Alert" andMessage:@"Comment can not be blank."];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Adding comment..."];
//    [NSThread sleepForTimeInterval:0.5];//2 seconds
    id obj = allPhotosAndVideosArray[0];
    Photo *selectedPhoto1;
    Video *selectedVideo1;
    if ([obj isKindOfClass:[Photo class]])
    {
        selectedPhoto1 = obj;
    }
    else
    {
        selectedVideo1 = obj;
    }
    [self.view endEditing:YES];
    [self.writeCommntTxt resignFirstResponder];
    Comments *comment = [Comments new];
    
    comment.Message = self.writeCommntTxt.text;
    comment.User=backendless.userService.currentUser;
    comment.photo=selectedPhoto1;
    comment.video=selectedVideo1;
//    comment.Grid
//    alreadyRatingForCurrentUser.GridId = self.grid.objectId;
    id<IDataStore> dataStore1 = [backendless.persistenceService of:[Comments class]];
    [dataStore1 save:comment response:^(id cm) {
        
        self.writeCommntTxt.text=@"";
        //[self.writeCommentTxtField becomeFirstResponder];
        [self fetchComments];
        
        [SVProgressHUD dismiss];
        
        
    } error:^(Fault *error) {
        NSLog(@"something went wrong... %@",error.detail);
        
        [SVProgressHUD dismiss];
        
    }];
    
}

#pragma mark - Table Views

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if (tableView.tag==1){
        
        return templates.count;
    }
    else if (tableView.tag==2)
    {
        return self.CommentsFound.count;
    }
    else if (tableView.tag==3)
    {
        return self.thumbsUpFound.count;
    }
    
    else if (tableView.tag==4)
    {
        return self.thumbsDownFound.count;
    }
    
    return 0;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [self.view layoutIfNeeded];
    
    if(tableView.tag==2){
        static NSString *MyIdentifier = @"commentsCell";
        
        
        SingleContentCommentsTableViewCell  *cell = [_commentsTableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
        
        if (cell == nil)
        {
            cell = [[SingleContentCommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                             reuseIdentifier:MyIdentifier];
        }
        
        
        Comments *comment= [self.CommentsFound objectAtIndex:indexPath.row];
        cell.userName.text= comment.User.name;
        cell.commentBody.text=comment.Message;
        
        NSString *commentDateString = [Utility getTimeFormatForDate:comment.created] ;
        cell.commentTime.text=[NSString stringWithFormat:@"%@",commentDateString];
        [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:[comment.User getProperty:@"profileImage"] ]];
        
        [cell.userProfileImage.layer setCornerRadius:cell.userProfileImage.frame.size.height/2];
        cell.userProfileImage.clipsToBounds=YES;
        return cell;}
    
    else if (tableView.tag==1){
        
        static NSString *simpleTableIdentifier = @"SimpleTableItem";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        cell.textLabel.text = [templates objectAtIndex:indexPath.row];
        return cell;
        
    }
    
    return 0;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1) {
        self.writeCommntTxt.text = templates[indexPath.row];
        _openTemplates.tag = 0 ;
    }
    [self.templateView setHidden:YES];
}

//-(void)setupScales
//{
//    // Set up the minimum & maximum zoom scales
//    CGRect scrollViewFrame = self.imageScrollView.frame;
//    CGFloat scaleWidth = scrollViewFrame.size.width / self.imageScrollView.contentSize.width;
//    CGFloat scaleHeight = scrollViewFrame.size.height / self.imageScrollView.contentSize.height;
//    CGFloat minScale = MIN(scaleWidth, scaleHeight);
//
//    self.imageScrollView.minimumZoomScale = minScale;
//    self.imageScrollView.maximumZoomScale = 3.0f;
//    self.imageScrollView.zoomScale = minScale;
//
//    [self centerScrollViewContents];
//}
//
//- (void)centerScrollViewContents {
//    // This method centers the scroll view contents also used on did zoom
//    CGSize boundsSize = self.imageScrollView.bounds.size;
//    CGRect contentsFrame = self.singleImageView.frame;
//
//    if (contentsFrame.size.width < boundsSize.width) {
//        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
//    } else {
//        contentsFrame.origin.x = 0.0f;
//    }
//
//    if (contentsFrame.size.height < boundsSize.height) {
//        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
//    } else {
//        contentsFrame.origin.y = 0.0f;
//    }
//
//    self.singleImageView.frame = contentsFrame;
//}
//
//#pragma mark -
//#pragma mark - ScrollView Delegate methods
//- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
//    // Return the view that we want to zoom
//    return self.singleImageView;
//}
//
//- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
//    // The scroll view has zoomed, so we need to re-center the contents
//    [self centerScrollViewContents];
//}
//
//#pragma mark -
//#pragma mark - ScrollView gesture methods
//- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer {
//    // Get the location within the image view where we tapped
//    CGPoint pointInView = [recognizer locationInView:self.singleImageView];
//
//    // Get a zoom scale that's zoomed in slightly, capped at the maximum zoom scale specified by the scroll view
//    CGFloat newZoomScale = self.imageScrollView.zoomScale * 1.5f;
//    newZoomScale = MIN(newZoomScale, self.imageScrollView.maximumZoomScale);
//
//    // Figure out the rect we want to zoom to, then zoom to it
//    CGSize scrollViewSize = self.imageScrollView.bounds.size;
//
//    CGFloat w = scrollViewSize.width / newZoomScale;
//    CGFloat h = scrollViewSize.height / newZoomScale;
//    CGFloat x = pointInView.x - (w / 2.0f);
//    CGFloat y = pointInView.y - (h / 2.0f);
//
//    CGRect rectToZoomTo = CGRectMake(x, y, w, h);
//
//    [self.imageScrollView zoomToRect:rectToZoomTo animated:YES];
//}
//
//- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer {
//    // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
//    CGFloat newZoomScale = self.imageScrollView.zoomScale / 1.5f;
//    newZoomScale = MAX(newZoomScale, self.imageScrollView.minimumZoomScale);
//    [self.imageScrollView setZoomScale:newZoomScale animated:YES];
//}
@end
