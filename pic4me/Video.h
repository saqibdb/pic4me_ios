//
//  Video.h
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/20/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"
@interface Video : NSObject

@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *VideoURL;
@property (nonatomic, strong) BackendlessUser *User;
@property (nonatomic, strong) NSString *Title;
@property (nonatomic, strong) NSString *ThumbnailURL;

@end
