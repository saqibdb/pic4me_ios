//
//  SingleContentViewController.m
//  pic4me
//
//  Created by ibuildx on 7/4/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//
#import "UIViewController+CWPopup.h"

#import "SingleContentViewController.h"
#import "SingleContentCommentsTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "Comments.h"
#import "Backendless.h"
#import "Utility.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import <Social/Social.h>
#import "Rating.h"
#import "sharePopUp.h"
#import "RatingUsersTableViewCell.h"
@interface SingleContentViewController(){
    NSArray *templates;
    UIButton *playBTN;
    NSUInteger thumbsUpCount ;
    NSInteger *thumbsDownCount ;
    Rating *alreadyRatingForCurrentUser;
    NSMutableArray *allRatings ;

}

@end

@interface SingleContentViewController()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end

@implementation SingleContentViewController
@synthesize grid;
- (void)viewDidLoad {
    NSLog(@"SingleContentViewController");
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    
    if(self.selectedPhoto != nil){
        [_mainContentImage sd_setImageWithURL:[NSURL URLWithString:self.selectedPhoto.photo]];
        _mainContentImage.clipsToBounds=YES;
    }
    else{
        NSURL *url = [NSURL URLWithString:self.selectedVideo.VideoURL];
        [self createAntPlayVideo:self.containerView andUrl:url];
    }
    self.writeCommentTxtField.delegate = self;
    self.writeCommntTxt.delegate = self;
    
    [self.gridUserProfileImage.layer setCornerRadius:self.gridUserProfileImage.frame.size.height/2];
    self.gridUserProfileImage.clipsToBounds=YES;
    self.gridUserProfileImage.layer.borderWidth=1.5;
    self.gridUserProfileImage.layer.borderColor=[[UIColor blackColor] CGColor];
    
    [self.gridUserProfileImage sd_setImageWithURL:[NSURL URLWithString:[self.grid.User getProperty:@"profileImage"]]
                                 placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                          options:SDWebImageRefreshCached];
    
    
    self.gridUserName.text = self.grid.User.name;
    if (self.grid.location != nil) {
        self.lblLocation.text = self.grid.location;
    }
    else{
        self.lblLocation.text = @" ";
    }
    if (self.grid.comment != nil) {
        self.lblComment.text = self.grid.comment;
    }
    NSString *commentDateString = [Utility getTimeFormatForDate:self.grid.created] ;
    self.lblTime.text = commentDateString;
    
    templates = [[NSArray alloc] initWithObjects:@"Love it!", @"You look ridiculous", @"wow ",@"nice look dude",@"awesome", @"Lit", @"Lit AF", @"Dope", @"No comment", nil];
    
    self.commentsTableView.dataSource =self;
    self.commentsTableView.delegate=self;
    self.templateCommetnsTableView.dataSource =self;
    self.templateCommetnsTableView.delegate=self;
    self.ratingUserTableView.dataSource=self;
    self.ratingUserTableView.delegate=self;
    [self fetchComments];
//    [self fetchThumbsUps];
//    [self fetchThumbsdown];
    [self fetchAllRating];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//#pragma mark - scroll to active textField
//
//- (void)textFieldDidBeginEditing:(UITextField *)textField {
//    [self.scrollView setContentOffset:CGPointMake(0,textField.center.y-60) animated:YES];
//}
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    NSInteger nextTag = textField.tag + 1;
//    // Try to find next responder
//    UIResponder *nextResponder = [textField.superview viewWithTag:nextTag];
//
//    if (nextResponder) {
//        [self.scrollView setContentOffset:CGPointMake(0,textField.center.y-60) animated:YES];
//        // Found next responder, so set it.
//        [nextResponder becomeFirstResponder];
//    } else {
//        [self.scrollView setContentOffset:CGPointMake(0,0) animated:YES];
//        [textField resignFirstResponder];
//        return YES;
//    }
//    return NO;
//}
#pragma mark - fetch data
-(void)createAntPlayVideo:(UIView *)view andUrl:(NSURL *)url{
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.delegate = self;
    _playerViewController.view.frame = view.bounds;
    _playerViewController.showsPlaybackControls = YES;
    
    // First create an AVPlayerItem
    
    AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:url];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
    _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
    
    UIImage *normImage = [UIImage imageNamed:@""];
    playBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    [playBTN addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
    [playBTN setImage:normImage forState:UIControlStateNormal];
    playBTN.frame = CGRectMake(0, 0, normImage.size.width, normImage.size.height);
    playBTN.center = CGPointMake(_playerViewController.view.frame.size.width/2, _playerViewController.view.frame.size.height/2);
    playBTN.tag = view.tag;
    [view addSubview:_playerViewController.view];
    //target.layer.contents = ;
    [_playerViewController.view addSubview:playBTN];
    
    
    [ _playerViewController.view setAutoresizingMask:( UIViewAutoresizingFlexibleWidth |
                                                      UIViewAutoresizingFlexibleHeight )];
    [view setAutoresizesSubviews:YES ];
    
    
    playBTN.hidden = NO ;
    [[_playerViewController player] play];
    
    
    
    
}
- (void)playVideo:(UIButton *)sender
{
    
    playBTN.hidden = YES;
    
    
    [[_playerViewController player] play];
    /*
     timeSpent = 0.0;
     previewTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
     target:self
     selector:@selector(checkPlayerTimer:)
     userInfo:nil
     repeats:YES];
     */
}
-(void)itemDidFinishPlaying:(NSNotification *) notification {
    [playBTN setHidden:NO];
    AVPlayerItem *p = [notification object];
    
    
    [p seekToTime:kCMTimeZero];
}

-(void)fetchThumbsUps
{
    
    NSString *itemsObject;
    NSString *itemsObjectId;
    if(self.selectedPhoto != nil){
        
        itemsObjectId = self.selectedPhoto.objectId;
        itemsObject = @"photo";
    }
    else {
        
        itemsObjectId = self.selectedVideo.objectId;
        itemsObject = @"video";
    }
    
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"type = 'thumbsUp' AND %@.objectId =\'%@\' " ,itemsObject,itemsObjectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Rating class]];
    [dataStore find:query response:^(BackendlessCollection *thumbsUpColection) {
        
        
        self.thumbsUpFound = [[NSMutableArray alloc] initWithArray:thumbsUpColection.data];
        
        [_showThumbsUpBtn setTitle:[NSString stringWithFormat:@"%lu Thumbs Up ",(unsigned long)self.thumbsUpFound.count] forState:UIControlStateNormal];
        thumbsUpCount=self.thumbsUpFound.count;
        
        for (Rating *rating in self.thumbsUpFound){
            
            
            if([rating.User.objectId isEqualToString: backendless.userService.currentUser.objectId]){
                
                //self.BtnThumbsUp.enabled=NO;
                //self.btnThumbsDown.enabled=NO;
                alreadyRatingForCurrentUser = rating;
                
            }
        }
        
        [self.ratingUserTableView reloadData];
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
    
    
}
-(void)fetchThumbsdown{
    
    NSString *itemsObject;
    NSString *itemsObjectId;
    if(self.selectedPhoto != nil){
        itemsObjectId = self.selectedPhoto.objectId;
        itemsObject = @"photo";
    }
    else {
        itemsObjectId = self.selectedVideo.objectId;
        itemsObject = @"video";
    }

    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"type = 'thumbsDown' AND %@.objectId =\'%@\' ",itemsObject,itemsObjectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Rating class]];
    [dataStore find:query response:^(BackendlessCollection *thumbsdownColection) {
        
        
        self.thumbsDownFound = [[NSMutableArray alloc] initWithArray:thumbsdownColection.data];
        
        [_showThumbsDownBtn setTitle:[NSString stringWithFormat:@"%lu Thumbs Down ",(unsigned long)self.thumbsDownFound.count] forState:UIControlStateNormal];
        
        for (Rating *rating in self.thumbsDownFound){
            
            
            if([rating.User.objectId isEqualToString :backendless.userService.currentUser.objectId  ]){
                
                //self.btnThumbsDown.enabled=NO;
                //self.BtnThumbsUp.enabled=NO;
                
                alreadyRatingForCurrentUser = rating;
                
            }
        }
        [self.ratingUserTableView reloadData];
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
    
    
}
-(void)fetchComments{
    
    
    NSString *itemsObject;
    NSString *itemsObjectId;
    if(self.selectedPhoto != nil){
        itemsObjectId = self.selectedPhoto.objectId;
        itemsObject = @"photo";
    }
    else {
        itemsObjectId = self.selectedVideo.objectId;
        itemsObject = @"video";
    }

    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"%@.objectId = '%@' ",itemsObject,itemsObjectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Comments class]];
    [dataStore find:query response:^(BackendlessCollection *Comments) {
        
        
        self.CommentsFound = [[NSMutableArray alloc] initWithArray:Comments.data];
        [SVProgressHUD dismiss];
        
        
        [self.commentsTableView reloadData];
        
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
}

-(void)fetchAllRating{
    
    NSString *itemsObject;
    NSString *itemsObjectId;
    if(self.selectedPhoto != nil){
        
        itemsObjectId = self.selectedPhoto.objectId;
        itemsObject = @"photo";
    }
    else {
        
        itemsObjectId = self.selectedVideo.objectId;
        itemsObject = @"video";
    }
    
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"%@.objectId =\'%@\' ",itemsObject,itemsObjectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Rating class]];
    [dataStore find:query response:^(BackendlessCollection *foundRatings) {
        
        if (foundRatings.data.count) {
            allRatings = [[NSMutableArray alloc] initWithArray:foundRatings.data];
            self.thumbsUpFound = [[NSMutableArray alloc] init];
            self.thumbsDownFound = [[NSMutableArray alloc] init];
            
            for (Rating *rating in allRatings)
            {
                if ([rating.type isEqualToString:@"thumbsUp"])
                {
                    [self.thumbsUpFound addObject:rating];
                }
                else
                {
                    [self.thumbsDownFound addObject:rating];
                }
                if([rating.User.objectId isEqualToString: backendless.userService.currentUser.objectId])
                {
                    alreadyRatingForCurrentUser = rating;
                    break;
                }
            }
            
        }
        [self.showThumbsUpBtn setTitle:[NSString stringWithFormat:@"%lu Thumbs Up ",(unsigned long)self.thumbsUpFound.count] forState:UIControlStateNormal];
        thumbsUpCount=self.thumbsUpFound.count;
        [_showThumbsDownBtn setTitle:[NSString stringWithFormat:@"%lu Thumbs Down ",(unsigned long)self.thumbsDownFound.count] forState:UIControlStateNormal];
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at photo  %@",error.detail);
    }];
    
    
}
#pragma mark - tableView delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if (tableView.tag==1){
        
        return templates.count;
    }
    else if (tableView.tag==2)
    {
        return self.CommentsFound.count;
    }
    else if (tableView.tag==3)
    {
        return self.thumbsUpFound.count;
    }
    
    else if (tableView.tag==4)
    {
        return self.thumbsDownFound.count;
    }
    
    return 0;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view layoutIfNeeded];

    if(tableView.tag==2){
        static NSString *MyIdentifier = @"commentsCell";
        
        
        SingleContentCommentsTableViewCell  *cell = [_commentsTableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
        
        if (cell == nil)
        {
            cell = [[SingleContentCommentsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                             reuseIdentifier:MyIdentifier];
        }
        
        
        Comments *comment= [self.CommentsFound objectAtIndex:indexPath.row];
        cell.userName.text= comment.User.name;
        cell.commentBody.text=comment.Message;
        
        NSString *commentDateString = [Utility getTimeFormatForDate:comment.created] ;
        cell.commentTime.text=[NSString stringWithFormat:@"%@",commentDateString];
        [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:[comment.User getProperty:@"profileImage"] ]];
        
        [cell.userProfileImage.layer setCornerRadius:cell.userProfileImage.frame.size.height/2];
        cell.userProfileImage.clipsToBounds=YES;
        return cell;}
    
    else if (tableView.tag==1){
        
        static NSString *simpleTableIdentifier = @"SimpleTableItem";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        cell.textLabel.text = [templates objectAtIndex:indexPath.row];
        return cell;
        
    }
    else if (self.ratingUserTableView.tag==3) {
        
        static NSString *simpleTableIdentifier = @"ratingUserCell";
        
        RatingUsersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[RatingUsersTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        Rating *rating = [self.thumbsUpFound objectAtIndex:indexPath.row];
        [cell.ProfileImage sd_setImageWithURL:[NSURL URLWithString:[rating.User getProperty:@"profileImage"] ]];
        cell.userName.text = rating.User.name;
        [cell.ProfileImage.layer setCornerRadius:cell.ProfileImage.frame.size.height/2];
        cell.ProfileImage.clipsToBounds=YES;
        return cell;
        
    }
    else if(self.ratingUserTableView.tag==4) {
        static NSString *simpleTableIdentifier = @"ratingUserCell";
        
        RatingUsersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[RatingUsersTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        Rating *rating = [self.thumbsDownFound objectAtIndex:indexPath.row];
        [cell.ProfileImage sd_setImageWithURL:[NSURL URLWithString:[rating.User getProperty:@"profileImage"] ]];
        cell.userName.text = rating.User.name;
        [cell.ProfileImage.layer setCornerRadius:cell.ProfileImage.frame.size.height/2];
        cell.ProfileImage.clipsToBounds=YES;
        return cell;
        
    }
    
    return 0;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag == 1) {
        self.writeCommntTxt.text = templates[indexPath.row];
        _openTemplates.tag = 0 ;
    }
    [self.templateView setHidden:YES];
    
}
#pragma mark - social media sharing
-(void)dismissPopup{
    
    [self dismissPopupViewControllerAnimated:YES completion:nil];
}
#pragma mark - actions
- (IBAction)openTemplateComments:(UIButton *)sender {
    
    self.templateCommetnsTableView.tag = 1 ;
    if (!sender.tag) {
        [self.templateView setHidden:NO];
        
        sender.tag = 1 ;
        _openTemplates.tag = 0 ;
        
    }
    else{
        [self.templateView setHidden:YES];
        
        sender.tag = 0 ;
    }
    [self.templateCommetnsTableView reloadData];
}


- (IBAction)thumbsUpAction:(UIButton *)sender
{
    [self.BtnThumbsUp setEnabled:NO];
    [self.btnThumbsDown setEnabled:NO];
    
//    Rating *rating = [Rating new];
    
    if (!alreadyRatingForCurrentUser)
    {
        alreadyRatingForCurrentUser = [Rating new];
    }
    else
    {
        if ([alreadyRatingForCurrentUser.type isEqualToString:@"thumbsUp"])
        {
            [self.BtnThumbsUp setEnabled:YES];
            [self.btnThumbsDown setEnabled:YES];
            return;
        }
    }
    
    [self.showThumbsUpBtn setTitle:[NSString stringWithFormat:@"1 Thumbs Up "] forState:UIControlStateNormal];
    [_showThumbsDownBtn setTitle:[NSString stringWithFormat:@"0 Thumbs Down "] forState:UIControlStateNormal];
    
    NSString *itemsObjectId;
    if(self.selectedPhoto != nil)
    {
        
        itemsObjectId = self.selectedPhoto.objectId;
    }
    else {
        
        itemsObjectId = self.selectedVideo.objectId;
    }
    
    alreadyRatingForCurrentUser.User = backendless.userService.currentUser;
    alreadyRatingForCurrentUser.photo=self.selectedPhoto;
    alreadyRatingForCurrentUser.video=self.selectedVideo;
    alreadyRatingForCurrentUser.type=@"thumbsUp";
//    alreadyRatingForCurrentUser.GridId = itemsObjectId;

    
    id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
    [dataStore1 save:alreadyRatingForCurrentUser response:^(id cm) {
        
//        [self fetchThumbsUps];
//        [self fetchThumbsdown];
        [self fetchAllRating];
        [self.BtnThumbsUp setEnabled:YES];
        [self.btnThumbsDown setEnabled:YES];
        
        
    } error:^(Fault *error) {
        NSLog(@"something went wrong... %@",error.detail);
        [self.BtnThumbsUp setEnabled:YES];
        [self.btnThumbsDown setEnabled:YES];
    }];
    
}

- (IBAction)thumbsDownAction:(UIButton *)sender {
    [self.BtnThumbsUp setEnabled:NO];
    [self.btnThumbsDown setEnabled:NO];
    
    if (!alreadyRatingForCurrentUser) {
        alreadyRatingForCurrentUser = [Rating new];
    }
    else{
        if ([alreadyRatingForCurrentUser.type isEqualToString:@"thumbsDown"]) {
            [self.BtnThumbsUp setEnabled:YES];
            [self.btnThumbsDown setEnabled:YES];
            return;
        }
    }
    NSString *itemsObjectId;
    if(self.selectedPhoto != nil){
        
        itemsObjectId = self.selectedPhoto.objectId;
    }
    else {
        
        itemsObjectId = self.selectedVideo.objectId;
    }
    
    
    [self.showThumbsUpBtn setTitle:[NSString stringWithFormat:@"0 Thumbs Up "] forState:UIControlStateNormal];
    [_showThumbsDownBtn setTitle:[NSString stringWithFormat:@"1 Thumbs Down "] forState:UIControlStateNormal];
    
//    Rating *rating = [Rating new];
    
    alreadyRatingForCurrentUser.User = backendless.userService.currentUser;
    alreadyRatingForCurrentUser.photo=self.selectedPhoto;
    alreadyRatingForCurrentUser.video=self.selectedVideo;
    alreadyRatingForCurrentUser.type=@"thumbsDown";
//    alreadyRatingForCurrentUser.GridId = itemsObjectId;

    id<IDataStore> dataStore1 = [backendless.persistenceService of:[Rating class]];
    [dataStore1 save:alreadyRatingForCurrentUser response:^(id ch) {
        
//        [self fetchThumbsUps];
//        [self fetchThumbsdown];
        [self fetchAllRating];
        [self.BtnThumbsUp setEnabled:YES];
        [self.btnThumbsDown setEnabled:YES];
        
    } error:^(Fault *error) {
        NSLog(@"something went wrong... %@",error.detail);
        [self.BtnThumbsUp setEnabled:YES];
        [self.btnThumbsDown setEnabled:YES];
    }];
    
}
- (IBAction)showThumbsUpAction:(UIButton *)sender {
    
    self.ratingUserTableView.tag = 3 ;
    if (!sender.tag) {
//        [self.ratingview setHidden:NO];
        
        sender.tag = 1 ;
        _showThumbsDownBtn.tag = 0 ;
        
        
    }
    else{
//        [self.ratingview setHidden:YES];
        sender.tag = 0 ;
    }
//    [self fetchThumbsUps];
    
}

- (IBAction)showThumbsDownAction:(UIButton *)sender {
    
    self.ratingUserTableView.tag = 4 ;
    if (!sender.tag) {
//        [self.ratingview setHidden:NO];
        
        sender.tag = 1 ;
        _showThumbsUpBtn.tag = 0 ;
        
    }
    else{
//        [self.ratingview setHidden:YES];
        sender.tag = 0 ;
    }
//    [self fetchThumbsdown];
    
}
- (IBAction)unwindToSingleContent:(UIStoryboardSegue*)sender
{
    
}

- (IBAction)singleContentBackAction:(id)sender {
    
    
    [self performSegueWithIdentifier:@"singleToShared" sender:self];
}
- (IBAction)doneCommentAction:(id)sender {
    
    
    
    
    [self.writeCommntTxt resignFirstResponder];
    Comments *comment = [Comments new];
    
    comment.Message = self.writeCommntTxt.text;
    comment.User=backendless.userService.currentUser;
    comment.photo=self.selectedPhoto;
    comment.video=self.selectedVideo;
    
    id<IDataStore> dataStore1 = [backendless.persistenceService of:[Comments class]];
    [dataStore1 save:comment response:^(id cm) {
        
        
        self.writeCommntTxt.text=@"";
        [self fetchComments];
        
        
    } error:^(Fault *error) {
        NSLog(@"something went wrong... %@",error.detail);
        
    }];
    
}
- (IBAction)shareAndRatingAction:(id)sender{
    sharePopUp *sharePop = [[sharePopUp alloc] initWithNibName:@"sharePopUp" bundle:nil];
    
    
    
    [self presentPopupViewController:sharePop animated:YES completion:^(void) {
        
        [sharePop.closeBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        [sharePop.facebookBtn addTarget:self action:@selector(facebookShare) forControlEvents:UIControlEventTouchUpInside];
        [sharePop.twitterBtn addTarget:self action:@selector(twitterShare) forControlEvents:UIControlEventTouchUpInside];
        [sharePop.instagramBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        
    }];
    
    
}
- (IBAction)hidePopAction:(UIButton *)sender {
    [self.ratingview setHidden:YES];
}
-(void)facebookShare{
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        
        SLComposeViewController *fbController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        // set up a completion handler (optional)
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
            
            [fbController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                    break;
                case SLComposeViewControllerResultDone:
                    break;
            }};
        
        
        
        [fbController setInitialText:@""];
        [fbController addURL:[NSURL URLWithString:@""]];
        //[fbController addImage:self.selectedPhoto];
        
        
        
        [fbController setCompletionHandler:completionHandler];
        [self presentViewController:fbController animated:YES completion:nil];
    }
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"you are not logged in your facebook account "] andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
        NSLog(@"you are not logged in ");
    }
    
    
    
    
    
}
-(void)twitterShare{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        
        SLComposeViewController *twitterController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        // set up a completion handler (optional)
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
            
            [twitterController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                    break;
                case SLComposeViewControllerResultDone:
                    break;
            }};
        
        
        
        //[twitterController addImage:image];
        
        [twitterController setInitialText:@""];
        [twitterController addURL:[NSURL URLWithString:@""]];
        [twitterController setCompletionHandler:completionHandler];
        [self presentViewController:twitterController animated:YES completion:nil];
    }
    
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"you are not logged in your twitter account"] andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
        
        NSLog(@"you are not logged in ");
    }
    
}

@end
