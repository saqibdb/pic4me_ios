//
//  ChatsViewController.m
//  pic4me
//
//  Created by ibuildx on 8/30/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//
#import "Backendless.h"
#import "ChatsViewController.h"
#import "InboxTableViewCell.h"
#import "Inbox.h"
#import "UIImageView+WebCache.h"
#import "FlatChatViewController.h"
#import "SVProgressHUD.h"
@interface ChatsViewController (){

    NSMutableArray *uniqueMsgs;
    NSMutableArray *allUniqueMsgs;


}

@end

@implementation ChatsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];

    self.tableView.dataSource=self;
    self.tableView.delegate=self;
//   [self fetchingUsersInboxAsync];
    uniqueMsgs = [[NSMutableArray alloc] init];
    allUniqueMsgs = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
    [self fetchingUsersInboxAsync];
}
//-(void)viewDidAppear:(BOOL)animated{
//    [self.view layoutIfNeeded];
////    uniqueMsgs = [[NSMutableArray alloc] init];
////    allUniqueMsgs = [[NSMutableArray alloc] init];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)fetchingUsersInboxAsync {
    
    [SVProgressHUD show];// showWithStatus:@"Adding comment..."];
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"Sender.objectId = '%@' OR Reciever.objectId = '%@'", backendless.userService.currentUser.objectId, backendless.userService.currentUser.objectId];
    
    /*NSNumber *pageSize = [NSNumber numberWithInt:1];
     [query.queryOptions setPageSize:pageSize];*/
    
    [backendless.persistenceService find:[Inbox class] dataQuery:query response:^(BackendlessCollection *collection1)  {
        NSLog(@"Inbox Found %i", [[collection1 getTotalObjects] intValue]);
        
        
        for (Inbox *inbox in collection1.data) {
            
            
            NSPredicate *predicateSender = [NSPredicate predicateWithFormat:@"Sender.objectId==%@",inbox.Sender.objectId];
            NSArray *resultsSender = [uniqueMsgs filteredArrayUsingPredicate:predicateSender];
            
            NSPredicate *predicateReceiver = [NSPredicate predicateWithFormat:@"Reciever.objectId==%@",inbox.Reciever.objectId];
            NSArray *resultsReceiver = [uniqueMsgs filteredArrayUsingPredicate:predicateReceiver];
            
            if (!resultsReceiver.count) {
                
                NSPredicate *predicateSenderReciever = [NSPredicate predicateWithFormat:@"Sender.objectId==%@ AND Reciever.objectId==%@",inbox.Reciever.objectId,inbox.Sender.objectId];
                NSArray *resultsSenderReciever = [uniqueMsgs filteredArrayUsingPredicate:predicateSenderReciever];
                if (!resultsSenderReciever.count) {
                    [uniqueMsgs addObject:inbox];
                    [allUniqueMsgs addObject:inbox];
                }
             }
        }
        
        NSLog(@"Unique Senders Messages are %lu", (unsigned long)uniqueMsgs.count);
        if(uniqueMsgs.count==0){
            [self.tableView setHidden:YES];
            [self.noChatsView setHidden:NO];
        }   else{
            [self.tableView setHidden:NO];
            [self.tableView reloadData];
        }
        [SVProgressHUD dismiss];
    } error:^(Fault *error) {
        NSLog(@"Inbox Found %@",error.description);
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - tableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
     return uniqueMsgs.count;
  }


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"inboxCell";
    
    InboxTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[InboxTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                              reuseIdentifier:MyIdentifier];
    }
    
    
    Inbox *inbox = [uniqueMsgs objectAtIndex:indexPath.row];
    
    // Decode
    NSString *companyStr = inbox.messageBody;
    NSString *trimmedString2 = [companyStr stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSData *decodedCompany = [[NSData alloc] initWithBase64EncodedString:trimmedString2 options:0];
    NSString *msg = [[NSString alloc] initWithData:decodedCompany encoding:NSUTF8StringEncoding];
    //
    
    if([inbox.isSeen isEqualToString:@"NO"] && ![backendless.userService.currentUser.objectId isEqualToString:inbox.Sender.objectId])
    {
        UIFont *newFont = [UIFont fontWithName:[NSString stringWithFormat:@"CaviarDreams_Bold"] size:18];
        cell.userMessage.font = newFont;
    }
    else{
        UIFont *newFont = [UIFont fontWithName:[NSString stringWithFormat:@"CaviarDreams"] size:14];
        cell.userMessage.font = newFont;
    }
    
    if([inbox.Reciever.objectId isEqualToString:backendless.userService.currentUser.objectId])
    {
        cell.userName.text = inbox.Sender.name;
        cell.userMessage.text = msg;
        [cell.userProfileImage  sd_setImageWithURL:[NSURL URLWithString:[inbox.Sender valueForKey:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"avatar.png"] options:SDWebImageRefreshCached];
        [cell.userProfileImage.layer setCornerRadius:cell.userProfileImage.frame.size.height/2];
        cell.userProfileImage.clipsToBounds=YES;
        
    }
    else{
    
        cell.userName.text = inbox.Reciever.name;
        cell.userMessage.text = msg;
        [cell.userProfileImage  sd_setImageWithURL:[NSURL URLWithString:[inbox.Reciever valueForKey:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"avatar.png"] options:SDWebImageRefreshCached];
        [cell.userProfileImage.layer setCornerRadius:cell.userProfileImage.frame.size.height/2];
        cell.userProfileImage.clipsToBounds=YES;
    
    }
    
    
      return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Inbox *inbox = uniqueMsgs[indexPath.row] ;
    if([inbox.Reciever.objectId isEqualToString:backendless.userService.currentUser.objectId]){
        self.selectedUser = inbox.Sender ;
    }
    else{
        self.selectedUser = inbox.Reciever ;
    
    }
    [self performSegueWithIdentifier:@"chatsToMessages" sender:nil];
    
   }


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"chatsToMessages"]) {
        
        FlatChatViewController *vc = segue.destinationViewController ;
        vc.selectedUser=self.selectedUser;
        
    }
}


- (IBAction)backAction:(id)sender {
    [self performSegueWithIdentifier:@"chatTohome" sender:self];
}
-(IBAction)unwindToChats:(UIStoryboardSegue*)sender
{
    
}
- (IBAction)newChatAction:(UIButton *)sender {
}
@end
