//
//  HomeScreenViewController.m
//  pic4me
//
//  Created by ibuildx on 8/1/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "MobileCoreServices/MobileCoreServices.h"

#import "HomeScreenViewController.h"
#import "UIImageView+WebCache.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "ActionSheetPicker.h"
#import "Photo.h"
#import "Video.h"

#import "UIViewController+CWPopup.h"

@interface HomeScreenViewController (){

  UIImage *selectedImage;
    NSURL *videoURL ;
    UIImage *thumbImageForVideo ;
   
 }

@end

@implementation HomeScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    [self.profileImage.layer setCornerRadius:self.profileImage.frame.size.height/2];
    self.profileImage.clipsToBounds=YES;
    
    [self.profileImage sd_setImageWithURL:[NSURL URLWithString:[backendless.userService.currentUser getProperty:@"profileImage"]]
                         placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                  options:SDWebImageRefreshCached];
    
    self.welcomeTextLabel.text=[NSString stringWithFormat:@"Hello %@ : what can we help you choose today?",backendless.userService.currentUser.name];
   

}

-(void)viewWillAppear:(BOOL)animated{
    //[igcMenu hideCircularMenu];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark actions
- (IBAction)unwindToHome:(UIStoryboardSegue*)sender
{
    
}
- (IBAction)DecisionsAction:(UIButton *)sender {
     [self performSegueWithIdentifier:@"hometoGridLists" sender:self];}

- (IBAction)chatsAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"homeTochats" sender:self];
}

- (IBAction)ArchiveAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"homeTogallery" sender:self];
}

- (IBAction)profileAction:(UIButton *)sender {
     [self performSegueWithIdentifier:@"homeToTabBar" sender:self];
}
@end
