//
//  CustomImageView.h
//  uChoose
//
//  Created by Mohit Gorakhiya on 4/21/18.
//  Copyright © 2018 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomImageView : UIImageView
{
    UIActivityIndicatorView *activity;
}
@property(nonatomic,retain)UIActivityIndicatorView *activity;
-(void)generateThumbImage:(NSString *)filepath;
-(void)displayIndicator;
@end
