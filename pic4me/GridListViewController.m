//
//  GridListViewController.m
//  pic4me
//
//  Created by ibuildx on 11/25/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "GridListViewController.h"
#import "GridListsCell.h"
#import "Grid.h"
#import "SharedViewController.h"
#import "UIImageView+WebCache.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "Backendless.h"
#import "UIViewController+CWPopup.h"
#import "WelcomePopup.h"



@interface GridListViewController (){
    Grid *selectedGrid;
    NSMutableArray *myDecesions;
    NSMutableArray *othersDecesions;
    NSTimer* myTimer;
}

@end

@implementation GridListViewController

- (void)viewDidLoad {
    NSLog(@"GridListViewController");
    [super viewDidLoad];

    
    
    myDecesions = [[NSMutableArray alloc] init];
    othersDecesions = [[NSMutableArray alloc] init];
    self.tableView.dataSource=self;
    self.tableView.delegate=self;
    [self.view layoutIfNeeded];
    [self showWelcomePopup];
    myTimer = [NSTimer scheduledTimerWithTimeInterval: 10.0 target: self
                                                      selector: @selector(callAfterTenSecond:) userInfo: nil repeats: NO];

}

-(void)viewWillAppear:(BOOL)animated {
    [self fetchAllDecesions];
}

-(void) callAfterTenSecond:(NSTimer*) t{
    [self dismissPopup];
    [myTimer invalidate];
}
- (void)showWelcomePopup {
    WelcomePopup *callUsPopUpView = [[WelcomePopup alloc] initWithNibName:@"WelcomePopup" bundle:nil];
    
    callUsPopUpView.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width- 50, self.view.frame.size.height * 0.4 );
   
    [callUsPopUpView.avatarImageView sd_setImageWithURL:[NSURL URLWithString:[backendless.userService.currentUser getProperty:@"profileImage"]]
                         placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                  options:SDWebImageRefreshCached];

    callUsPopUpView.titletext.text=[NSString stringWithFormat:@"Hello %@ what can we help you DECIDE today?",backendless.userService.currentUser.name];
    [self presentPopupViewController:callUsPopUpView animated:YES completion:^(void) {
        
        [callUsPopUpView.shareBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        
    }];
}
- (void)dismissPopup {
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            NSLog(@"popup view dismissed");
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{

}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"listToDetailGrid"]) {
        
        SharedViewController *vc = segue.destinationViewController ;
        vc.grid=selectedGrid;
        vc.delegate=self;
        
        
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return  self.gridsFound.count;
   }


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"gridList";
    
    GridListsCell  *cell = [self.tableView dequeueReusableCellWithIdentifier:MyIdentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[GridListsCell alloc] initWithStyle:UITableViewCellStyleDefault
                                              reuseIdentifier:MyIdentifier];
    }
    [cell layoutIfNeeded];
    Grid *grid = [_gridsFound objectAtIndex:indexPath.row];
    
    cell.lblGridDetails.text = grid.User.name;
    [cell.gridImage sd_setImageWithURL:[NSURL URLWithString:[grid.User getProperty:@"profileImage"]]
                       placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                options:SDWebImageRefreshCached];
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedGrid = [_gridsFound objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"listToDetailGrid" sender:self];
}

- (IBAction)backAction:(UIButton *)sender {
     [self performSegueWithIdentifier:@"listToHome" sender:self];
    
}
-(IBAction)unwindToListGrid:(UIStoryboardSegue*)sender
{
    
    
}
- (void)tellGridListSomething:(NSObject*)something{}


- (void)fetchAllDecesions {
    
    [SVProgressHUD show];
    [self showGlobalActivity];
    _gridsFound = [[NSMutableArray alloc] init];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Grid class]];
    [dataStore find:nil response:^(BackendlessCollection *gridColection) {
        
        for(Grid *grid in gridColection.data){
            
            NSArray *arr = [[NSArray alloc]initWithArray:grid.friends];
            for(int i = 0; i<arr.count ; i++){
                BackendlessUser *friendUser = arr[i];
                
                if([friendUser.objectId  isEqualToString: backendless.userService.currentUser.objectId ]){
                    
                    [_gridsFound addObject:grid];
                    
                }
            }
            if([grid.User.objectId isEqualToString:backendless.userService.currentUser.objectId]){
            
                [self.gridsFound addObject:grid];
            }
        }
        [SVProgressHUD dismiss];
        [self dismissGlobalActivity];
        if(self.gridsFound.count==0){
            [self.lblNothingToShow setHidden:NO];
            [self.tableView setHidden:YES];
            
        }else{
            [self.lblNothingToShow setHidden:YES];
            [self.tableView setHidden:NO];
        }
        
        [self.tableView reloadData];
        
        
    }
              error:^(Fault *error) {
                  [SVProgressHUD dismiss];
                  [self dismissGlobalActivity];
                  NSLog(@"grid Collection Error  %@",error.detail);
              }];
    
}

- (IBAction)startNewDecesionAction:(UIButton *)sender {
    
     [self performSegueWithIdentifier:@"listToNewGrid" sender:self];
}

#pragma mark - Handle Global Activity

-(void)showGlobalActivity {
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

-(void)dismissGlobalActivity {
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end
