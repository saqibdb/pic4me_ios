//
//  RatingUsersTableViewCell.h
//  pic4me
//
//  Created by ibuildx on 7/29/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RatingUsersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIImageView *rateType;

@end
