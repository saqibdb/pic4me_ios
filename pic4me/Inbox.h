//
//  Inbox.h
//  Accomplice
//
//  Created by Muhammad Saqib Yaqeen on 4/1/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"


@interface Inbox : NSObject

@property (nonatomic, strong) BackendlessUser *Sender;
@property (nonatomic, strong) BackendlessUser *Reciever;
@property (nonatomic, strong) NSString *isSeen;
@property (nonatomic, strong) NSString *messageBody;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *AppTime;



@end
