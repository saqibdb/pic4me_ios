//
//  DiscussForumViewController.m
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/14/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//
#import "UIViewController+CWPopup.h"
#import "SingleContentViewController.h"
#import "DiscussForumViewController.h"
#import "PhotoGalleryTableViewCell.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "Backendless.h"
#import "Photo.h"
#import "Video.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "Grid.h"
#import "GridListViewController.h"
#import "ImageViewerPopup.h"
@interface DiscussForumViewController (){
    
    NSMutableArray *gridsFound;
    ImageViewerPopup *popUp;
    Photo *popUpPhoto ;
    NSUInteger index;
}

@end

@implementation DiscussForumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.yourPhotosSprite setHidden:NO];
    [self.friendsPhotosSprite setHidden:YES];
    self.collectionView.dataSource=self;
    self.collectionView.delegate=self;
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchAllMyDecesions];
}
-(void)viewDidAppear:(BOOL)animated{
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)fetchAllMyDecesions {
    
    [SVProgressHUD show];
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[Grid class]];
    [dataStore find:nil response:^(BackendlessCollection *gridColection) {
        
        self.photosFound = [[NSMutableArray alloc] init];
        self.vidsFound = [[NSMutableArray alloc] init];
        
        for (Grid *grid in gridColection.data)
        {
            
            if([grid.User.objectId isEqualToString:backendless.userService.currentUser.objectId]){
                
                for (Photo *photo in grid.Photos) {
                    
                    [self.photosFound addObject:photo];
                    
                }
                for (Video *video in grid.Videos) {
                    [self.vidsFound addObject:video];
                }
            }
        }
        if(self.photosFound.count==0){
            [SVProgressHUD dismiss];

            [self.collectionView setHidden:YES];
            
        }else
        {
            
            [self.collectionView setHidden:NO];
            [self.collectionView reloadData];
            [SVProgressHUD dismiss];

        }
        
    }
              error:^(Fault *error) {
                  [SVProgressHUD dismiss];
                  NSLog(@"grid Collection Error  %@",error.detail);
              }];
    
}
- (void)fetchAllDecesionsOfOthers {
    
    //[SVProgressHUD show];
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[Grid class]];
    [dataStore find:nil response:^(BackendlessCollection *gridColection) {
        
        self.photosFound = [[NSMutableArray alloc] init];
        self.vidsFound = [[NSMutableArray alloc] init];
       
        for(Grid *grid in gridColection.data){
            
            NSArray *arr = [[NSArray alloc]initWithArray:grid.friends];
            for(int i = 0; i<arr.count ; i++){
                BackendlessUser *friendUser = arr[i];
                
                if([friendUser.objectId  isEqualToString: backendless.userService.currentUser.objectId ]){
                    
                    [self.photosFound addObjectsFromArray:grid.Photos];
                    [self.vidsFound addObjectsFromArray:grid.Videos];
                }
            }
        }
        
//        for (Photo *photo in friendsGrids) {
//            [self.photosFound addObject:photo];
//        }
//        for (Video *video in friendsGrids) {
//            [self.vidsFound addObject:video];
//        }
        if(self.photosFound.count==0){
            [SVProgressHUD dismiss];

            [self.collectionView setHidden:YES];
            
        }else
        {
            [self.collectionView setHidden:NO];
            [self.collectionView reloadData];
            [SVProgressHUD dismiss];

        }
        
    }
              error:^(Fault *error) {
                  [SVProgressHUD dismiss];
                  NSLog(@"grid Collection Error  %@",error.detail);
              }];
    
}

#pragma mark collection view cell paddings
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 3.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake(cellWidth, cellWidth);
    
    return size;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _photosFound.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    
    Photo *photo = self.photosFound[indexPath.row];
    
    
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    
    [recipeImageView sd_setImageWithURL:[NSURL URLWithString:photo.photo]
                       placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                options:SDWebImageRefreshCached];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    index = indexPath.row;
    popUpPhoto = self.photosFound[index];
    [self ShowPopUpImage];
    
}
- (void)ShowPopUpImage {
    
    popUp = [[ImageViewerPopup alloc] initWithNibName:@"ImageViewerPopUp" bundle:nil];
    
    popUp.view.frame = self.view.frame;
    self.useBlurForPopup=YES;
    [self presentPopupViewController:popUp animated:YES completion:^(void) {
        
        [popUp.nextBtn addTarget:self action:@selector(nextPrevBtnsPressed:) forControlEvents:UIControlEventTouchUpInside];
        [popUp.prevBtn addTarget:self action:@selector(nextPrevBtnsPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [popUp.btnClose addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        [popUp.mainImage sd_setImageWithURL:[NSURL URLWithString:popUpPhoto.photo]
                           placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                    options:SDWebImageRefreshCached];
        
        
        
    }];
    
}
-(IBAction)nextPrevBtnsPressed:(UIButton *)sender{
    NSUInteger i = self.photosFound.count;
    
    if(sender.tag == 13){
        if(index>=i-1){}
        else{
            index ++;
            popUpPhoto = self.photosFound[index];
            [popUp.mainImage sd_setImageWithURL:[NSURL URLWithString:popUpPhoto.photo]
                               placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                        options:SDWebImageRefreshCached];
            
        }
    }
    else{
        if(index<=0){}
        else{
            index --;
            popUpPhoto = self.photosFound[index];
            [popUp.mainImage sd_setImageWithURL:[NSURL URLWithString:popUpPhoto.photo]
                               placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                        options:SDWebImageRefreshCached];
            
        }
        
    }
}
-(void)dismissPopup{
    
    [self dismissPopupViewControllerAnimated:YES completion:nil];
}

- (IBAction)unwindToDiscuss:(UIStoryboardSegue*)sender
{
    
}
- (IBAction)backeAction:(id)sender {
    [self performSegueWithIdentifier:@"galleryToHomeBack" sender:self];
}


- (IBAction)yourPhotos:(UIButton *)sender {
//    [SVProgressHUD show];
    [self.yourPhotosSprite setHidden:NO];
    [self.friendsPhotosSprite setHidden:YES];
    [self fetchAllMyDecesions];
}

- (IBAction)photosOfYouAction:(UIButton *)sender {
    [SVProgressHUD show];

    [self.yourPhotosSprite setHidden:YES];
    [self.friendsPhotosSprite setHidden:NO];
    [self fetchAllDecesionsOfOthers];
}
@end
