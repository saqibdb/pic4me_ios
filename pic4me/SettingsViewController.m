//
//  SettingsViewController.m
//  pic4me
//
//  Created by ibuildx on 8/4/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "SettingsViewController.h"
#import "UIImageView+WebCache.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = YES;
    
    [self.view layoutIfNeeded];
    [self.userProfile.layer setCornerRadius:self.userProfile.frame.size.height/2];
    self.userProfile.clipsToBounds=YES;
    
    [self.userProfile sd_setImageWithURL:[NSURL URLWithString:[backendless.userService.currentUser getProperty:@"profileImage"]]
                        placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                 options:SDWebImageRefreshCached];
    
    self.userName.text=[NSString stringWithFormat:@"%@",backendless.userService.currentUser.name];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBar.hidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)locationSwitchAction:(UISwitch *)sender {
    
        if(self.locationSwitch.isOn){
    
     
        NSLog(@"on");
    }else{
    NSLog(@"off");
   
    }
}

- (IBAction)notificationSwitchAction:(UISwitch *)sender {
    if(self.locationSwitch.isOn){
        
        
        NSLog(@"on");
    }else{
        NSLog(@"off");
        
    }

}
- (IBAction)changePasswordAction:(UIButton *)sender {
    [SVProgressHUD show];
    
    [self userPasswordRecovery:@"asadjavid46@gmail.com"];
   }
-(void)userPasswordRecovery:(NSString *)login
{
        [backendless.userService restorePassword:login response:^(id changePassword) {
              [SVProgressHUD dismiss];
            
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@" " andText:@"Email sent successfully" andCancelButton:NO forAlertType:AlertSuccess];
            
            [alert show];
            
        } error:^(Fault *error) {
            [SVProgressHUD dismiss];
            
            NSString *errorMsg = [NSString stringWithFormat:@"Fail to send mail = %@", error.description];
            NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
            
            [alert show];

            
        }];
}

- (IBAction)cancelChangeAction:(UIButton *)sender {
     [self.changePasswordView setHidden:YES];
     [self.txtOldPassword resignFirstResponder];
}

- (IBAction)openChangePasswordPopUp:(UIButton *)sender {
    [self.changePasswordView setHidden:NO];
    [self.txtOldPassword becomeFirstResponder];
}

- (IBAction)backAction:(UIButton *)sender {
    [self .navigationController popViewControllerAnimated:YES];
    
    [self performSegueWithIdentifier:@"settingsTohome" sender:self];
}

- (IBAction)logoutAction:(id)sender {
    if (backendless.userService.currentUser) {
        [SVProgressHUD show];
        
        [backendless.userService logout];
        
        [SVProgressHUD dismiss];
        [self performSegueWithIdentifier:@"logoutToLoginVC" sender:self];
    }
}
@end
