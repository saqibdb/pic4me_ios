//
//  BrowserViewController.h
//  pic4me
//
//  Created by ibuildx on 11/23/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"


@protocol BrowserDelegate <NSObject>
- (void)uploadGridToBrowser:(NSObject*)something;
- (void)shareGridToBrowser:(NSObject*)something;
@end
@interface BrowserViewController : UIViewController<UIWebViewDelegate, UITextFieldDelegate, iCarouselDataSource, iCarouselDelegate>

- (IBAction)goAction:(UIButton *)sender;
@property (weak, nonatomic) id <BrowserDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *imageTitle;
@property (weak, nonatomic) IBOutlet UIView *complitionVie;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldUrl;

@property (weak, nonatomic) IBOutlet UIProgressView *webProgressView;


@property (weak, nonatomic) IBOutlet UIView *carouselSuperView;
@property (weak, nonatomic) IBOutlet iCarousel *imagescarousel;







- (IBAction)PActionTriggered:(UITextField *)sender;
- (IBAction)doneAction:(UIButton *)sender;
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)backToShareGrid:(UIButton *)sender;
- (IBAction)txtUrlDoneAction:(id)sender;
- (IBAction)selectImageForCarouselAction:(UIButton *)sender;

@end
