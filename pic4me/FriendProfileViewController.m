//
//  FriendProfileViewController.m
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/25/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "FriendProfileViewController.h"
#import "UIImageView+WebCache.h"
#import "Backendless.h"
@interface FriendProfileViewController ()
{


UIVisualEffectView *visualEffectView;
}

@end

@implementation FriendProfileViewController

- (void)viewDidLoad {
    NSLog(@"FriendProfileViewController");
    [super viewDidLoad];
    [self loadProfileData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadProfileData{
    
    
    self.userName.text= self.selectedUser.name;
    self.email.text=self.selectedUser.email;
    
    
    
    if ([self.selectedUser getProperty:@"phone"] == [NSNull null]){
        self.phone.text=@"";
    }
    else
    {
        self.phone.text=[self.selectedUser getProperty:@"phone"];
    }

    
    
    if ([self.selectedUser getProperty:@"gender"] == [NSNull null]){
        self.gender.text=@"";
        }
    else
    {
        self.gender.text=[self.selectedUser getProperty:@"gender"];
    }
    
    if ([self.selectedUser getProperty:@"age"] == [NSNull null]){
         self.age.text=@"";
    }
    else
    {
      self.age.text=[NSString stringWithFormat:@"%@ Years",[self.selectedUser getProperty:@"age"]];
    }
    
    [self.profilePhoto sd_setImageWithURL:[NSURL URLWithString:[self.selectedUser getProperty:@"profileImage"]]
                              placeholderImage:[UIImage imageNamed:@"avatar.png"]
                                       options:SDWebImageRefreshCached];
    [self.coverPhoto sd_setImageWithURL:[NSURL URLWithString:[self.selectedUser getProperty:@"profileImage"]]
                      placeholderImage:[UIImage imageNamed:@"avatar.png"]
                               options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   
                                   UIVisualEffect *blurEffect;
                                   blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
                                   [visualEffectView removeFromSuperview];
                                   visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
                                   
                                   visualEffectView.frame = self.coverPhoto.bounds;
                                   [self.coverPhoto addSubview:visualEffectView];
                               }];

    
    [self.profilePhoto.layer setCornerRadius:self.profilePhoto.frame.size.height/2];
    self.profilePhoto.clipsToBounds=YES;
   self.profilePhoto.layer.borderWidth=2.0;
   self.profilePhoto.layer.borderColor=[[UIColor whiteColor] CGColor];

}
- (IBAction)backAction:(id)sender {
    [self performSegueWithIdentifier:@"profileToFriends" sender:self];
}
@end
