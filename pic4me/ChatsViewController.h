//
//  ChatsViewController.h
//  pic4me
//
//  Created by ibuildx on 8/30/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backendless.h"
@interface ChatsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
- (IBAction)backAction:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong) BackendlessUser *selectedUser;
- (IBAction)newChatAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *noChatsView;


@end
