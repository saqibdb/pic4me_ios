//
//  GridListViewController.h
//  pic4me
//
//  Created by ibuildx on 11/25/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedViewController.h"



@interface GridListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,ListGridDelegate>
- (IBAction)backAction:(UIButton *)sender;
@property (strong) NSMutableArray  *gridsFound;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lblNothingToShow;

- (IBAction)startNewDecesionAction:(UIButton *)sender;



@end
