//
//  AppDelegate.m
//  pic4me
//
//  Created by ibuildx on 7/3/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//
//com.saqibdb.pic4me
#import "HomeScreenViewController.h"
#import "FlatChatViewController.h"
#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "Backendless.h"
#import "MediaService.h"
#import "Reachability.h"

@interface AppDelegate ()
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    [backendless initApp:@"A8D0BF60-064F-F6A0-FF75-78EE1237D400" secret:@"ADF4490C-9934-2263-FF82-3FC87E408500" version:@"v1"];
   
    
    // Override point for customization after application launch.
    [Fabric with:@[[Crashlytics class]]];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    //Caused crash
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//
//        [backendless.messaging registerForRemoteNotifications];
//
//    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
         [backendless.messaging registerForRemoteNotifications];
    });
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
     [FBSDKAppEvents activateApp];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (void)applicationDidFinishLaunching:(UIApplication *)application
{
    NSSetUncaughtExceptionHandler(&myExceptionHandler);
}
void myExceptionHandler(NSException *exception)
{
    NSArray *stack = [exception callStackReturnAddresses];
    NSLog(@"Stack trace: %@", stack);
}
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    
    
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    
    
    NSLog(@"currentDeviceId = %@", currentDeviceId);
    
    
    
    NSString *deviceTokenStr = [backendless.messagingService deviceTokenAsString:deviceToken];
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @try {
            NSString *deviceRegistrationId = [NSString stringWithFormat:@"%@",[backendless.messagingService registerDeviceToken:deviceTokenStr]];
            NSLog(@"deviceToken = %@, deviceRegistrationId = %@", deviceTokenStr, deviceRegistrationId);
        }
        @catch (Fault *fault) {
            NSLog(@"deviceToken = %@, FAULT = %@ <%@>", deviceTokenStr, fault.message, fault.detail);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    });
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    
    NSLog(@"Error gotten At %@",err);
    // handle error
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
    {
        NSLog(@"Push Recieved In Background");
        
        //opened from a push notification when the app was on background
        
        //TODO Open the chat view on message recieved
    }
    else{
        NSLog(@"Push Recieved");
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName: @"MessageREcieved" object:nil userInfo:userInfo];
        
        
        NSString *alertMessage = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        
        
        /* [TSMessage showNotificationInViewController:[self topViewController]
                                              title:@"Message Recieved"
                                           subtitle:alertMessage
                                              image:nil
                                               type:TSMessageNotificationTypeMessage
                                           duration:TSMessageNotificationDurationAutomatic
                                           callback:nil
                                        buttonTitle:@"OK"
                                     buttonCallback:^{
                                         NSLog(@"User tapped the button");
                                     }
                                         atPosition:TSMessageNotificationPositionTop
                               canBeDismissedByUser:YES];
        */
        
        
        
    }
    [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
    
    
}
- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

-(void)displayAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    UIViewController *viewController = [self topViewController];
    
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:title  message:message  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                {
                                    
                                }]];
    [viewController presentViewController:alertController animated:YES completion:nil];
}

@end

