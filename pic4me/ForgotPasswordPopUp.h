//
//  ForgotPasswordPopUp.h
//  pic4me
//
//  Created by iBuildx-Mac3 on 2/24/17.
//  Copyright © 2017 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordPopUp : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;

@end
