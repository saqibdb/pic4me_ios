//
//  ViewController.m
//  pic4me
//
//  Created by ibuildx on 7/3/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "ViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "ForgotPasswordPopUp.h"
#import "UIViewController+CWPopup.h"

@interface ViewController ()
{
    ForgotPasswordPopUp *popUp ;
    IBOutlet UIScrollView *scrollView;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [self maskTriananlgesAndCircularBorderedImage];
    [super viewDidLoad];
    self.loginEmailTxt.delegate = self;
    self.loginPasswordTxt.delegate = self;
    
    scrollView.scrollEnabled = YES;
    CGRect frame = self.view.frame;
    frame.size.height =  frame.size.height + 1;
    scrollView.contentSize = frame.size;
    
    Reachability *_reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
    if (remoteHostStatus == NotReachable) {
        
        NSLog(@"not reachable");
        // not reachable
    } else if (remoteHostStatus == ReachableViaWiFi) {
        NSLog(@"reachable via Wifi");
        // reachable via Wifi
    } else if (remoteHostStatus == ReachableViaWWAN) {
        
         NSLog(@"reachable via WWAN");
        // reachable via WWAN
    }
    [self setCursorColor];
    
   [self loginSilently];
   
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWillShow: (NSNotification *) notif{
    // Do something here
    NSLog(@"keyboardDidShow");
//    CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
//    [scrollView setContentOffset:bottomOffset animated:YES];
    [scrollView setContentOffset:CGPointMake(0, 55)];
}

- (void)keyboardWillHide: (NSNotification *) notif{
    // Do something here
    NSLog(@"keyboardDidHide");
    [scrollView setContentOffset:CGPointMake(0, 0)];
}
-(void) loginSilently{
    if (![self chechIfTokenAlive]) {
        NSLog(@"Token expired");
        //Need user login action
    }
    else {
        //Token alive, continue to home screen after login
        [self loginAction:nil];
    }
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)setCursorColor {
    [[UITextField appearance] setTintColor:[UIColor blackColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - View Starting Methods

-(void)maskTriananlgesAndCircularBorderedImage {
    // Build a triangular path
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){self.loginSprite.frame.size.width / 2, 0}];
    
    [path addLineToPoint:(CGPoint){0, self.loginSprite.frame.size.height}];
    
    
    [path addLineToPoint:(CGPoint){self.loginSprite.frame.size.width, self.loginSprite.frame.size.height}];
    [path addLineToPoint:(CGPoint){self.loginSprite.frame.size.width / 2, 0}];
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = self.loginSprite.bounds;
    mask.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    self.loginSprite.layer.mask = mask;
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask2 = [CAShapeLayer new];
    mask2.frame = self.signUpSprite.bounds;
    mask2.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    self.signUpSprite.layer.mask = mask2;
    self.loginBtn.layer.cornerRadius = 5;
    self.loginBtn.clipsToBounds = YES;
    self.facebookLoginBtn.layer.cornerRadius = 5;
    self.facebookLoginBtn.clipsToBounds = YES;
    self.signUpBtn.layer.cornerRadius = 5;
    self.signUpBtn.clipsToBounds = YES;
    self.browseBtn.layer.cornerRadius = 5;
    self.browseBtn.clipsToBounds = YES;
}


- (IBAction)signUpAction:(id)sender {
}
- (IBAction)backAction:(id)sender {
    NSLog(@"clicked");
}
- (IBAction)loginAction:(id)sender {
    [SVProgressHUD showWithStatus:@"Checking Credentials ..."];
    BackendlessUser *user = [BackendlessUser new];
    [user setProperty:@"email" object:_loginEmailTxt.text];
    [user setProperty:@"password" object:_loginPasswordTxt.text];
    //user.email = _loginEmailTxt.text;
    //user.password = _loginPasswordTxt.text;
    [self loginWithUser:user];
}

-(void)userPasswordRecovery:(NSString *)login
{
    [backendless.userService restorePassword:login response:^(id changePassword) {
        [SVProgressHUD dismiss];
        [self dismissPopup];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"" andText:@"An email with instructions to change your password has been sent to you." andCancelButton:NO forAlertType:AlertSuccess];
        [alert show];
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        
               AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:error.message andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}

- (IBAction)forgotPasswordAction:(UIButton *)sender {
    
     popUp = [[ForgotPasswordPopUp alloc] initWithNibName:@"ForgotPasswordPopUp" bundle:nil];
    
    [self presentPopupViewController:popUp animated:YES completion:^(void) {
        
        [popUp.sendBtn addTarget:self action:@selector(sendButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [popUp.cancelBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
    }];
     
}
-(void)sendButtonPressed{
    NSLog(@"%@",popUp.email.text);
    if(!popUp.email.text.length){
    
    }
    else{
        [self userPasswordRecovery:popUp.email.text];
    }
    
}
-(void)dismissPopup{
    
    [self dismissPopupViewControllerAnimated:YES completion:nil];
}
- (IBAction)loginPasswordAction:(id)sender {
}
- (IBAction)learnMoreAction:(id)sender {
}
- (IBAction)loginTabAction:(id)sender {
}
- (IBAction)signUpTabAction:(id)sender {
}
- (IBAction)browseAction:(id)sender {
}

- (IBAction)facebookLoginAction:(id)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    [login
     logInWithReadPermissions: @[@"user_about_me",
                                 @"user_birthday",
                                 @"email",
                                 @"user_friends",
                                 @"user_photos",
                                 @"public_profile"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        [SVProgressHUD show];
         
         if (error) {
             NSLog(@"Process error");
             
          //[self showActioSheetWithMessage:error.description];
             
             
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
            // [self showActioSheetWithMessage:@"Cancelled By User"];
             
         } else {
             NSLog(@"Logged in");
             if ([result.grantedPermissions containsObject:@"email"]) {
                 NSLog(@"Granted all permission");
                 
                 if ([FBSDKAccessToken currentAccessToken]) {
                     
                     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"picture, email, name, gender, birthday"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         if (!error) {
                             NSLog(@"Private Date = %@",result);
                             
                             [self compileAndRegisterUser:result];
                         }
                         else{
                             //[self showActioSheetWithMessage:error.description];
                         }
                     }];
                 }
             } else {
                 NSLog(@"Not granted");
                 //[self showActioSheetWithMessage:@"Access Not Granted, Please grant access to the facebook"];
             }
         }
     }];
}

- (IBAction)signUpNewAction:(id)sender {
    [self performSegueWithIdentifier:@"loginToSignUp" sender:self];
}
-(void)compileAndRegisterUser :(id) result {
    
    
    BackendlessUser *user = [BackendlessUser new];
    [user setProperty:@"password" object:@"1223"];
    
    NSString *userEmail = [result valueForKey:@"email"];
    if (!userEmail) {
        userEmail = [NSString stringWithFormat:@"%@@facebook.com",[result valueForKey:@"id"]];
    }
    
    [user setProperty:@"name" object:[result valueForKey:@"name"]];
    [user setProperty:@"email" object:userEmail];
    
    
    
    NSString *userProfilePicStr = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [result valueForKey:@"id"]];
    
    
    [user setProperty:@"profileImage" object:userProfilePicStr];
       
    [backendless.userService registering:user response:^(BackendlessUser *newRegisteredUser) {
        NSLog(@"user = %@", newRegisteredUser.name);
        [newRegisteredUser setProperty:@"password" object:@"1223"];
        
        [self loginWithUser:newRegisteredUser];
        
           [self performSegueWithIdentifier:@"loginToHomeScreen" sender:self];
        
        
    } error:^(Fault *error) {
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        if ([error.message containsString:@"User already exists"]) {
            [self loginWithUser:user];
        }
        else{
           [SVProgressHUD dismiss];
        }
    }];
    
}
-(void)loginWithUser :(BackendlessUser *)loginUser
{
    [backendless.userService login:loginUser.email password:loginUser.password response:^(BackendlessUser *userLogged) {

//        [backendless.messagingService getRegistrationAsync:^(DeviceRegistration *deviceRegistration) {
//            NSLog(@"The new device Id for push is %@ and token %@",deviceRegistration.deviceId , deviceRegistration.deviceToken);
//            [userLogged setProperty:@"deviceId" object:deviceRegistration.deviceId];
        
        
        //[self performSegueWithIdentifier:@"loginToHomeScreen" sender:self];

        
            id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
            [dataStore save:userLogged response:^(id result) {
                //NSLog(@"New Device Id Saved %@",deviceRegistration.deviceId);
                
                [self performSegueWithIdentifier:@"loginToHomeScreen" sender:self];
               //[self findAndRepealDeviceId:deviceRegistration.deviceId];
                
                
            } error:^(Fault *error) {
                NSLog(@"Error at deviceId %@",error.description);
                NSLog(@"Error at deviceId %@",error.description);
                [SVProgressHUD dismiss];
                
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:[NSString stringWithFormat:@"%@",error.message] andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
            }];
//     } error:^(Fault *error) {
//           NSLog(@"Error at get Device Registration %@",error.description);
//        }];
    
        [SVProgressHUD dismiss];
    }
    error:^(Fault *error) {
        
       [SVProgressHUD dismiss];
        
       AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:[NSString stringWithFormat:@"%@",error.message] andCancelButton:NO forAlertType:AlertInfo];
       [alert show];
    }];
} 

-(void)findAndRepealDeviceId :(NSString *)deviceId {
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"deviceId = '%@'", deviceId];
    [backendless.persistenceService find:[BackendlessUser class] dataQuery:query response:^(BackendlessCollection *collection) {
        for (BackendlessUser *user in collection.data) {
            if (![user.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
                [user setProperty:@"deviceId" object:@"Repealled"] ;
                
            }
            else{
                [user setProperty:@"deviceId" object:deviceId];
            }
            id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
            [dataStore save:user response:^(id saved) {
                NSLog(@"Users Device Id Repealles");
            } error:^(Fault *error) {
              NSLog(@"Error = %@" , error.description);
            }];
        }
    } error:^(Fault *error) {
        NSLog(@"Error = %@" , error.description);
    }];
}
#pragma mark - Check token validity
-(BOOL) chechIfTokenAlive{
   return [backendless.userService isValidUserToken];
}

- (IBAction)unwindToLogin:(UIStoryboardSegue*)sender
{
    
}
@end
