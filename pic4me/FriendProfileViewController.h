//
//  FriendProfileViewController.h
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/25/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backendless.h"

@interface FriendProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *coverPhoto;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIImageView *profilePhoto;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *gender;
@property (weak, nonatomic) IBOutlet UILabel *age;
@property (strong,nonatomic) BackendlessUser *selectedUser;

- (IBAction)backAction:(id)sender;

@end
