//
//  PhotoGalleryTableViewCell.m
//  pic4me
//
//  Created by Muhammad Saqib Yaqeen on 7/14/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import "PhotoGalleryTableViewCell.h"

@implementation PhotoGalleryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
