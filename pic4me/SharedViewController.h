//
//  SharedViewController.h
//  pic4me
//
//  Created by ibuildx on 11/21/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//
#import <AVKit/AVKit.h>
#import <UIKit/UIKit.h>
#import "Backendless.h"
#import "Grid.h"
#import "Video.h"
#import "AppDelegate.h"
@protocol ListGridDelegate <NSObject>
- (void)tellGridListSomething:(NSObject*)something;
@end

@interface SharedViewController : UIViewController<AVPlayerViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>
{
    NSMutableArray *ratingsGridA;
    NSMutableArray *ratingsGridB;
    NSMutableArray *ratingsGridC;
    NSMutableArray *ratingsGridD;
    IBOutlet UIScrollView *imageScrollView;
    IBOutlet UIImageView *singleImageView;
    AppDelegate *appDel;
}
@property(nonatomic,retain) IBOutlet UIScrollView *imageScrollView;
@property(nonatomic,retain)IBOutlet UIImageView *singleImageView;
@property (strong, nonatomic) AVPlayerViewController *myplayerViewController;

@property NSInteger senderTag;
@property (weak, nonatomic) id <ListGridDelegate> delegate;
@property (nonatomic, strong) Grid *grid;
- (IBAction)shareSocialMediaAction:(UIButton *)sender;
@property (nonatomic) AVPlayerViewController *playerViewController;
//one
@property (weak, nonatomic) IBOutlet UIView *container_a;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *views_a;
//two
@property (weak, nonatomic) IBOutlet UIView *container_b;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *view_b;
//three
@property (weak, nonatomic) IBOutlet UIView *container_c;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *views_c;
//four
@property (weak, nonatomic) IBOutlet UIView *container_d;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *views_d;
//five
@property (weak, nonatomic) IBOutlet UIView *container_e;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *view_e;
//six
@property (weak, nonatomic) IBOutlet UIView *container_f;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *views_f;
@property (weak, nonatomic) IBOutlet UIImageView *gridUserProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *gridUserName;
@property (strong) Photo *selectedPhoto;
@property (strong) Video *selectedVideo;


@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@property (weak, nonatomic) IBOutlet UILabel *lblComment;

@property (strong, nonatomic) IBOutlet UIView *likeFullView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *likeViewHeight;

@property (strong, nonatomic) IBOutlet UIView *likeTopView;
@property (strong, nonatomic) IBOutlet UIView *likeBottomView;
@property (strong, nonatomic) IBOutlet UIView *likeViewA;
@property (strong, nonatomic) IBOutlet UIView *likeViewB;
@property (strong, nonatomic) IBOutlet UIView *likeViewC;
@property (strong, nonatomic) IBOutlet UIView *likeViewD;
@property (strong, nonatomic) IBOutlet UIButton *openInWebButton;

@property (strong, nonatomic) IBOutlet UIButton *thumbsUpA;
@property (strong, nonatomic) IBOutlet UIButton *thumbsDownA;

@property (strong, nonatomic) IBOutlet UIButton *thumbsUpB;
@property (strong, nonatomic) IBOutlet UIButton *thumbsDownB;

@property (strong, nonatomic) IBOutlet UIButton *thumbsUpC;
@property (strong, nonatomic) IBOutlet UIButton *thumbsDownC;

@property (strong, nonatomic) IBOutlet UIButton *thumbsUpD;
@property (strong, nonatomic) IBOutlet UIButton *thumbsDownD;

@property NSInteger thumsUpCountA;
@property NSInteger thumsUpCountB;
@property NSInteger thumsUpCountC;
@property NSInteger thumsUpCountD;

@property NSInteger thumsDownCountA;
@property NSInteger thumsDownCountB;
@property NSInteger thumsDownCountC;
@property NSInteger thumsDownCountD;


/*Rating and comment IBOutlets*/
@property (weak, nonatomic) IBOutlet UITableView *commentsTableView;
@property (weak, nonatomic) IBOutlet UITextField *writeCommentTxtField;
- (IBAction)doneCommentAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *writeCommntTxt;
- (IBAction)openTemplateComments:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *BtnThumbsUp;
@property (weak, nonatomic) IBOutlet UIButton *btnThumbsDown;

@property (weak, nonatomic) IBOutlet UITableView *templateCommetnsTableView;
@property (weak, nonatomic) IBOutlet UIButton *openTemplates;
@property (weak, nonatomic) IBOutlet UIView *templateView;
@property (strong) NSMutableArray  *CommentsFound;
- (IBAction)thumbsUpAction:(UIButton *)sender;
- (IBAction)thumbsDownAction:(UIButton *)sender;
@property (strong) NSMutableArray  *thumbsUpFound;
@property (strong) NSMutableArray  *thumbsDownFound;
@property (weak, nonatomic) IBOutlet UIButton *showThumbsUpBtn;
@property (weak, nonatomic) IBOutlet UIButton *showThumbsDownBtn;
- (IBAction)showThumbsUpAction:(UIButton *)sender;
- (IBAction)showThumbsDownAction:(UIButton *)sender;
@property (nonatomic, strong) Photo *clickedPhoto;
@end
