//
//  ProfileViewController.h
//  pic4me
//
//  Created by ibuildx on 7/4/16.
//  Copyright © 2016 ibuildx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ActionSheetPicker.h"

//#import <PermissionScope/PermissionScope-Swift.h>
@interface ProfileViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtUserName;

@property (weak, nonatomic) IBOutlet UITextField *txtAge;
@property (weak, nonatomic) IBOutlet UITextField *txtGender;
@property (weak, nonatomic) IBOutlet UITextField *txtNickName;
@property (nonatomic, strong) NSMutableArray *contacts;
@property (weak, nonatomic) IBOutlet UIButton *updateProfileAction;

- (IBAction)updateProfileAction:(id)sender;
- (IBAction)profileBackAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *outerView;

//@property (nonatomic, strong) PermissionScope *singlePscope;
//@property (nonatomic, strong) PermissionScope *multiPscope;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;

- (IBAction)cangeProfileAction:(UIButton *)sender;

- (IBAction)genderRadioAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *maleRadioImage;
@property (weak, nonatomic) IBOutlet UIImageView *femailRadioImage;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;

@end
